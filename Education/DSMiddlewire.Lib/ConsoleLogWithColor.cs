﻿using FuNong.Framework.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSMiddlewire.Lib
{
    public class ConsoleLogWithColor
    {
        public ConsoleLogWithColor(ILoggerService logService)
        {
            this.logService = logService;
        }

        private ILoggerService logService;

        public void WriteLine(string text, ConsoleColor color)
        {
            //Console output
            Console.ForegroundColor = color;
            var dtNow = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "]";
            Console.WriteLine(dtNow + text);
            Console.ForegroundColor = ConsoleColor.White;

            //log file output
            logService.Info(text);
        }
    }
}
