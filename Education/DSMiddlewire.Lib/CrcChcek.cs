﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSMiddlewire.Lib
{
    public class CrcChcek
    {
        public static bool CheckMessageCRC(byte[] message, out string messageReceived)
        {
            //接收到的数据长度
            var messageLength = message.Length;

            //收到的数据信息
            var messageReceivedStrBytes = new byte[messageLength - 2];
            Array.Copy(message, 0, messageReceivedStrBytes, 0, messageLength - 2);

            //CRC校验码固定2个字节
            var messageReceivedCrcBytes = new byte[2];
            Array.Copy(message, messageLength - 2, messageReceivedCrcBytes, 0, 2);

            //获取传输数据
            var messageCalculatedString = Encoding.Default.GetString(messageReceivedStrBytes);
            messageReceived = messageCalculatedString;

            //获取CRC校验码
            var currentCRC = byte.Parse(messageReceivedCrcBytes[0].ToString()) * 256 + byte.Parse(messageReceivedCrcBytes[1].ToString());

            //数据部分的crc校验
            var result = ushort.Parse(CRC16.CalculateCrc16(messageCalculatedString));

            if (currentCRC == result)
                return true;
            return false;
        }

           /*
            * 向底层硬件发送的CRC封装方法
            * 1.var crcGenerate = GetCRC(result); 先得到crc码：14321
            * 2.得到高位，低位值： 14321/256 = 55, 14321%256=241
            * 3.转为byte，追加到最后两位byte数组中，传给硬件即可
            * */
        public static byte[] ConstructMessageWithCRC(string message)
        {
            //信息主体，加上一个分隔符
            var messageToConstruct = message + "|";

            //算出crc码
            var crcCode = CRC16.CalculateCrc16(messageToConstruct);
            var crcCodeShort = ushort.Parse(crcCode);

            //CRC码高位
            var crcHigh = byte.Parse((crcCodeShort / 256).ToString());
            //CRC码低位
            var crcLow = byte.Parse((crcCodeShort % 256).ToString());

            var messageBytes = Encoding.Default.GetBytes(messageToConstruct);
            var messageLength = messageBytes.Length;

            var messageBytesWithCRC = new byte[messageLength + 2];
            Array.Copy(messageBytes, 0, messageBytesWithCRC, 0, messageLength);

            //放CRC码到数组最后两位
            messageBytesWithCRC[messageLength] = crcHigh;
            messageBytesWithCRC[messageLength + 1] = crcLow;

            return messageBytesWithCRC;
        }
    }
}
