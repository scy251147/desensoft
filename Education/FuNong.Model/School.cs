﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using System.Linq.Expressions;

namespace FuNong.Model
{
    public class School : BaseQuery<edu_school,DateTime?>
    {

        public School(IRepository<edu_school> schoolRepo)
            : base(schoolRepo)
        {
            this.schoolRepo = schoolRepo;
        }

        private readonly IRepository<edu_school> schoolRepo;
        public IQueryable<edu_school> GetSchoolQuery(
                                        int pageCount
                                      , int currentIndex
                                      , out int totalCount
                                      , Expression<Func<edu_school, bool>> where
                                      , Expression<Func<edu_school, DateTime?>> orderBy
                                      , string propertyName = ""
                                      , string propertyValue = "")
        {
            return GetQuery(pageCount, currentIndex,out totalCount, where, orderBy, propertyName, propertyValue);
        }
    }
}
