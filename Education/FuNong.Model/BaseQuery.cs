﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using System.Linq.Expressions;

namespace FuNong.Model
{
    public class BaseQuery<T,T1> : Base<T> where T : class
    {
        public BaseQuery(IRepository<T> repo)
        {
            this.repo = repo;
        }

        private readonly IRepository<T> repo;

        public IQueryable<T> GetQuery(
                                       int pageCount
                                     , int currentIndex
                                     , out int totalCount
                                     , Expression<Func<T, bool>> where
                                     , Expression<Func<T, T1>> orderBy
                                     , string propertyName = ""
                                     , string propertyValue = "")
        {
            IQueryable<T> queryObjects = null;
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            if (!string.IsNullOrEmpty(propertyName))
                queryObjects = CreateDynamicQuery<T>(propertyName, propertyValue, where, repo);
            else
                queryObjects = repo.GetMany(where);
            totalCount = queryObjects.Count();

            return queryObjects.OrderByDescending(orderBy).Skip(skipRows).Take(pageCount);
        }

    }
}
