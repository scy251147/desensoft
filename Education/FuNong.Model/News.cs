﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using System.Linq.Expressions;

namespace FuNong.Model
{
    public class News : Base<edu_cms_news>
    {
        public News(IRepository<edu_cms_news> newsRepo, IRepository<edu_cms_news_type> newsTypeRepo, IRepository<funong_news_soil_map> mapRepo)
        {
            this.newsRepo = newsRepo;
            this.newsTypeRepo = newsTypeRepo;
            this.mapRepo = mapRepo;
        }

        private readonly IRepository<edu_cms_news> newsRepo;
        private readonly IRepository<edu_cms_news_type> newsTypeRepo;
        private readonly IRepository<funong_news_soil_map> mapRepo;

        public IQueryable<edu_cms_news> GetNewsList(
                                        int pageCount
                                      , int currentIndex
                                      , out int totalCount
                                      , Expression<Func<edu_cms_news, bool>> where
                                      , Expression<Func<edu_cms_news, DateTime?>> orderBy
                                      , string propertyName = ""
                                      , string propertyValue = "")
        {
            IQueryable<edu_cms_news> queryObjects = null;
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            if (!string.IsNullOrEmpty(propertyName))
                queryObjects = CreateDynamicQuery<edu_cms_news>(propertyName, propertyValue, where, newsRepo);
            else
                queryObjects = newsRepo.GetMany(where);
            totalCount = queryObjects.Count();

            return queryObjects.OrderByDescending(orderBy).Skip(skipRows).Take(pageCount);
        }

        public IQueryable<edu_cms_news_type> GetNewsTypeList(
                                      int pageCount
                                    , int currentIndex
                                    , out int totalCount
                                    , Expression<Func<edu_cms_news_type, bool>> where
                                    , Expression<Func<edu_cms_news_type, int?>> orderBy
                                    , string propertyName = ""
                                    , string propertyValue = "")
        {
            IQueryable<edu_cms_news_type> queryObjects = null;
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            if (!string.IsNullOrEmpty(propertyName))
                queryObjects = CreateDynamicQuery<edu_cms_news_type>(propertyName, propertyValue, where, newsTypeRepo);
            else
                queryObjects = newsTypeRepo.GetMany(where);
            totalCount = queryObjects.Count();

            return queryObjects.OrderBy(orderBy).Skip(skipRows).Take(pageCount);
        }

        public IQueryable<funong_news_soil_map> GetNewsTypeSoilTypeList(
                                    int pageCount
                                  , int currentIndex
                                  , out int totalCount
                                  , Expression<Func<funong_news_soil_map, bool>> where
                                  , Expression<Func<funong_news_soil_map, DateTime?>> orderBy
                                  , string propertyName = ""
                                  , string propertyValue = "")
        {
            IQueryable<funong_news_soil_map> queryObjects = null;
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            if (!string.IsNullOrEmpty(propertyName))
                queryObjects = CreateDynamicQuery<funong_news_soil_map>(propertyName, propertyValue, where, mapRepo);
            else
                queryObjects = mapRepo.GetMany(where);
            totalCount = queryObjects.Count();

            return queryObjects.OrderByDescending(orderBy).Skip(skipRows).Take(pageCount);
        }
    }
}
