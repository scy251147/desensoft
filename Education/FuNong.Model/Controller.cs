﻿using FuNong.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace FuNong.Model
{
    public class Controller : Base<ds_machine_controller>
    {
        public Controller(IRepository<ds_machine_controller> controllerRepo)
        {
            this.controllerRepo = controllerRepo;
        }

        private readonly IRepository<ds_machine_controller> controllerRepo;

        public IQueryable<ds_machine_controller> GetMachineList(
                                       int pageCount
                                     , int currentIndex
                                     , out int totalCount
                                     , Expression<Func<ds_machine_controller, bool>> where
                                     , Expression<Func<ds_machine_controller, DateTime?>> orderBy
                                     , string propertyName = ""
                                     , string propertyValue = "")
        {
            IQueryable<ds_machine_controller> queryObjects = null;
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            if (!string.IsNullOrEmpty(propertyName))
                queryObjects = CreateDynamicQuery<ds_machine_controller>(propertyName, propertyValue, where, controllerRepo);
            else
                queryObjects = controllerRepo.GetMany(where);
            totalCount = queryObjects.Count();

            return queryObjects.OrderByDescending(orderBy).Skip(skipRows).Take(pageCount);
        }
    }
}
