﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using System.Linq.Expressions;

namespace FuNong.Model
{
    public class Department:Base<edu_department_detail>
    {
        public Department(IRepository<edu_department_detail> departmentRepo)
        {
            this.departmentRepo = departmentRepo;
        }

        private readonly IRepository<edu_department_detail> departmentRepo;

        public IQueryable<edu_department_detail> GetDepartmentDetails(
                                       int pageCount
                                     , int currentIndex
                                     , out int totalCount
                                     , Expression<Func<edu_department_detail, bool>> where
                                     , Expression<Func<edu_department_detail, DateTime?>> orderBy
                                     , string propertyName = ""
                                     , string propertyValue = "")
        {
            IQueryable<edu_department_detail> queryObjects = null;
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            if (!string.IsNullOrEmpty(propertyName))
                queryObjects = CreateDynamicQuery<edu_department_detail>(propertyName, propertyValue, where, departmentRepo);
            else
                queryObjects = departmentRepo.GetMany(where);
            totalCount = queryObjects.Count();

            return queryObjects.OrderByDescending(orderBy).Skip(skipRows).Take(pageCount);
        }
    }
}
