﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using System.Linq.Expressions;

namespace FuNong.Model
{
    public class Deal : Base<funong_deal>
    {
        public Deal(
                IRepository<funong_publish_main> soilMainRepo
              , IRepository<funong_publish_detail> soilDetailRepo
              , IRepository<funong_soil_around> soilAroundRepo
              , IRepository<edu_area_province> provinceRepo
              , IRepository<edu_area_city> cityRepo
              , IRepository<edu_area_district> districtRepo
              , IRepository<funong_soil_type> soilTypeRepo
              , IRepository<funong_change_type> changeTypeRepo
              , IRepository<funong_intention> intentionRepo
              , IRepository<funong_deal> dealRepo
            )
        {
            this.soilMainRepo = soilMainRepo;
            this.soilDetailRepo = soilDetailRepo;
            this.soilAroundRepo = soilAroundRepo;

            this.provinceRepo = provinceRepo;
            this.cityRepo = cityRepo;
            this.districtRepo = districtRepo;
            
            this.soilTypeRepo = soilTypeRepo;
            this.changeTypeRepo = changeTypeRepo;

            this.intentionRepo = intentionRepo;
            this.dealRepo = dealRepo;
        }

        private readonly IRepository<funong_publish_main> soilMainRepo;
        private readonly IRepository<funong_publish_detail> soilDetailRepo;
        private readonly IRepository<funong_soil_around> soilAroundRepo;

        private readonly IRepository<edu_area_province> provinceRepo;
        private readonly IRepository<edu_area_city> cityRepo;
        private readonly IRepository<edu_area_district> districtRepo;

        private readonly IRepository<funong_soil_type> soilTypeRepo;
        private readonly IRepository<funong_change_type> changeTypeRepo;

        private readonly IRepository<funong_intention> intentionRepo;
        private readonly IRepository<funong_deal> dealRepo;

        //获取土地信息主体分页列表
        public IQueryable<funong_deal> GetDealList(
                                                    int pageCount
                                                    , int currentIndex
                                                    , out int totalCount
                                                    , Expression<Func<funong_deal, bool>> where
                                                    , Expression<Func<funong_deal, DateTime?>> orderBy
                                                    , string propertyName = ""
                                                    , string propertyValue = "")
        {
            IQueryable<funong_deal> queryObjects = null;
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            if (!string.IsNullOrEmpty(propertyName))
                queryObjects = CreateDynamicQuery<funong_deal>(propertyName, propertyValue, where, dealRepo);
            else
                queryObjects = dealRepo.GetMany(where);
            totalCount = queryObjects.Count();

            return queryObjects.OrderByDescending(orderBy).Skip(skipRows).Take(pageCount);
        }
    }
}
