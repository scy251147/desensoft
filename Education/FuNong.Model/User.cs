﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using System.Linq.Expressions;
using System.Linq.Dynamic;
using FuNong.Framework.MD5;

namespace FuNong.Model
{
    public class User : Base<edu_user_detail>
    {
        public User(IRepository<edu_department_detail> departmentRepo
                  , IRepository<edu_users_view> userRepo
                  , IRepository<bma_users> mainUserRepo
                  , IRepository<edu_role_detail> roleRepo
                  , IRepository<edu_model_detail> modelRepo
                  , IRepository<edu_role_model> rolemodelRepo
                  , IRepository<edu_area_district> districtRepo
                  , IRepository<edu_area_city> cityRepo
                  , IRepository<edu_area_province> provinceRepo
                  , IRepository<funong_service_request> serviceRequestRepo
                  , IRepository<funong_publish_main> mainRepo
                   )
        {
            this.departmentRepo = departmentRepo;
            this.userRepo = userRepo;
            this.mainUserRepo = mainUserRepo;
            this.roleRepo = roleRepo;
            this.modelRepo = modelRepo;
            this.rolemodelRepo = rolemodelRepo;
            this.districtRepo = districtRepo;
            this.cityRepo = cityRepo;
            this.provinceRepo = provinceRepo;

            this.serviceRequestRepo = serviceRequestRepo;
            this.mainRepo = mainRepo;

            //初始化，防止null带来的未知错误
            UserDetailEntity = new edu_users_view();
        }

        private readonly IRepository<edu_department_detail> departmentRepo;
        private readonly IRepository<edu_users_view> userRepo;
        private readonly IRepository<bma_users> mainUserRepo;
        private readonly IRepository<edu_role_detail> roleRepo;
        private readonly IRepository<edu_model_detail> modelRepo;
        private readonly IRepository<edu_role_model> rolemodelRepo;
        private readonly IRepository<edu_area_district> districtRepo;
        private readonly IRepository<edu_area_city> cityRepo;
        private readonly IRepository<edu_area_province> provinceRepo;

        private readonly IRepository<funong_service_request> serviceRequestRepo;
        private readonly IRepository<funong_publish_main> mainRepo;

        public edu_users_view UserDetailEntity { get; set; }

        //用户所在部门
        public edu_department_detail AttachDepartment()
        {
            return departmentRepo.Get(x => x.id == UserDetailEntity.departmentid);
        }

        //用户所拥有的角色
        public edu_role_detail AttachRole()
        {
            return roleRepo.Get(x => x.id == UserDetailEntity.roleid);
        }

        //用户所拥有的可访问模块
        public IQueryable<edu_model_detail> AttachModel()
        {
            var roleModelList = rolemodelRepo.GetMany(x => x.roleid == UserDetailEntity.roleid).ToList();

            var modelIDList = new List<int>();

            roleModelList.ForEach(item =>
            {
                modelIDList.Add(item.modelid);
            });

            return modelRepo.GetMany(x => modelIDList.Contains(x.id));
        }

        //用户属于哪个区县
        public edu_area_district AttachDistrict()
        {
            //return districtRepo.Get(x => x.id == UserDetailEntity.districtid);
            return null;
        }

        //用户属于哪个城市
        public edu_area_city AttachCity()
        {
            var district = AttachDistrict();
            return cityRepo.Get(x => x.id == district.cityid);
        }

        //用户属于哪个省份
        public edu_area_province AttachProvince()
        {
            var city = AttachCity();
            return provinceRepo.Get(x => x.id == city.provinceid);
        }

        public IQueryable<funong_service_request> AttachServiceRequest(int userid)
        {
            if (userid == -1)
                return serviceRequestRepo.GetMany(x => x.clientid != 0);
            else
                return serviceRequestRepo.GetMany(x => x.clientid == userid);
        }

        public IQueryable<funong_publish_main> AttachPublishMain(int userid)
        {
            if (userid == -1)
            {
                var result = from p in mainRepo.GetMany(x => x.id != 0)
                             join x in serviceRequestRepo.GetMany(x => x.id != 0)
                             on p.requestid equals x.id
                             select p;
                return result;
            }
            else
            {
                var result = from p in mainRepo.GetMany(x => x.id != 0)
                             join x in serviceRequestRepo.GetMany(x => x.id != 0)
                             on p.requestid equals x.id
                             where x.clientid == userid
                             select p;
                return result;
            }
        }

        //根据userid值返回用户列表，pager用于确定是否进行分页返回(true,分页返回；false，全部返回)
        public IQueryable<edu_users_view> GetUserDetails(
                                        int pageCount
                                      , int currentIndex
                                      , out int totalCount
                                      , Expression<Func<edu_users_view, bool>> where
                                      , Expression<Func<edu_users_view, DateTime?>> orderBy
                                      , string propertyName = ""
                                      , string propertyValue = "")
        {
            IQueryable<edu_users_view> queryObjects = null;
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            if (!string.IsNullOrEmpty(propertyName))
                queryObjects = CreateDynamicQuery<edu_users_view>(propertyName, propertyValue, where, userRepo);
            else
                queryObjects = userRepo.GetMany(where);
            totalCount = queryObjects.Count();

            return queryObjects.OrderByDescending(orderBy).Skip(skipRows).Take(pageCount);
        }

        //根据userid值返回用户列表，pager用于确定是否进行分页返回(true,分页返回；false，全部返回)
        public IQueryable<bma_users> GetUserMain(
                                        int pageCount
                                      , int currentIndex
                                      , out int totalCount
                                      , Expression<Func<bma_users, bool>> where
                                      , Expression<Func<bma_users, DateTime?>> orderBy
                                      , string propertyName = ""
                                      , string propertyValue = "")
        {
            IQueryable<bma_users> queryObjects = null;
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            if (!string.IsNullOrEmpty(propertyName))
                queryObjects = CreateDynamicQuery<bma_users>(propertyName, propertyValue, where, mainUserRepo);
            else
                queryObjects = mainUserRepo.GetMany(where);
            totalCount = queryObjects.Count();

            return queryObjects.OrderByDescending(orderBy).Skip(skipRows).Take(pageCount);
        }

        //验证用户登陆
        public edu_users_view ValidateLogin(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))  //用户名或者密码空
                return null;

            var result = userRepo.Get(x => x.username == username); //获取用户密码等信息

            if (result != null)
            {
                var pwd = FuNong.Framework.MD5.MD5Wrapper.Md5Wrapper.CreateUserPassword(password, result.salt);
                if (result.password == pwd) //验证通过
                {
                    if (result.ischeck == 0) //未审核通过
                        return null;
                    else  //用户被审核
                    {
                        return result;
                    }
                }
                else //用户存在
                    return null;
            }
            else
                return null;
        }
    }
}
