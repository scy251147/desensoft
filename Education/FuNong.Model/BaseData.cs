﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using System.Linq.Expressions;

namespace FuNong.Model
{
    public class BaseData : Base<edu_base_setting>
    {
        public BaseData(IRepository<edu_base_setting> baseRepo,IRepository<edu_area_village> villageRepo)
        {
            this.baseRepo = baseRepo;
            this.villageRepo = villageRepo;
        }

        private readonly IRepository<edu_base_setting> baseRepo;
        private readonly IRepository<edu_area_village> villageRepo;

        public IQueryable<edu_base_setting> GetSettingList(
                                       int pageCount
                                     , int currentIndex
                                     , out int totalCount
                                     , Expression<Func<edu_base_setting, bool>> where
                                     , Expression<Func<edu_base_setting, DateTime?>> orderBy
                                     , string propertyName = ""
                                     , string propertyValue = "")
        {
            IQueryable<edu_base_setting> queryObjects = null;
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            if (!string.IsNullOrEmpty(propertyName))
                queryObjects = CreateDynamicQuery<edu_base_setting>(propertyName, propertyValue, where, baseRepo);
            else
                queryObjects = baseRepo.GetMany(where);
            totalCount = queryObjects.Count();

            return queryObjects.OrderByDescending(orderBy).Skip(skipRows).Take(pageCount);
        }

        public IQueryable<edu_area_village> GetVillageList(
                                      int pageCount
                                    , int currentIndex
                                    , out int totalCount
                                    , Expression<Func<edu_area_village, bool>> where
                                    , Expression<Func<edu_area_village, DateTime?>> orderBy
                                    , string propertyName = ""
                                    , string propertyValue = "")
        {
            IQueryable<edu_area_village> queryObjects = null;
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            if (!string.IsNullOrEmpty(propertyName))
                queryObjects = CreateDynamicQuery<edu_area_village>(propertyName, propertyValue, where, villageRepo);
            else
                queryObjects = villageRepo.GetMany(where);
            totalCount = queryObjects.Count();

            return queryObjects.OrderByDescending(orderBy).Skip(skipRows).Take(pageCount);
        }
    }
}
