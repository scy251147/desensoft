﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using System.Linq.Expressions;

namespace FuNong.Model
{
    public class SoilPublish : Base<funong_publish_main>
    {
        public SoilPublish(
                IRepository<funong_publish_main> soilMainRepo
              , IRepository<funong_publish_detail> soilDetailRepo
              , IRepository<funong_soil_around> soilAroundRepo
              , IRepository<edu_area_province> provinceRepo
              , IRepository<edu_area_city> cityRepo
              , IRepository<edu_area_district> districtRepo
              , IRepository<funong_soil_type> soilTypeRepo
              , IRepository<funong_change_type> changeTypeRepo
              , IRepository<funong_intention> intentionRepo
            )
        {
            this.soilMainRepo = soilMainRepo;
            this.soilDetailRepo = soilDetailRepo;
            this.soilAroundRepo = soilAroundRepo;

            this.provinceRepo = provinceRepo;
            this.cityRepo = cityRepo;
            this.districtRepo = districtRepo;
            
            this.soilTypeRepo = soilTypeRepo;
            this.changeTypeRepo = changeTypeRepo;

            this.intentionRepo = intentionRepo;
        }

        private readonly IRepository<funong_publish_main> soilMainRepo;
        private readonly IRepository<funong_publish_detail> soilDetailRepo;
        private readonly IRepository<funong_soil_around> soilAroundRepo;

        private readonly IRepository<edu_area_province> provinceRepo;
        private readonly IRepository<edu_area_city> cityRepo;
        private readonly IRepository<edu_area_district> districtRepo;

        private readonly IRepository<funong_soil_type> soilTypeRepo;
        private readonly IRepository<funong_change_type> changeTypeRepo;

        private readonly IRepository<funong_intention> intentionRepo;

        //获取土地信息主体分页列表
        public IQueryable<funong_publish_main> GetSoilMainList(
                                                                int pageCount
                                                              , int currentIndex
                                                              , out int totalCount
                                                              , Expression<Func<funong_publish_main, bool>> where
                                                              , Expression<Func<funong_publish_main, DateTime?>> orderBy
                                                              , string propertyName = ""
                                                              , string propertyValue = "")
        {
            IQueryable<funong_publish_main> queryObjects = null;
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            if (!string.IsNullOrEmpty(propertyName))
                queryObjects = CreateDynamicQuery<funong_publish_main>(propertyName, propertyValue, where, soilMainRepo);
            else
                queryObjects = soilMainRepo.GetMany(where);
            totalCount = queryObjects.Count();

            return queryObjects.OrderByDescending(orderBy).Skip(skipRows).Take(pageCount);
        }

        //获取意向受让人分页列表
        public IQueryable<funong_intention> GetIntentionList(
                                                                int pageCount
                                                              , int currentIndex
                                                              , out int totalCount
                                                              , Expression<Func<funong_intention, bool>> where
                                                              , Expression<Func<funong_intention, DateTime?>> orderBy
                                                              , string propertyName = ""
                                                              , string propertyValue = "")
        {
            IQueryable<funong_intention> queryObjects = null;
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            if (!string.IsNullOrEmpty(propertyName))
                queryObjects = CreateDynamicQuery<funong_intention>(propertyName, propertyValue, where, intentionRepo);
            else
                queryObjects = intentionRepo.GetMany(where);
            totalCount = queryObjects.Count();

            return queryObjects.OrderByDescending(orderBy).Skip(skipRows).Take(pageCount);
        }

        //获取省份列表
        public IQueryable<edu_area_province> AttachProvince()
        {
            IQueryable<edu_area_province> provinceQuery = provinceRepo.GetMany(x => x.id != 0);
            return provinceQuery;
        }

        //获取省份下的市区列表
        public IQueryable<edu_area_city> AttachCityByProvinceID(int provinceID)
        {
            IQueryable<edu_area_city> cityQuery = from p in provinceRepo.GetMany(x => x.id != 0)
                                              join q in cityRepo.GetMany(x => x.id != 0) on p.id equals q.provinceid
                                              where p.id == provinceID
                                              select q;
            return cityQuery;
        }

        //获取市/区下面的区县列表
        public IQueryable<edu_area_district> AttachDistrictByCityID(int cityID)
        {
            IQueryable<edu_area_district> districtQuery = from p in districtRepo.GetMany(x => x.id != 0)
                                                      join q in cityRepo.GetMany(x => x.id != 0) on p.cityid equals q.id
                                                      where q.id == cityID
                                                      select p;
            return districtQuery;
        }

        //获取土地类型 (农业地商用地)
        public IQueryable<funong_soil_type> AttachSoilType()
        {
            return soilTypeRepo.GetMany(x => x.id != 0);
        }

        //获取流转方式 (转包，招拍挂等)
        public IQueryable<funong_change_type> AttachChangeType()
        {
            return changeTypeRepo.GetMany(x => x.id != 0);
        }

        //获取土地周边
        public IQueryable<funong_soil_around> AttachSoilAround(string flag)
        {
            return soilAroundRepo.GetMany(x => x.aroundgroup == flag);
        }

        //首页展示的土地数据
        public Dictionary<funong_soil_type, List<funong_publish_main>> TriggerSoilIndex()
        {

            var dict = new Dictionary<funong_soil_type, List<funong_publish_main>>();

            var soilTypeParent = AttachSoilType().Where(x => x.parentid == 0).ToList();
            int totalCount;

            if (soilTypeParent != null)
            {
                soilTypeParent.ForEach(item =>
                {
                    var soilTypeChild = soilTypeRepo.GetMany(x => x.parentid == item.id).ToList();
                    var soilTypeChildList = new List<int>();

                    soilTypeChild.ForEach(typechild =>
                    {
                        soilTypeChildList.Add(typechild.id);
                    });

                    var soilMain = GetSoilMainList(8, 0, out totalCount, x => soilTypeChildList.Contains(x.soiltypeid) && x.isapprove == 1, x => x.updatetime, string.Empty, string.Empty).ToList();
                    dict.Add(item, soilMain);
                });
            }

            return dict;
        }
    }
}
