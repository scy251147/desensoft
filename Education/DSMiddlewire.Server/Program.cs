﻿using Autofac;
using DSMiddlewire.Lib;
using FuNong.DataAccess;
using FuNong.Framework.Caching;
using FuNong.Framework.Logger;
using FuNong.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DSMiddlewire.Server
{
    class Program
    {
        public Program()
        {
            if (loggerService == null)
                loggerService = new LoggerService();
        }

        private static ILoggerService loggerService ;
        static void Main(string[] args)
        {
            Console.Title = "中间件转发平台V1.0©鹤壁德森科技有限公司";

            try
            {
                //处理未捕获的异常
                Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
                //处理UI线程异常
                Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
                //处理非UI线程异常
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

                PStart();
            }
            catch (Exception ex)
            {
                string str = "";
                string strDateInfo = "出现应用程序未处理的异常：" + DateTime.Now.ToString() + "\r\n";

                if (ex != null)
                {
                    str = string.Format(strDateInfo + "异常类型：{0}\r\n异常消息：{1}\r\n异常信息：{2}\r\n",
                    ex.GetType().Name, ex.Message, ex.StackTrace);
                }
                else
                {
                    str = string.Format("应用程序线程错误:{0}", ex);
                }

                loggerService.Error(str);
            }

            while (true)
            {
                string command = Console.ReadLine();
                if(command=="exit")
                {
                    return;
                }
            }
        }

        static void PStart()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<MemoryCacheManager>().As<ICacheManager>().SingleInstance();
            builder.RegisterType<LoggerService>().As<ILoggerService>().SingleInstance();
            builder.RegisterType<FuNongContext>().As<IFuNongContext>().SingleInstance();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().SingleInstance();
            builder.RegisterType<BaseService<ds_collector_data>>().As<IBaseService<ds_collector_data>>().SingleInstance();
            builder.RegisterType<ConsoleLogWithColor>().SingleInstance();

            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>)).SingleInstance();

            builder.RegisterType<DSCollectorService>().As<IDSCollectorService>().SingleInstance();
            builder.RegisterType<DSControllerService>().As<IDSControllerService>().SingleInstance();
            builder.RegisterType<DeviceService>().As<IDeviceService>().SingleInstance();

            builder.RegisterType<DSServer>().SingleInstance();

            var container = builder.Build();

            //启动实例
            container.Resolve<DSServer>();
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            string str = "";
            string strDateInfo = "出现应用程序未处理的异常：" + DateTime.Now.ToString() + "\r\n";
            Exception error = e.Exception as Exception;
            if (error != null)
            {
                str = string.Format(strDateInfo + "异常类型：{0}\r\n异常消息：{1}\r\n异常信息：{2}\r\n",
                error.GetType().Name, error.Message, error.StackTrace);
            }
            else
            {
                str = string.Format("应用程序线程错误:{0}", e);
            }

            loggerService.Error(str);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            string str = "";
            Exception error = e.ExceptionObject as Exception;
            string strDateInfo = "出现应用程序未处理的异常：" + DateTime.Now.ToString() + "\r\n";
            if (error != null)
            {
                str = string.Format(strDateInfo + "Application UnhandledException:{0};\n\r堆栈信息:{1}", error.Message, error.StackTrace);
            }
            else
            {
                str = string.Format("Application UnhandledError:{0}", e);
            }

            loggerService.Error(str);
        }

    }
}
