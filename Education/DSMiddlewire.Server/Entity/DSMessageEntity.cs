﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSMiddlewire.Server
{
    //封包格式： dst|数据来源(手机，PC，硬件等)| 功能码|硬件id|数据内容|延时时间|crc
    public class DSMessageEntity : ICloneable
    {
        //信息头
        public string MessageHeader { get; set; }

        //数据来源
        public CommandSource MessageSource { get; set; }

        //功能码
        public MessageType FunctionCode { get; set; }

        //硬件id
        public string MachineID { get; set; }

        //数据内容
        public string MessageContent { get; set; }

        //数据内容扩展部分，可能是延时时间，可能是采集数据等
        public string MessageContentAppendix { get; set; }

        //复制
        public object Clone()
        {
            return new DSMessageEntity()
            {
                MessageHeader = MessageHeader,
                MessageSource = MessageSource,
                FunctionCode = FunctionCode,
                MachineID = MachineID,
                MessageContent = MessageContent,
                MessageContentAppendix = MessageContentAppendix
            };
        }
    }
}
