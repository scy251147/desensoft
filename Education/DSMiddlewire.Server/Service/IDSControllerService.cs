﻿using System;
using System.Net.Sockets;

namespace DSMiddlewire.Server
{
    public interface IDSControllerService
    {
        //处理来自于用户发送的控制命令
        void ProcessCommandFromWeb(DSMessageEntity cmdFromWeb, Socket hardwareSocket);
        //处理来自于硬件终端的上报命令
        void ProcessCommandFromHardware(DSMessageEntity cmdFromHardware, Socket webSocket);
        //处理来自于硬件端的透传命令
        void ProcessBullteFromHardware(DSMessageEntity cmdFromHardware,Socket hardwareSocket);
    }
}
