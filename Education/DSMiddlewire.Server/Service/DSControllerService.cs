﻿using DSMiddlewire.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

/*名称：控制器
 *描述：负责控制数据的解析
 */

namespace DSMiddlewire.Server
{
    public class DSControllerService : IDSControllerService
    {
        public DSControllerService(ConsoleLogWithColor logConsole)
        {
            this.logConsole = logConsole;
        }

        private ConsoleLogWithColor logConsole;

        //处理来自于Web端的控制命令
        public  void ProcessCommandFromWeb(DSMessageEntity cmdFromWeb, Socket hardwareSocket)
        {
            //dst|0|11|ds0001|1x0|0003|crc
            var builder = new StringBuilder();
            builder.Append(cmdFromWeb.MessageHeader).Append("|");
            builder.Append((int)CommandSource.MiddleWare).Append("|");
            builder.Append(((int)cmdFromWeb.FunctionCode).ToString().PadLeft(3, '0')).Append("|");
            builder.Append(cmdFromWeb.MachineID).Append("|");
            builder.Append(cmdFromWeb.MessageContent).Append("|");
            builder.Append(cmdFromWeb.MessageContentAppendix);

            var cmdPackage = CrcChcek.ConstructMessageWithCRC(builder.ToString());

            if (hardwareSocket==null)
            {
                logConsole.WriteLine("未找到当前硬件连接,放弃下发控制命令!", ConsoleColor.DarkRed);
                return;
            }
            try
            {
                hardwareSocket.Send(cmdPackage);
                if (cmdFromWeb.FunctionCode == MessageType.RequestLed)
                {
                    logConsole.WriteLine("↓↓↓↓    下发请求指示灯状态命令成功    ↓↓↓↓", ConsoleColor.Green);
                }
                if(cmdFromWeb.FunctionCode == MessageType.RequestSwitch)
                {
                    logConsole.WriteLine("↓↓↓↓    下发请求开关量状态命令成功    ↓↓↓↓", ConsoleColor.Green);
                }
                if(cmdFromWeb.FunctionCode== MessageType.SwitchTransfer)
                {
                    logConsole.WriteLine("↓↓↓↓↓↓↓↓    下发控制命令成功    ↓↓↓↓↓↓↓↓", ConsoleColor.Green);
                }
                
            }
            catch(SocketException se)
            {
                logConsole.WriteLine(se.Message,ConsoleColor.Red);
            }
        }

        //处理来自于硬件的状态上报命令
        public void ProcessCommandFromHardware(DSMessageEntity cmdFromHardware, Socket webSocket)
        {
            var builder = new StringBuilder();
            builder.Append(cmdFromHardware.MessageHeader).Append("|");
            builder.Append((int)CommandSource.MiddleWare).Append("|");
            builder.Append(((int)cmdFromHardware.FunctionCode).ToString().PadLeft(3, '0')).Append("|");
            builder.Append(cmdFromHardware.MachineID).Append("|");
            builder.Append(cmdFromHardware.MessageContent).Append("|");
            builder.Append(cmdFromHardware.MessageContentAppendix);

            var cmdPackage = CrcChcek.ConstructMessageWithCRC(builder.ToString());

            if (webSocket == null)
            {
                logConsole.WriteLine("未找到当前用户端连接,放弃上传控制状态!", ConsoleColor.DarkRed);
                return;
            }
            try
            {
                webSocket.Send(cmdPackage);
                logConsole.WriteLine("↑↑↑↑↑↑↑↑    上传控制状态成功    ↑↑↑↑↑↑↑↑", ConsoleColor.Green);
            }
            catch (SocketException se)
            {
                logConsole.WriteLine(se.Message, ConsoleColor.Red);
            }
        }

        //处理来自于硬件端的透传命令
        public void ProcessBullteFromHardware(DSMessageEntity cmdFromHardware, Socket hardwareSocket)
        {
            //dst|0|11|ds0001|1x0|0003|crc
            var builder = new StringBuilder();
            builder.Append(cmdFromHardware.MessageHeader).Append("|");
            builder.Append((int)CommandSource.MiddleWare).Append("|");
            builder.Append(((int)MessageType.BulletReceive).ToString().PadLeft(3, '0')).Append("|");
            builder.Append(cmdFromHardware.MachineID).Append("|");
            builder.Append(cmdFromHardware.MessageContent).Append("|");
            builder.Append(cmdFromHardware.MessageContentAppendix);

            var cmdPackage = CrcChcek.ConstructMessageWithCRC(builder.ToString());

            if (hardwareSocket == null)
            {
                logConsole.WriteLine("未找到待透传的硬件连接,放弃下发透传命令!", ConsoleColor.DarkRed);
                return;
            }
            try
            {
                hardwareSocket.Send(cmdPackage);
                logConsole.WriteLine("↓↓↓↓↓↓↓↓    下发透传命令成功    ↓↓↓↓↓↓↓↓", ConsoleColor.Green);

            }
            catch (SocketException se)
            {
                logConsole.WriteLine(se.Message, ConsoleColor.Red);
            }
        }
    }
}
