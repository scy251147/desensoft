﻿using FuNong.DataAccess;
using FuNong.DataContract;
using FuNong.Framework.Caching;
using FuNong.Framework.Logger;
using FuNong.Service;
using FuNong.UserInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSMiddlewire.Server
{
    public class DeviceService:IDeviceService
    {
        public DeviceService(IUnitOfWork unitOfWork
                            , ICacheManager cacheManager
                            , ILoggerService loggerService
                            , IRepository<edu_school> companyRepo
                            ,IRepository<edu_users_view> userRepo
                            ,IRepository<edu_base_setting> settingRepo
                            ,IRepository<ds_machine_list> machineRepo
                            )
        {
            this.unitOfWork = unitOfWork;
            this.cacheManager = cacheManager;
            this.loggerService = loggerService;

            this.companyRepo = companyRepo;
            this.userRepo = userRepo;
            this.settingRepo = settingRepo;
            this.machineRepo = machineRepo;

        }

        private IUnitOfWork unitOfWork;
        private ICacheManager cacheManager;
        private ILoggerService loggerService;
        private IRepository<edu_school> companyRepo;
        private IRepository<edu_users_view> userRepo;
        private IRepository<edu_base_setting> settingRepo;
        private IRepository<ds_machine_list> machineRepo;
        public List<controller> GetAllMachineDetails()
        {
            var deviceService = new MachineService(unitOfWork,cacheManager,loggerService,companyRepo,userRepo,settingRepo,machineRepo);
            
            var request = new MachineRequest();
            request.pageCount = Int32.MaxValue;
            request.currentIndex = 0;
            request.orderBy = x => x.updatetime;
            request.where = x => x.machine_id != "";

            var controllerAll = new List<controller>();

            //获取设备列表
            var machineList = deviceService.TriggerList(request);

            if (machineList != null)
            {
                //获取所有的设备ID
                var machineIDList = machineList.Select(x => x.machine_id);

                var cRequest = new ControllerRequest();
                cRequest.pageCount = Int32.MaxValue;
                cRequest.currentIndex = 0;
                cRequest.orderBy = x => x.updatetime;
                cRequest.where = x => machineIDList.Contains(x.machine_id);

                //获取到了所有的控制设备详细信息
                var controllerList = deviceService.TriggerList(cRequest);

                if (controllerList != null)
                {
                    //遍历设备信息表
                    machineList.ForEach(parent =>
                    {
                        var controllerEntity = new controller();
                        controllerEntity.title = parent.machine_name;
                        controllerEntity.machineid = parent.machine_id;
                        controllerEntity.routers = new List<router>();
                        controllerEntity.leds = new List<led>();
                        //遍历控制设备详细信息表
                        controllerList.ForEach(child =>
                        {
                            //如果为同一设备
                            if (parent.machine_id == child.machine_id)
                            {
                                router router = null;
                                led led = null;

                                if (child.machine_type == 3)
                                {
                                    router = new router();
                                    //可控制路数
                                    router.routeid = child.route_id;
                                    router.title = child.route_name;
                                    router.functionsList = new List<functions>();
                                }
                                if (child.machine_type == 4)
                                {
                                    led = new led();
                                    //指示灯路数
                                    led.routerid = child.route_id;
                                    led.title = child.route_name;
                                    led.functionsList = new List<functions>();
                                }

                                #region 拆分每个router上面的功能
                                var rMul = child.route_multiple;
                                if (rMul.Contains('|'))
                                {
                                    var rMulList = child.route_multiple.Split('|').ToList();
                                    rMulList.ForEach(item =>
                                    {
                                        var functions = new functions();
                                        if (item.Contains(","))
                                        {
                                            var currentFuncList = item.Split(',').ToList();
                                            functions.functionflag = currentFuncList[0];
                                            functions.functionname = currentFuncList[1];
                                            functions.functionicon = currentFuncList[2];

                                            functions.functionorder = "1";
                                            functions.state = false;

                                            if (child.machine_type == 3) router.functionsList.Add(functions);

                                            if (child.machine_type == 4) led.functionsList.Add(functions);
                                        }
                                        else
                                        {
                                            //配置有问题 
                                            //TODO
                                        }
                                    });

                                    if (child.machine_type == 3) controllerEntity.routers.Add(router);

                                    if (child.machine_type == 4) controllerEntity.leds.Add(led);
                                }
                                else
                                {
                                    //配置有问题
                                    //TODO
                                }
                                #endregion
                            }
                        });

                        controllerAll.Add(controllerEntity);
                    });
                }
            }

            return controllerAll;
        }
    }
}
