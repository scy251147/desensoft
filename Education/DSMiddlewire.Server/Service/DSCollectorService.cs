﻿using DSMiddlewire.Lib;
using FuNong.DataAccess;
using FuNong.Framework;
using FuNong.Framework.Caching;
using FuNong.Framework.Logger;
using FuNong.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*名称：采集器
 *描述：负责采集数据的解析
 */

namespace DSMiddlewire.Server
{
    public class DSCollectorService : IDSCollectorService
    {
        public DSCollectorService(IUnitOfWork unitOfWork
                                , ICacheManager cacheManager
                                , ILoggerService loggerService
                                , IBaseService<ds_collector_data> dataService
                                , ConsoleLogWithColor logConsole)
        {
            this.unitOfWork = unitOfWork;
            this.cacheManager = cacheManager;
            this.loggerService = loggerService;
            this.dataService = dataService;
            this.logConsole = logConsole;

        }

        private IUnitOfWork unitOfWork;
        private ICacheManager cacheManager;
        private ILoggerService loggerService;
        private IBaseService<ds_collector_data> dataService;
        private ConsoleLogWithColor logConsole;

        public  void ProcessCommandFromCollector(DSMessageEntity message
                                                    
                                                      )
        {
            //设备编号
            var machineID = message.MachineID;
            //参数编号
            var paramID = message.MessageContent;

            var dataPart = message.MessageContentAppendix;
            var dataSlice = dataPart.Split('*');

            //设备路数
            var routeID = Int32.Parse(dataSlice[0]);

            //参数数值
            var data = dataSlice[1];

            //开始入库
            var collectorEntity = new ds_collector_data();
            collectorEntity.id = Guid.NewGuid().ToString();
            collectorEntity.data_isvalid = 1;
            collectorEntity.data_time = DateTime.Now;
            collectorEntity.updatetime = DateTime.Now;
            collectorEntity.machine_id = machineID;
            collectorEntity.route_id = routeID;
            collectorEntity.data_value = data;
            collectorEntity.recordorder = 1;
            collectorEntity.param_id = paramID;

            var list = new List<ds_collector_data>();
            list.Add(collectorEntity);
            try
            {
                dataService.AddInBatch(list);
                unitOfWork.Commit();
            }
            catch(Exception ex)
            {
                logConsole.WriteLine("采集数据插入失败:" + ex.Message, ConsoleColor.Red);
            }
        }
    }
}
