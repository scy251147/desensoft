﻿var pageSize = 18;
var pageList = [18, 30, 50];

var addFlag = false;
var editFlag = false;
var deleteFlag = false;
var refreshFlag = false;
var exportFlag = false;
var printFlag = false;

$(document).ready(function () {
    $('#dg').datagrid({
        url: DataURL,
        singleSelect: true,
        height: 'auto',
        nowrap: false,
        striped: true,
        border: true,
        collapsible: true,
        fit: true,
        singleSelect: false,
        pagination: true, //分页控件   
        pageSize: pageSize, //每页显示的记录条数
        pageList: pageList, //可以设置每页记录条数的列表   
        method: 'get',
        //onClickRow:function(rowIndex, rowData){}
        rownumbers: true, //行号   
        toolbar: [ {
            text: '刷新',
            iconCls: 'icon-reload',
            disabled: refreshFlag,
            handler: function () {
                $('#dg').datagrid('reload');
            }
        }, '-', {
            text: '导出',
            iconCls: 'icon-export',
            disabled: exportFlag,
            handler: function () {
                ExportRecords();
            }
        }, '-', {
            text: '打印',
            iconCls: 'icon-print',
            disabled: printFlag,
            handler: function () {
                PrintRecords();
            }
        }],
        columns: ColumnData
    });

    //设置分页控件   
    var p = $('#dg').datagrid('getPager');
    $(p).pagination({
        beforePageText: '第', //页数文本框前显示的汉字   
        afterPageText: '页    共 {pages} 页',
        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录'
    });
    AddSearchBar();
});

var AddSearchBar = function () {
    var fields = $('#dg').datagrid('getColumnFields');
    for (var i = 0; i < fields.length; i++) {
        var opts = $('#dg').datagrid('getColumnOption', fields[i]);
        if (opts.title != undefined) {
            var muit = "<div name='" + fields[i] + "'>" + opts.title + "</div>";
            $('#TypeSelect').html($('#TypeSelect').html() + muit);
        }
    }
    $('.searchbox').appendTo('.datagrid-toolbar').css("margin-top", "3px");
    $('#ValueInput').appendTo('.datagrid-toolbar');
    $('<br/><br/>').appendTo('.datagrid-toolbar');
    $('#city_4').appendTo('.datagrid-toolbar');
    $('#jd').appendTo('.datagrid-toolbar');
    $('#Span1').appendTo('.datagrid-toolbar');
    $('#zwlx').appendTo('.datagrid-toolbar');
    $('#Span2').appendTo('.datagrid-toolbar');
    $('#jcd').appendTo('.datagrid-toolbar');
    $('#Span3').appendTo('.datagrid-toolbar');
    $('#sdfsd').appendTo('.datagrid-toolbar');
    $('#sst').appendTo('.datagrid-toolbar');
    $(".datagrid-toolbar table").css("float", "left");   //让SearchBox在同一行显示的关键语句
    $('#ValueInput').searchbox({
        menu: '#TypeSelect',
        prompt: '请输入待查询的值',
        searcher: function (value, name) {
            if (value == "") {
                alert("无输入值，请重试！");
            }
            else {
                alert(name + ":" + value);
            }
        }
    });
}

var formatDateTimeJSON = function (value) {
    if (value == null || value == '') {
        return '';
    }
    var dt;
    if (value instanceof Date) {
        dt = value;
    }
    else {
        dt = new Date(value);
        if (isNaN(dt)) {
            value = value.replace(/\/Date\((-?\d+)\)\//, '$1'); //标红的这段是关键代码，将那个长字符串的日期值转换成正常的JS日期格式
            dt = new Date();
            dt.setTime(value);
        }
    }
    return dt.getFullYear() + "年" + (dt.getMonth() + 1) + "月" + dt.getDate() + "日";   //这里用到一个javascript的Date类型的拓展方法，这个是自己添加的拓展方法，在后面的步骤3定义
}

var Messager = function (title, msg) {
    $.messager.show({    // show error message
        title: title,
        msg: msg,
        showType: 'fade',
        style: {
            right: '',
            bottom: ''
        }
    });
}