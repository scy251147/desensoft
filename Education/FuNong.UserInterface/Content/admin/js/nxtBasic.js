﻿/*
作者：常领峰
日期：2013年11月11日16:25:38
功能：nxt js基础类
*/
function getQueryString() {
    var url = location.search; //获取url中"?"符后的字串 
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        if (str.indexOf("&") != -1) {
            strs = str.split("&");
            for (var i = 0; i < strs.length; i++) {
                theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
            }
        } else {
            theRequest[str.split("=")[0]] = unescape(str.split("=")[1]);
        }
    }
    return theRequest; 

}
function OnlyDouble(str) {
    return /^[0-9]+\.{0,1}[0-9]*$/.test(str);
}
function OnlyInt(str)
{
    //return /^[0-9]*$/.test(str);
    return /^-?\d+$/.test(str);
   
}
function isMobilePhone(str) {
    var reg = /^(\d{11})$/;
    return reg.test(str);
}