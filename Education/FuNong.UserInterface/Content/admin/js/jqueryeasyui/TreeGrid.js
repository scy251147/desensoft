﻿var pageSize = 20;
var pageList = [20, 30, 50];

var addFlag = false;
var editFlag = false;
var deleteFlag = false;
var saveFlag = false;
var refreshFlag = false;
var exportFlag = false;
var printFlag = false;

var idField = "";
var treeField = "";

var method = 'post';

$(document).ready(function () {
    BindDataGrid();
});

var BindDataGrid = function () {
    $('#dg').treegrid({
        url: DataURL,
        idField: idField,
        treeField: treeField,
        height: 'auto',
        nowrap: false,
        striped: true,
        border: true,
        collapsible: true,
        fit: true,
        fitColumns: true,
        singleSelect: true,
        pagination: true, //分页控件   
        pageSize: pageSize, //每页显示的记录条数
        pageList: pageList, //可以设置每页记录条数的列表   
        method: method,
        rownumbers: true, //行号   
        toolbar: [{
            text: '添加',
            iconCls: 'icon-add',
            disabled: addFlag,
            handler: function () {
                AddRecords();
            }
        }, '-', {
            text: '修改',
            iconCls: 'icon-edit',
            disabled: editFlag,
            handler: function () {
                UpdateRecords();
            }
        }, '-', {
            text: '删除',
            iconCls: 'icon-remove',
            disabled: deleteFlag,
            handler: function () {
                DeleteRecords();
            }
        }, '-', {
            text: '保存',
            iconCls: 'icon-save',
            disabled: saveFlag,
            handler: function () {
                SaveRecords();
            }
        }, '-', {
            text: '刷新',
            iconCls: 'icon-reload',
            disabled: deleteFlag,
            handler: function () {
                ReloadRecords();
            }
        }],
        columns: ColumnData
    });

    //设置分页控件   
    //    var p = $('#dg').datagrid('getPager');
    //    $(p).pagination({
    //        beforePageText: '第', //页数文本框前显示的汉字   
    //        afterPageText: '页    共 {pages} 页',
    //        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录'
    //    });
    // AddSearchBar();
}

var AddSearchBar = function () {
    var fields = $('#dg').datagrid('getColumnFields');
    for (var i = 0; i < fields.length; i++) {
        var opts = $('#dg').datagrid('getColumnOption', fields[i]);
        if (opts.title != undefined) {
            var muit = "<div name='" + fields[i] + "'>" + opts.title + "</div>";
            $('#TypeSelect').html($('#TypeSelect').html() + muit);
        }
    }
    $('.searchbox').appendTo('.datagrid-toolbar').css("margin-top", "3px");
    $('#ValueInput').appendTo('.datagrid-toolbar');
    $(".datagrid-toolbar table").css("float", "left");   //让SearchBox在同一行显示的关键语句
    $('#ValueInput').searchbox({
        menu: '#TypeSelect',
        prompt: '请输入待查询的值',
        searcher: function (value, name) {
            if (value == "") {
                alert("无输入值，请重试！");
            }
            else {
                //alert(name + ":" + value);
                QueryRecords(value, name);
            }
        }
    });
}

var formatDateTimeJSON = function (value) {
    if (value == null || value == '') {
        return '';
    }
    var dt;
    if (value instanceof Date) {
        dt = value;
    }
    else {
        dt = new Date(value);
        if (isNaN(dt)) {
            value = value.replace(/\/Date\((-?\d+)\)\//, '$1'); //标红的这段是关键代码，将那个长字符串的日期值转换成正常的JS日期格式
            dt = new Date();
            dt.setTime(value);
        }
    }
    return dt.getFullYear() + "年" + (dt.getMonth() + 1) + "月" + dt.getDate() + "日";   //这里用到一个javascript的Date类型的拓展方法，这个是自己添加的拓展方法，在后面的步骤3定义
}

var formatCurrentDateTime = function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    var hh = today.getHours();
    var mmm = today.getMinutes();
    var sec = today.getSeconds();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd + ' ' + hh + ":" + mmm + ":" + sec;
    return today;
}

var Messager = function (title, msg) {
    $.messager.show({    // show error message
        title: title,
        msg: msg,
        showType: 'fade',
        style: {
            right: '',
            bottom: ''
        }
    });
}


var editIndex = undefined;
function endEditing() {
    if (editIndex == undefined) { return true }
    if ($('#dg').datagrid('validateRow', editIndex)) {
        var ed = $('#dg').datagrid('getEditor', { index: editIndex, field: 'ID' });
        var appName = $(ed.target).text('getText');
        $('#dg').datagrid('getRows')[editIndex]['App_Name'] = appName;
        $('#dg').datagrid('endEdit', editIndex);
        editIndex = undefined;
        return true;
    } else {
        return false;
    }
}

function endEdit() {
    var rows = $('#dg').datagrid('getRows');
    for (var i = 0; i < rows.length; i++) {
        $('#dg').datagrid('endEdit', i);
    }
}

