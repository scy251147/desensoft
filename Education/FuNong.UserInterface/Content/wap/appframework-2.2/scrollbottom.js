  /*榜单*/
  var bangdanScroller;
  var bangdanPage=2;
	$.ui.ready(function () {
		bangdanScroller = $("#bangdan_panel").scroller(); //Fetch the scroller from cache
		//Since this is a App Framework UI scroller, we could also do
		// bangdanScroller=$.ui.scrollingDivs['webslider'];
		bangdanScroller.addInfinite();
		bangdanScroller.addPullToRefresh();
		bangdanScroller.runCB=true;
		
		/*下拉的时候触发的事件*/
		var hideClose;
		$.bind(bangdanScroller, "refresh-release", function () {
			var that = this;
			console.log("Refresh release");
			clearTimeout(hideClose);
			hideClose = setTimeout(function () {
				console.log("hiding manually refresh");
				that.hideRefresh();
			}, 500);
			return false; //tells it to not auto-cancel the refresh
		});	
	
		bangdanScroller.enable();
		
		//解决 一个page可能请求多次的问题
		var flagBangdanScroller=true;		
	
		/*上拉的时候触发的事件*/
		$.bind(bangdanScroller, "infinite-scroll", function () {
			var self = this;
			console.log("infinite triggered");
			/*上拉更新加载多次*/
			if($(self.el).find("#infinite1").length==0){
         	 $(this.el).append("<div id='infinite1' style='border:1px solid gray;margin-top:10px;width:100%;height:30px;line-height:30px;text-align:center;'>加载中，请稍候...</div>");
     	   }
			
			
			$.bind(bangdanScroller, "infinite-scroll-end", function () {
				$.unbind(bangdanScroller, "infinite-scroll-end");
				//self.scrollToBottom();
				
				$(self.el).find("#infinite1").remove();
				
				
				if(!flagBangdanScroller) return;
				
				flagBangdanScroller=false;
				
				
				/*请求服务器加载数据*/
				
				var bangdan_list='';
				$.ajax({
				    //url: 'http://www.phonegap100.com/appapi.php?a=getThreadList&fid=2&page='+bangdanPage+'&callback=?',
				    url: 'GetNewsList?page='+bangdanPage,
					success: function(data) {

					    for (var i = 0; i < data.length; i++) {
					        bangdan_list += '<li><a href="#" class="icon heart">' + data[i].newstitle + '  <span style="color:gray;font-size:12px;">[' + formatDateTimeJSON(data[i].updatetime) + ']</span></a> </li>';
						}
						$("#bangdan_list").append(bangdan_list);
						bangdanPage++;			
									
						flagBangdanScroller=true;
						
						self.clearInfinite();
										
					}
				});	
				
				
				/*请求服务器加载数据 完成*/
								
				
			});
		});				
	});
	
	
	
  /*分类*/
var fenleiScroller;
var fenleiPage=2;
$.ui.ready(function () {
	fenleiScroller = $("#fenlei_panel").scroller(); //Fetch the scroller from cache
	//Since this is a App Framework UI scroller, we could also do
	// fenleiScroller=$.ui.scrollingDivs['webslider'];
	fenleiScroller.addInfinite();
	fenleiScroller.addPullToRefresh();
	fenleiScroller.runCB=true;
	
	/*下拉的时候触发的事件*/
	var hideClose;
	$.bind(fenleiScroller, "refresh-release", function () {
		var that = this;
		console.log("Refresh release");
		clearTimeout(hideClose);
		hideClose = setTimeout(function () {
			console.log("hiding manually refresh");
			that.hideRefresh();
		}, 500);
		return false; //tells it to not auto-cancel the refresh
	});	

	fenleiScroller.enable();
	//解决 一个page可能请求多次的问题
	var flagFenleiScroller=true;		
	
	/*上拉的时候触发的事件*/
	$.bind(fenleiScroller, "infinite-scroll", function () {
		var self = this;
		console.log("infinite triggered");
		/*出现多次Fetching content*/
		if($(self.el).find("#infinite2").length==0){
		    $(this.el).append("<div id='infinite2' style='border:1px solid gray;margin-top:10px;width:100%;height:30px;line-height:30px;text-align:center;'>加载中，请稍候...</div>");
        }
		
		$.bind(fenleiScroller, "infinite-scroll-end", function () {
			$.unbind(fenleiScroller, "infinite-scroll-end");
			//self.scrollToBottom();
			
			$(self.el).find("#infinite2").remove();
			
			//解决 一个page可能请求多次的问题
			if(!flagFenleiScroller) return;
			//解决 一个page可能请求多次的问题
			flagFenleiScroller=false;
				
			
			/*请求服务器加载数据*/
				var fenlei_list='';
				$.ajax({
				    //url: 'http://www.phonegap100.com/appapi.php?a=getThreadList&fid=44&page='+fenleiPage+'&callback=?',
				    url: 'GetNewsType?page=' + fenleiPage,
					success: function(data) {
					    for (var i = 0; i < data.length; i++) {
					        fenlei_list += '<li><a href="#" class="icon tv">' + data[i].typename + '</a></li>';
						}
						$("#fenlei_list").append(fenlei_list);	
						fenleiPage++;	
						flagFenleiScroller=true;
						self.clearInfinite();				
					}
				});	
			
			
			
			/*请求服务器加载数据 完成*/
							
			
		});
	});
});


/*新闻类别*/
var typeScroller;
var typePage = 2;
var currentTypeid = 0;
$.ui.ready(function () {
    //Fetch the scroller from cache
    typeScroller = $("#type_panel").scroller(); 
    typeScroller.addInfinite();
    typeScroller.addPullToRefresh();
    typeScroller.runCB = true;

    /*下拉的时候触发的事件*/
    var hideClose;
    $.bind(typeScroller, "refresh-release", function () {
        var that = this;
        clearTimeout(hideClose);
        hideClose = setTimeout(function () {
            that.hideRefresh();
        }, 500);
        //tells it to not auto-cancel the refresh
        return false; 
    });

    typeScroller.enable();
    //解决 一个page可能请求多次的问题
    var flagTypeScroller = true;

    /*上拉的时候触发的事件*/
    $.bind(typeScroller, "infinite-scroll", function () {
        var self = this;
        if ($(self.el).find("#infinite3").length == 0) {
            $(this.el).append("<div id='infinite3' style='border:1px solid gray;margin-top:10px;width:100%;height:30px;line-height:30px;text-align:center;'>加载中，请稍候...</div>");
        }

        $.bind(typeScroller, "infinite-scroll-end", function () {
            $.unbind(typeScroller, "infinite-scroll-end");
            $(self.el).find("#infinite3").remove();

            //解决 一个page可能请求多次的问题
            if (!flagTypeScroller) return;
            //解决 一个page可能请求多次的问题
            flagTypeScroller = false;

            /*请求服务器加载数据*/
            var type_list = '';
            $.ajax({
                url: 'GetNewsList?page=' + typePage + '&typeid=' + currentTypeid,
                success: function (data) {
                    for (var i = 0; i < data.length; i++) {
                        type_list += '<li><a href="#" class="icon heart">' + data[i].newstitle + '  <span style="color:gray;font-size:12px;">[' + formatDateTimeJSON(data[i].updatetime) + ']</span></a> </li>';
                    }
                    $("#type_list").append(type_list);
                    typePage++;
                    flagTypeScroller = true;
                    self.clearInfinite();
                }
            });
            /*请求服务器加载数据 完成*/
        });
    });
});