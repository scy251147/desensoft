﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using FuNong.UserInterface.Models;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using DSMiddlewire.Lib;

namespace FuNong.UserInterface.Hubs
{
    public class ChatHub : Hub, IChatHub
    {
        public ChatHub()
        {
        }

        private static ClientConnection client = ChatUserCache.client;
        private object lockObject = new object();

        #region Hub Events
        public override Task OnConnected()
        {
            ConnectToServer();
            return base.OnConnected();
        }

        public void ConnectToServer()
        {
            client.ClientSession = Context.ConnectionId;
            client.ClientInstance = new TcpClient();

            var endPoint = new IPEndPoint(IPAddress.Parse("219.235.3.215"), 60000);
            try
            {
                if (client != null)
                {
                    if (!IsConnected())
                    {
                        client.ClientInstance.Connect(endPoint);
                        Clients.All.printCommand("你已经连接到服务器！");
                        while (true)
                        {
                            lock (lockObject)
                            {
                                ReceiveServerCommands(client.ClientInstance);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Clients.All.printCommand(ex.Message);
            }

        }

        public override Task OnDisconnected()
        {
            client.ClientInstance.Client.Close();
            return base.OnDisconnected();
        }

        public override Task OnReconnected()
        {
            return base.OnReconnected();
        }
        #endregion

        //发送String数据到服务端
        private void SendClientRequests(TcpClient tcpClient, string message)
        {
            if(!IsConnected())
            {
                Clients.All.printCommand("你已经掉线，请刷新页面重试！");
                return;
            }

            NetworkStream ns = tcpClient.GetStream();
            if (ns.CanWrite)
            {
                var sendBytes = System.Text.Encoding.ASCII.GetBytes(message);
                ns.Write(sendBytes, 0, sendBytes.Length);
            }
            else
            {
                ns.Close();
            }
        }

        //发送Byte数据到服务端
        private void SendClientRequests(TcpClient tcpClient, byte[] messageBytes)
        {
            if (!IsConnected())
            {
                Clients.All.printCommand("你已经掉线，请刷新页面重试！");
                return;
            }
            NetworkStream ns = tcpClient.GetStream();
            if (ns.CanWrite)
            {
                ns.Write(messageBytes, 0, messageBytes.Length);
            }
            else
            {
                ns.Close();
            }
        }

        //这里可以接收到从前端发送过来的控制命令
        public void SendCommandToServer(string sessionid,string machineid, string routerid, functions message)
        {
            if (!string.IsNullOrEmpty(machineid))
                client.MachineID = machineid;

            //dst|0|11|ds0001|1x0x2x1|0003|crc
            var source = "0";  //从用户端发出的命令
            var funcCode = "011";

            var machineID = client.MachineID;
            var routerID = routerid;
            var flag = message.functionflag;
            var delay = "0000";
            var list = new List<string>();
            list.Add("dst");
            list.Add(source);
            list.Add(funcCode);
            list.Add(machineID);
            list.Add(routerid + "x" + flag);
            list.Add(delay);
            var result = string.Join("|", list);

            var mainPackage = CrcChcek.ConstructMessageWithCRC(result);

            SendClientRequests(client.ClientInstance, mainPackage);
        }

        //发送心跳包
        public void SendHeartBeatToMiddleware()
        {
            var heartBeatMsg = "dst|0|006|" + client.MachineID + "|00|00";
            var heartBeatPackage = CrcChcek.ConstructMessageWithCRC(heartBeatMsg);
            SendClientRequests(client.ClientInstance, heartBeatPackage);
        }

        //这里可以接收到从前端传过来的数据
        public void GetCurrentControllerData(controller controller)
        {
            client.MachineID = controller.machineid;

            Thread.Sleep(300);
            //立即发送一次heartbeat，以便于能够更新客户端信息
            SendHeartBeatToMiddleware();
            //可以在这里添加发送请求命令
            Thread.Sleep(300);
            SendRequestToServer("009");
            Thread.Sleep(300);
            SendRequestToServer("010");

        }

        private void SendRequestToServer(string messageType)
        {
            //dst|0|009|ds0001|1x0x2x1|0003|crc
            var source = "0";  //从用户端发出的命令
            var funcCode = messageType;
            var machineID = client.MachineID;
            var routerID = "0000";
            var delay = "0000";
            var list = new List<string>();
            list.Add("dst");
            list.Add(source);
            list.Add(funcCode);
            list.Add(machineID);
            list.Add(routerID);
            list.Add(delay);
            var result = string.Join("|", list);

            var mainPackage = CrcChcek.ConstructMessageWithCRC(result);

            SendClientRequests(client.ClientInstance, mainPackage);
        }

        //接收服务器端信息并处理
        private void ReceiveServerCommands(TcpClient tcpClient)
        {
            NetworkStream ns = tcpClient.GetStream();
            if (ns.CanRead)
            {
                var receiveBuffer = new byte[tcpClient.ReceiveBufferSize];
                //获取实际接收的数据长度
                var bytesReceivedLength = ns.Read(receiveBuffer, 0, receiveBuffer.Length);
                //创建能够容纳实际数据长度的byte数组
                var receiveBytes = new Byte[bytesReceivedLength];
                Array.Copy(receiveBuffer, 0, receiveBytes, 0, receiveBytes.Length);

                string messageReceived;
                if (CrcChcek.CheckMessageCRC(receiveBytes, out messageReceived))
                {
                    Clients.All.printCommand(messageReceived);
                }
                else
                {
                    //数据不正确，这里将不做任何处理，相当于抛弃数据
                }
            }
            else
            {
                ns.Close();
            }
        }

        //测试主机是否在线
        private bool IsConnected()
        {
            try
            {
                if (client.ClientInstance != null && client.ClientInstance.Client != null && client.ClientInstance.Client.Connected)
                {
                    /* pear to the documentation on Poll:
                     * When passing SelectMode.SelectRead as a parameter to the Poll method it will return 
                     * -either- true if Socket.Listen(Int32) has been called and a connection is pending;
                     * -or- true if data is available for reading; 
                     * -or- true if the connection has been closed, reset, or terminated; 
                     * otherwise, returns false
                     */

                    // Detect if client disconnected
                    if (client.ClientInstance.Client.Poll(0, SelectMode.SelectRead))
                    {
                        byte[] buff = new byte[1];
                        if (client.ClientInstance.Client.Receive(buff, SocketFlags.Peek) == 0)
                        {
                            // Client disconnected
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}