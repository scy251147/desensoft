﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FuNong.UserInterface.Models;

namespace FuNong.UserInterface.Hubs
{
    public class ChatUserCompare : IEqualityComparer<UserChat>
    {
        public bool Equals(UserChat x, UserChat y)
        {
            return x.ID == y.ID;
        }

        public int GetHashCode(UserChat obj)
        {
            return obj.GetHashCode();
        }
    }
}