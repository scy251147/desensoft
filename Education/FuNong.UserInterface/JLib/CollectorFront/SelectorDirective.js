﻿app.directive('sectorPart', [function() {
    return {
        restrict: 'AE',
        replace: true,
        scope: { options: "=" },
        template: '<select class="form-control" ng-options="item as item.name for item in options" name="state" style="width:220px;height:33px;">'
                + '<option value="">请选择</option>'
                + '</select>'
    };
}]);

app.directive('sectorMachine', [function () {
    return {
        restrict: 'AE',
        replace: true,
        scope: { options: "=" },
        template: '<select class="form-control" ng-options="item as item.machine_name for item in options" name="state" style="width:220px;height:33px;">'
                + '<option value="">请选择</option>'
                + '</select>'
    };
}]);

app.directive('sectorVideo', [function () {
    return {
        restrict: 'AE',
        replace: true,
        scope: { options: "=" },
        template: '<select class="form-control" ng-options="item as item.videoname for item in options" name="state" style="width:220px;height:33px;">'
                + '<option value="">请选择</option>'
                + '</select>'
    };
}]);

//当使用replace：true的时候，要求template必须有一个root节点，否则会报$compile:tplrt的error。解决方法就是讲replace改为false
app.directive('realData', [function () {
    return {
        restrict: 'AE',
        replace: false,
        scope: { options: "=" },
        link:function(scope,elem,attrs)
        {
            console.log(scope.options);
        },
        template: '<button type="button" ng-repeat="item in options" ng-click="$parent.$parent.ClickToGetParamDataList(item.param_id)" style="margin-left:3px;" class="btn btn-default glyphicon glyphicon-eye-close" style="margin-bottom:4px;">&nbsp;{{item.settingname}}:{{item.data_value}}{{item.settingvalue4}}</button>'
    };
}]);

app.directive('historyData', [function () {
    return {
        restrict: 'AE',
        replace: true,
        scope: { options: "=" },
        link: function (scope, elem, attrs) {

            //数据
            var d = eval("[" + scope.options.Param_data + "]");
            //随机线条颜色生成
            var colors = ["#FF0000", "#0062E3", "#FF00CC", "#00CCCC"];
            var index = Math.floor((Math.random() * 4) + 1);

            //添加图标鼠标悬浮事件
            var previousPoint = null, previousLabel = null;

            elem.on("plothover", function (event, pos, item) {
                if (item) {
                    debugger;
                    if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                        previousPoint = item.dataIndex;
                        previousLabel = item.series.label;
                        $("#tooltip").remove();

                        var x = item.datapoint[0];
                        var y = item.datapoint[1];
                        var date = new Date(x);
                        var color = item.series.color;

                        showTooltip(item.pageX, item.pageY, color,
                        "<strong>" + item.series.label + "</strong><br>" +
                        (date.getUTCMonth()) + "月" + date.getUTCDate() + "日" + date.getUTCHours() + "时" + date.getMinutes() + "分" +
                        "   " + item.series.label + ": <strong>" + y + "</strong> (" + scope.options.Param_unit + ")");
                    }
                }
                else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            });

            function showTooltip(x, y, color, contents) {
                $('<div id="tooltip">' + contents + '</div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y - 40,
                    left: x - 120,
                    border: '2px solid ' + color,
                    padding: '3px',
                    'font-size': '12px',
                    'color':'white',
                    'border-radius': '5px',
                    'background-color': ''+color+'',
                    'font-family': '宋体,Verdana, Arial, Helvetica, Tahoma, sans-serif',
                    opacity: 0.9
                }).appendTo("body").fadeIn(200);
            }

          
           
           
            //绑定到chart图标
            $.plot(elem, [{ label: scope.options.Param_name, data: d, color: colors[index] }], {
                xaxis: {
                    mode: "time"
                },
                legend: { position: "ne" },
                series: {
                    lines: { show: true },
                    points: { show: true, fill: false, radius: 2 }
                },
                grid: {
                    hoverable: true, //想用tooltip 这里一定要设置hoverable
                    backgroundColor: { colors: ["#fff", "#eee"] },
                    borderWidth: {
                        top: 1,
                        right: 1,
                        bottom: 2,
                        left: 2
                    }
                }
            });
        },
        template: '<div class="dae-placeholder" style="width:950px;height:200px;margin-left:20px;"></div>'
    };
}]);