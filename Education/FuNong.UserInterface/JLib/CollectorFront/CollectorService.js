﻿app.service('collectorService', ['$http', function ($http) {
    var provinceData = function () {
        return $http({
            method: 'POST',
            url: '/Process/GetLocationList?provinceid=-1&cityid=-1'
        });
    }

    var cityData = function (provinceId) {
        return $http({
            method: 'POST',
            url: '/Process/GetLocationList?provinceid=' + provinceId + '&cityid=-1'
        });
    }

    var districtData = function (cityId) {
        return $http({
            method: 'POST',
            url: '/Process/GetLocationList?provinceid=-1&cityid=' + cityId
        });
    }

    var companyData = function (districtId) {
        return $http({
            method: 'POST',
            url: '/BaseData/GetSchoolByDistrictId?districtId=' + districtId
        });
    }

    var machineList = function (companyId) {
        return $http({
            method: 'POST',
            url: '/Machine/GetMachineBy?companyId=' + companyId + '&isController=0'
        });
    }

    var realDataList = function (machineId) {
        return $http({
            method: 'POST',
            url: '/Machine/GetRealTimeCollectorDataByMachine?machineId=' + machineId
        });
    }

    var historyDataList = function (machineId,starttime,endtime) {
        return $http({
            method: 'POST',
            url: '/Machine/GetHistoryCollectorDataByMachine?machineId=' + machineId + "&paramId=&starttime=" + starttime + "&endtime=" + endtime
        });
    }

    var historyDataByParam = function (machineId,paramId,starttime,endtime) {
        return $http({
            method: 'POST',
            url: '/Machine/GetHistoryCollectorDataByMachine?machineId=' + machineId + '&paramId=' + paramId + "&starttime=" + starttime + "&endtime=" + endtime
        });
    }

    return {
        GetProvinceData: provinceData,
        GetCityData: cityData,
        GetDistrictData: districtData,
        GetCompanyData: companyData,
        GetMachineList: machineList,
        GetRealDataList: realDataList,
        GetHistoryDataList: historyDataList,
        GetHistoryDataListByParamId: historyDataByParam
    };
}]);
