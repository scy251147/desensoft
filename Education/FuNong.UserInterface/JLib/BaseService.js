﻿app.service('baseService', ['$cookies', function ($cookies) {

    //获取比对数据并赋值
    //type: province,city,district,company,machine
    var selectionMapper = function (sourceData, type) {
        //从cookie获取值
        var collectorSelectionFromCookie = $cookies.get('frontselection');
        if (collectorSelectionFromCookie != undefined) {
            //如果cookie存在，则进行解析
            collectorSelectionFromCookie = JSON.parse($cookies.get('frontselection'));
            for (var i = 0; i < sourceData.length; i++) {
                var current = sourceData[i];
                if (type == "province") {
                    if (current.id == collectorSelectionFromCookie.province.id) {
                        return current;
                    }
                }
                if (type == "city") {
                    if (current.id == collectorSelectionFromCookie.city.id) {
                        return current;
                    }
                }
                if (type == "district") {
                    if (current.id == collectorSelectionFromCookie.district.id) {
                        return current;
                    }
                }
                if (type == "company") {
                    if (current.id == collectorSelectionFromCookie.company.id) {
                        return current;
                    }
                }
                if (type == "machine") {
                    if (current.machine_id == collectorSelectionFromCookie.machine.machine_id) {
                        return current;
                    }
                }
                if (type == "monitor") {
                    if (current.id == collectorSelectionFromCookie.monitor.id) {
                        return current;
                    }
                }
            }
        }
        return null;
    }

    return {
        getSelectedDataMapper: selectionMapper
    };
}]);
