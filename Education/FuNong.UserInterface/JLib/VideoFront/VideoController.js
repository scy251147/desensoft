﻿app.controller('videoController', ['$scope', '$cookies', '$sce','baseService', 'videoService', function ($scope, $cookies, $sce, baseService,videoService) {
    var self = this;

    $scope.ProvinceData = null;
    $scope.CityData = null;
    $scope.DistrictData = null;
    $scope.CompanyData = null;
    $scope.MonitorData = null;

    $scope.selectedProvince = null;
    $scope.selectedCity = null;
    $scope.selectedDistrict = null;
    $scope.selectedCompany = null;
    $scope.selectedMonitor = null;

    $scope.frameSrc = null;

    //省份绑定
    videoService.GetProvinceData().then(function (data) {
        var flag = data.data.success;
        if (flag) {
            $scope.ProvinceData = data.data.data;
            $scope.selectedProvince = baseService.getSelectedDataMapper($scope.ProvinceData, 'province');
        }
    }, null);

    $scope.GetCityList = function () {
        var selectedProvinceId = $scope.selectedProvince.id;
        //市区绑定
        videoService.GetCityData(selectedProvinceId).then(function (data) {
            var flag = data.data.success;
            if (flag) {
                $scope.CityData = data.data.data;
                $scope.selectedCity = baseService.getSelectedDataMapper($scope.CityData, 'city');
            }
        }, null);
    }

    $scope.GetDistrictList = function () {
        var selectedCityId = $scope.selectedCity.id;
        //区县绑定
        videoService.GetDistrictData(selectedCityId).then(function (data) {
            var flag = data.data.success;
            if (flag) {
                $scope.DistrictData = data.data.data;
                $scope.selectedDistrict = baseService.getSelectedDataMapper($scope.DistrictData, 'district');
            }
        }, null);
    }

    $scope.GetCompanyList = function () {
        var selectedDistrictId = $scope.selectedDistrict.id;
        //公司绑定
        videoService.GetCompanyData(selectedDistrictId).then(function (data) {
            var flag = data.data.success;
            if (flag) {
                $scope.CompanyData = data.data.data;
                $scope.selectedCompany = baseService.getSelectedDataMapper($scope.CompanyData, 'company');
            }
        }, null);
    }

    $scope.GetVideoList = function () {
        var selectedCompanyId = $scope.selectedCompany.id;
        //设备绑定
        videoService.GetMonitorList(selectedCompanyId).then(function (data) {
            var flag = data.data.success;
            if (flag) {
                $scope.MonitorData = data.data.data;
                $scope.selectedMonitor = baseService.getSelectedDataMapper($scope.MonitorData, 'monitor');
            }
        }, null);
    }

    //监测省份的变化，如果发生了变化，则加载城市列表
    $scope.$watch('selectedProvince', function (oldval, newval) {
        if (oldval == null && newval == null)
            return;
        $scope.GetCityList();
    });

    //监测城市变化，如果发生了变化，则加载地区列表
    $scope.$watch('selectedCity', function (oldval, newval) {
        if (oldval == null && newval == null)
            return;
        $scope.GetDistrictList();
    });
    //监测地区变化，如果发生了变化，则加载公司列表
    $scope.$watch('selectedDistrict', function (oldval, newval) {
        if (oldval == null && newval == null)
            return;
        $scope.GetCompanyList();
    });
    //监测公司变化，如果发生了变化，则加载机器列表
    $scope.$watch('selectedCompany', function (oldval, newval) {
        if (oldval == null && newval == null)
            return;
        $scope.GetVideoList();
    });

    //获取NVR地址
    //注意在配置数据库nvr地址的时候，不要加http://



    $scope.GetRealTimeMonitorByVideo = function () {
        var nvrAddress = $scope.selectedMonitor.videoNVRAddress;
        debugger;
        if (nvrAddress == null || nvrAddress == undefined || nvrAddress == '') {
            art.dialog({ title: '提示', icon: 'error', time: 6, content: "未配置监控地址或监控地址配置不正确，请重试！", padding: 0 });
        }
        else {
            //$scope.frameSrc = $sce.trustAsResourceUrl("http://" + $scope.selectedMonitor.videoNVRAddress);

            var map = null;
            var zoom = 12;
            var marker = null;
            var customerWinInfo = null;
            var hideFlag = true;

            //初始化地图对象
            map = new TMap("mapDiv");
            //设置显示地图的中心点和级别
            var lnglat = new TLngLat($scope.selectedMonitor.wei, $scope.selectedMonitor.jin);
            var lngico = new TIcon("../Content/front/images/securitycamera.png", new TSize(64, 64), { anchor: new TPixel(9, 27) });
            map.centerAndZoom(lnglat, zoom);
            //允许鼠标双击放大地图
            map.enableHandleMouseScroll();
            //创建标注对象
            marker = new TMarker(lnglat, { icon: lngico });
            //向地图上添加标注
            map.addOverLay(marker);
            //注册标注的点击事件
            TEvent.addListener(marker, "click", function () {
                hideFlag = !hideFlag;
                var html = [];
                html.push('<div style="background:#51A151;height:30px;color:#FFF;width:335px;text-align:center;line-height:30px;font-weight:bold;font-size:15px;">');
                html.push('====>视频监控<====');
                html.push('</div>');
                html.push('<div id="deliver-legend-ctrl" style="background:#fff;">');
                html.push(' <table cellspacing="0" cellspadding="0" style="width:335px;height:140px;">');
                html.push('     <tr align="center">');
                html.push('         <td style="border-bottom:1px dashed green;">视频监控地址:</td>');
                html.push('         <td style="border-bottom:1px dashed green;text-align:left;font-size:14px;"><a target="_blank" href=' + $sce.trustAsResourceUrl("http://" + $scope.selectedMonitor.videoNVRAddress) + '>点击进行查看</a></td>');
                html.push('     </tr>');
                html.push('     <tr align="center">');
                html.push('         <td style="border-bottom:1px dashed green;">视频监控厂家:</td>');
                html.push('         <td style="border-bottom:1px dashed green;text-align:left">' + $scope.selectedMonitor.videocompanyshow + '</td>');
                html.push('     </tr>');
                html.push('     <tr align="center">');
                html.push('         <td style="border-bottom:1px dashed green;">视频监控用户:</td>');
                html.push('         <td style="border-bottom:1px dashed green;text-align:left">' + $scope.selectedMonitor.videoLoginName + '</td>');
                html.push('     </tr>');
                html.push('     <tr align="center">');
                html.push('         <td>视频监控密码:</td>');
                html.push('         <td style="text-align:left">' + $scope.selectedMonitor.videoLoginPass + '</td>');
                html.push('     </tr>');
                html.push(' </table>');
                html.push('</div>');
                var config = {
                    offset: new TPixel(0, 0),
                    position: marker.getLngLat()
                };
                //先remove一下，以便于清除重复显示部分
                map.removeOverLay(customerWinInfo);

                customerWinInfo = new TLabel(config);
                customerWinInfo.setTitle('');
                customerWinInfo.setLabel(html.join(''));
                customerWinInfo.getObject().style.zIndex = 10000;
                map.addOverLay(customerWinInfo);

                var obj = customerWinInfo.getObject();
                var width = parseInt(obj.offsetWidth);
                var height = parseInt(obj.offsetHeight);
                var icon = this.getIcon();
                var anchor_icon = icon.getAnchor();
                var pixel = new TPixel(width / -2, height / -2 - anchor_icon[1]);
                customerWinInfo.setOffset(pixel);

                if (hideFlag) {
                    map.removeOverLay(customerWinInfo);
                }
            });
        }

        //将级联列表项放到cookie中，以便于之后的操作简易化
        var expireDate = new Date();
        expireDate.setDate(expireDate.getDate() + 7);
        delete $cookies['frontselection'];

        var cookieData = JSON.stringify({
            province: $scope.selectedProvince,
            city: $scope.selectedCity,
            district: $scope.selectedDistrict,
            company: $scope.selectedCompany,
            monitor: $scope.selectedMonitor
        });
        $cookies.put('frontselection', cookieData, { 'expires': expireDate });
    }

}]);

