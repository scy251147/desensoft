﻿app.controller('paramController', ['$scope', 'paramService', function ($scope, paramService) {
    $scope.paramOption = null;
    $scope.selectedRowID = null;

    //Get data list
    paramService.paramData().then(function success(data) {
        $scope.paramOption = data.data;
    },null);

    //Set selected row
    $scope.setSelected = function (selectedRowID) {
        var element = parent.document.getElementById("curParamID");
        element.value = selectedRowID;

        $scope.selectedRowID = selectedRowID;
    }

}]);

