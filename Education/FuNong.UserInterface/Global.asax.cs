﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using System.Reflection;
using FuNong.DataAccess;
using FuNong.Service;
using FuNong.Framework.Caching;
using FuNong.Framework.Cookie;
using FuNong.Framework.Logger;
using FuNong.DataContract;
using FuNong.Framework.PoolCenter;

namespace FuNong.UserInterface
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            RouteTable.Routes.MapHubs();

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            RegisterDependency();

            //RouteTable.Routes.MapHubs();
        }

        private void RegisterDependency()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>)).InstancePerHttpRequest();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerHttpRequest();
            //必须修改成InstancePerHttpRequest，否则会造成错误被hold住的情况
            builder.RegisterType<FuNongContext>().As<IFuNongContext>().InstancePerHttpRequest();

            builder.RegisterType<BaseService<funong_publish_main>>().As<IBaseService<funong_publish_main>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<funong_publish_detail>>().As<IBaseService<funong_publish_detail>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<funong_service_request>>().As<IBaseService<funong_service_request>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<funong_intention>>().As<IBaseService<funong_intention>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<funong_deal>>().As<IBaseService<funong_deal>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<edu_cms_news_type>>().As<IBaseService<edu_cms_news_type>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<funong_news_soil_map>>().As<IBaseService<funong_news_soil_map>>().InstancePerHttpRequest();

            builder.RegisterType<BaseService<edu_department_detail>>().As<IBaseService<edu_department_detail>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<edu_model_detail>>().As<IBaseService<edu_model_detail>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<edu_role_detail>>().As<IBaseService<edu_role_detail>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<edu_user_detail>>().As<IBaseService<edu_user_detail>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<edu_role_model>>().As<IBaseService<edu_role_model>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<edu_cms_news>>().As<IBaseService<edu_cms_news>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<edu_base_setting>>().As<IBaseService<edu_base_setting>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<edu_area_village>>().As<IBaseService<edu_area_village>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<edu_school>>().As<IBaseService<edu_school>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<edu_school_grade>>().As<IBaseService<edu_school_grade>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<edu_school_class>>().As<IBaseService<edu_school_class>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<bma_users>>().As<IBaseService<bma_users>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<edu_student_class_apply>>().As<IBaseService<edu_student_class_apply>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<edu_card_change>>().As<IBaseService<edu_card_change>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<edu_video_all>>().As<IBaseService<edu_video_all>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<edu_icon>>().As<IBaseService<edu_icon>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<edu_field_control>>().As<IBaseService<edu_field_control>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<ds_machine_list>>().As<IBaseService<ds_machine_list>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<ds_machine_controller>>().As<IBaseService<ds_machine_controller>>().InstancePerHttpRequest();
            builder.RegisterType<BaseService<ds_machine_collector>>().As<IBaseService<ds_machine_collector>>().InstancePerHttpRequest();

            builder.RegisterType<DepartmentService>().As<IDepartmentService>().InstancePerHttpRequest();
            builder.RegisterType<RoleService>().As<IRoleService>().InstancePerHttpRequest();
            builder.RegisterType<UserService>().As<IUserService>().InstancePerHttpRequest();
            builder.RegisterType<ModelService>().As<IModelService>().InstancePerHttpRequest();
            builder.RegisterType<NewsService>().As<INewsService>().InstancePerHttpRequest();
            builder.RegisterType<SoilPublishService>().As<ISoilPublishService>().InstancePerHttpRequest();
            builder.RegisterType<ProcessService>().As<IProcessService>().InstancePerHttpRequest();
            builder.RegisterType<DealService>().As<IDealService>().InstancePerHttpRequest();
            builder.RegisterType<BaseDataService>().As<IBaseDataService>().InstancePerHttpRequest();
            builder.RegisterType<SchoolService>().As<ISchoolService>().InstancePerHttpRequest();
            builder.RegisterType<MachineService>().As<IMachineService>().InstancePerHttpRequest();

            builder.RegisterType<MemoryCacheManager>().As<ICacheManager>().InstancePerHttpRequest();
            builder.RegisterType<CookieWrapper>().As<ICookie>().InstancePerHttpRequest();
            builder.RegisterType<LoggerService>().As<ILoggerService>().InstancePerHttpRequest();
            builder.RegisterType<EventPool>().As<IEventPool>().SingleInstance();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}