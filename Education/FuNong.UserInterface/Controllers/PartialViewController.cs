﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FuNong.Service;
using FuNong.DataAccess;
using FuNong.DataContract;
using FuNong.Framework.Cookie;
using FuNong.Framework.Logger;
using FuNong.Framework.PoolCenter;
using FuNong.UserInterface.Models;

namespace FuNong.UserInterface.Controllers
{
    public class PartialViewController : BaseController
    {
        public PartialViewController(
               ICookie cookie
             , ILoggerService logger
             , IEventPool eventPool
             , IBaseDataService baseDataService
             , ISchoolService schoolService
             , INewsService newsService)
            : base(cookie, logger, eventPool, false)
            //comment by scy, 由于我这里的auth一直设置为true，导致一直出现“子页面重定向”的错误。并且，只要用户登录了，这种错误就没有了；但是一旦用户没登录，就会出现这种错误。
            //原因就是： 在homecontroller中，我的auth为false，但是在partialview中，我的auth为true。 页面肯定是先加载homecontroller，然后检测到auth为false，就向客户端发送可以不用
            //验证的消息；但是执行到partialview中，发现auth为true，然后会让浏览器重定向，收回之前发的false，要求客户端进行验证，这样就会导致浏览器无法收回之前的命令，而抛出错误。
            //所以，出现子页面重定向的时候，原因一般都是 浏览器已经执行了发送给客户端命令的语句，但是在半路中需要收回之前发送的命令，所以会造成这种错误的出现。
            //一旦出现错误，请进入OnAuthorization(AuthorizationContext filterContext)函数，看看是不是设置上有不一致的地方
        {
            this.baseDataService = baseDataService;
            this.schoolService   = schoolService;
            this.newsService     = newsService;

            this.cookie = cookie;
            this.logger = logger;
            this.eventPool = eventPool;
        }

        private readonly ICookie cookie;
        private readonly ILoggerService logger;
        private readonly IEventPool eventPool;

        private readonly INewsService newsService;
        private readonly IBaseDataService baseDataService;
        private readonly ISchoolService schoolService;

        public ActionResult VideoNewsPartial()
        {

            //var dict = newsDict;
            //if (dict == null)
            //    dict = GetNewsList();
            return PartialView("VideoNewsPartial", null);
        }

        public ActionResult LZNewsPartial()
        {
            //var dict = newsDict;
            //if (dict == null)
            //    dict = GetNewsList();
            return PartialView("LZNewsPartial", null);
        }

        public ActionResult ImgNewsPartial()
        {
            //var dict = newsDict;
            //if (dict == null)
            //    dict = GetNewsList();
            return PartialView("ImgNewsPartial", null);
        }

        [ChildActionOnly]
        public ActionResult ScrollNewsPartial()
        {
            var request = new NewsRequest();
            request.pageCount = 5;
            request.currentIndex = 0;
            request.where = x => x.todayscroll == 1 ;
            request.orderBy = x => x.updatetime;

            var response = newsService.GetNewsList(request);

            return PartialView("ScrollNewsPartial", response);
        }


        private Dictionary<NewsTypeResponse, List<NewsResponse>> GetNewsList()
        {
            //遍历新闻获取其列表
            var newsTypeRequest = new NewsTypeRequest();
            newsTypeRequest.pageCount = Int32.MaxValue;
            newsTypeRequest.currentIndex = 0;
            newsTypeRequest.where = x => x.parentid != 0;
            newsTypeRequest.orderBy = x => x.updatetime;

            var dict = newsService.TriggerList(newsTypeRequest);

            return dict;
        }

        public ActionResult UserSelection()
        {
            var request = new UserRequest();

            return PartialView("UserSelection");
        }

        public JsonResult ConstructUserSelectionTree()
        {
            var request = new CourseTableRequest();
            request.schoolid = CurrentLoginSchoolID;
            request.roleid = 2;  //只获取教师
            request.pageCount = 1;
            request.currentIndex = 0;
            request.where = x => x.id != 0;
            request.orderBy = x => x.updatetime;

            var result = schoolService.GetCourseTableFillData(request);

            if (result == null)
            {
                return Json(new { success = false, message = "无法获取相关信息，创建树失败！" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var treeDataList = new List<UserSelectionTree>();

                //学校
                var schoolSelection = new UserSelectionTree();
                var sid = "school:" + CurrentLoginSchoolID;
                schoolSelection.id = sid;
                schoolSelection.pId = "0";
                schoolSelection.name = CurrentLoginSchoolName;
                schoolSelection.isParent = true;
                schoolSelection.iconOpen = schoolSelection.iconClose = "../Content/admin/js/zTree_v3/css/zTreeStyle/img/diy/1_open.png";
                schoolSelection.open = true;
                schoolSelection.doCheck = false;
                treeDataList.Add(schoolSelection);

                //年级
                result.gradelist.ForEach(item =>
                {
                    var gradeSelection = new UserSelectionTree();
                    var gid =  "grade:" + item.id;
                    gradeSelection.id =gid;
                    gradeSelection.pId = sid;
                    gradeSelection.name = item.name;
                    gradeSelection.isParent = true;
                    gradeSelection.iconOpen = gradeSelection.iconClose = "../Content/admin/js/zTree_v3/css/zTreeStyle/img/diy/2.png";
                    gradeSelection.open = false;
                    schoolSelection.doCheck = false;
                    treeDataList.Add(gradeSelection);

                    //班级
                    result.classlist.ForEach(itemChild =>
                    {
                        if (itemChild.gradeid == item.id)
                        {
                            var classSelection = new UserSelectionTree();
                            classSelection.id = "class:" + itemChild.id;
                            classSelection.pId = gid;
                            classSelection.name = itemChild.classname;
                            classSelection.isParent = false;
                            classSelection.iconOpen = classSelection.iconClose = "../Content/admin/js/zTree_v3/css/zTreeStyle/img/diy/3.png";
                            classSelection.open = false;
                            classSelection.doCheck = true;
                            treeDataList.Add(classSelection);
                        }
                    });
                });

                return Json(new { success = true, data = treeDataList }, JsonRequestBehavior.AllowGet);
            }
        }

        [ChildActionOnly]
        public ActionResult MenuPartial()
        {
            var request = new NewsTypeRequest();
            request.pageCount = Int32.MaxValue;
            request.currentIndex = 0;
            request.where = x => x.id != 0;
            request.orderBy = x => x.updatetime;

            var typeList = newsService.GetTypeResponseList(request);
            return PartialView("MenuPartial", typeList);
        }

        //右侧边栏部分
        public ActionResult RightBar()
        {

            return PartialView("RightBar",null);
        }
    }
}
