﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FuNong.Service;
using FuNong.DataAccess;
using FuNong.Framework.Cookie;
using FuNong.Framework.Logger;
using FuNong.DataContract;
using FuNong.Framework.PoolCenter;

namespace FuNong.UserInterface.Controllers
{
    public class NewsController:BaseController
    {
        public NewsController(
               IBaseService<edu_department_detail> departmentCRUDService
             , IBaseService<edu_role_detail> roleCRUDService
             , IBaseService<edu_user_detail> userCRUDService
             , IBaseService<edu_model_detail> modelCRUDService
             , IBaseService<edu_role_model> roleModelCRUDService
             , IBaseService<edu_cms_news_type> newsTypeCRUDService
             , IBaseService<edu_cms_news> newsCRUDService
             , IBaseService<funong_news_soil_map> mapCRUDService
             , IDepartmentService departmentService
             , IRoleService roleService
             , IUserService userService
             , IModelService modelService
             , INewsService newsService
             , ISoilPublishService soilService
             , ICookie cookie
             , ILoggerService logger
             , IEventPool eventPool
            ):base(cookie,logger,eventPool,true)
        {
            this.departmentCRUDService = departmentCRUDService;
            this.roleCRUDService = roleCRUDService;
            this.userCRUDService = userCRUDService;
            this.modelCRUDService = modelCRUDService;
            this.roleModelCRUDService = roleModelCRUDService;
            this.newsTypeCRUDService = newsTypeCRUDService;
            this.newsCRUDService = newsCRUDService;
            this.mapCRUDService = mapCRUDService;

            this.departmentService = departmentService;
            this.roleService = roleService;
            this.userService = userService;
            this.modelService = modelService;

            this.newsService = newsService;
            this.soilService = soilService;

            this.cookie = cookie;
            this.logger = logger;
            this.eventPool = eventPool;
        }

        private readonly IBaseService<edu_department_detail> departmentCRUDService;
        private readonly IBaseService<edu_role_detail> roleCRUDService;
        private readonly IBaseService<edu_user_detail> userCRUDService;
        private readonly IBaseService<edu_model_detail> modelCRUDService;
        private readonly IBaseService<edu_role_model> roleModelCRUDService;
        private readonly IBaseService<edu_cms_news_type> newsTypeCRUDService;
        private readonly IBaseService<edu_cms_news> newsCRUDService;
        private readonly IBaseService<funong_news_soil_map> mapCRUDService;

        private readonly IDepartmentService departmentService;
        private readonly IRoleService roleService;
        private readonly IUserService userService;
        private readonly IModelService modelService;
        private readonly INewsService newsService;
        private readonly ISoilPublishService soilService;

        private readonly ICookie cookie;
        private readonly ILoggerService logger;
        private readonly IEventPool eventPool;

        public ActionResult NewsType()
        {
            return View();
        }

        public ActionResult NewsList()
        {
            return View();
        }

        public ActionResult NewsListApprove()
        {
            return View();
        }

        //将新闻关联到相关土地信息的模块
        public ActionResult NewsApply()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetNewsTypeAndSoilTypeList()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var request = new NewsSoilMapRequest();
            request.pageCount = rows;
            request.currentIndex = page;
            request.orderBy = x => x.updatetime;
            request.where = x => x.id != 0;
            request.propertyName = propertyName;
            request.propertyValue = propertyValue;

            var newsList = newsService.GetNewsSoilMap(request);

            int totalCount = 0;
            if (newsList != null && newsList.Count > 0)
            {
                totalCount = newsList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = newsList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult CommitNewsSoil()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<funong_news_soil_map>();
            return operationbase.Commit(mapCRUDService, inserted, updated, deleted, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (mapCRUDService.AddInBatch(list))
                    return Json(new { success = true, message = "资讯类型和土地类型关联成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "资讯类型和土地类型关联失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (mapCRUDService.UpdateInBatch(list))
                    return Json(new { success = true, message = "资讯类型和土地类型关联更新成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "资讯类型和土地类型关联更新失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                if (mapCRUDService.DeleteInBatch(list))
                    return Json(new { success = true, message = "资讯类型和土地类型关联移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "资讯类型和土地类型关联移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }

        [HttpPost]
        public JsonResult GetNewsTypeAndSoilType(int flag)
        {
            if (flag == 0)
            {
                var request = new NewsTypeRequest();
                request.currentIndex = 0;
                request.pageCount = Int32.MaxValue;
                request.where = x => x.parentid == 0;
                request.orderBy = x => x.updatetime;

                var newsTypeResponse = newsService.GetTypeResponseList(request);
                return Json(newsTypeResponse, JsonRequestBehavior.AllowGet);
            }
            if (flag == 1)
            {
                var requestSoil = new SoilAttributeRequest();
                requestSoil.soiltypeid = 0;

                var soilTypeResponse = soilService.GetSoilType(requestSoil);

                return Json(soilTypeResponse, JsonRequestBehavior.AllowGet);
            }

            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetNewsList()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var request = new NewsRequest();
            request.pageCount = rows;
            request.currentIndex = page;
            request.orderBy = x => x.updatetime;
            request.where = x => x.id != 0;
            request.propertyName = propertyName;
            request.propertyValue = propertyValue;

            var newsList = newsService.GetNewsList(request);

            int totalCount = 0;
            if (newsList != null && newsList.Count > 0)
            {
                totalCount = newsList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = newsList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [ValidateInput(false)]
        public ActionResult NewsEditor(NewsResponse news)
        {
            var flag = Request.Form["flag"];

            //初次进入，不弹出提示框
            if (string.IsNullOrEmpty(news.newstitle))
                return View(new NewsResponse() { Success = false, Message = "" });

            if (string.IsNullOrEmpty(news.typeid))
                return View(new NewsResponse() { Success = false, Message = "必须选择新闻类别！" });

            if (string.IsNullOrEmpty(news.newscontent))
                return View(new NewsResponse() { Success = false, Message = "必须填写新闻内容！" });

            news.recordorder = 1;
            news.updatetime = DateTime.Now;

            var list = new List<edu_cms_news>();

            var entity = new edu_cms_news();
            entity.isapprove = 0;
            entity.uid = CurrentLoginUserID;
            entity.pageimg = news.pageimg;
            entity.language = news.language;
            entity.typeid = news.typeid;
            entity.newstitle = news.newstitle;
            entity.newscontent = news.newscontent;
            entity.newsnote = news.newsnote;
            entity.recordorder = news.recordorder;
            entity.updatetime = news.updatetime;
            entity.todayrecommand = news.todayrecommand;
            entity.todaytop = news.todaytop;
            entity.todayscroll = news.todayscroll;
            
            bool flagAction = false;
            if (flag.Equals("0"))
            {
                list.Add(entity);
                flagAction = newsCRUDService.AddInBatch(list);
            }
            if (flag.Equals("1"))
            {
                entity.id = news.id;
                list.Add(entity);
                flagAction = newsCRUDService.UpdateInBatch(list);
            }
            
            if (flagAction)
            {
                //资讯信息确认，则发送资讯信息到站内信中
                eventPool.Publish("FN.ApproveNews");
                return View(new NewsResponse() { Success = true, Message = "资讯内容操作成功！" });
            }
            else
            {
                return View(new NewsResponse() { Success = false, Message = "资讯内容操作失败，请重试！" });
            }
        }

        [HttpPost]
        public JsonResult GetParentNewsType()
        {
            var request = new NewsTypeRequest();
            request.currentIndex = 0;
            request.pageCount = Int32.MaxValue;
            request.where = x => x.parentid == 0;
            request.orderBy = x => x.updatetime;

            var response = newsService.GetTypeResponseList(request);
            response.Insert(0, new NewsTypeResponse() { typename = "顶级菜单", parentid = 0, id = 0 });
            response.Insert(0, new NewsTypeResponse() { typename = "请选择", parentid = -1, id = -1 });

            return Json(response,JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetNewsDetails(int newsid)
        {
            var request = new NewsRequest();
            request.newsid = newsid;

            var response = newsService.GetNewDetailById(request);

            if (response == null)
            {
                return Json(new { success = false, message = "无法找到当前资讯内容！" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, data = response }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetNewsTypeListForCombobox()
        {
            var userRequest = new BaseSettingRequest();
            userRequest.currentIndex = 0;
            userRequest.pageCount = Int32.MaxValue;
            userRequest.orderBy = x => x.updatetime;
            userRequest.where = x => x.settinggroup=="newstype";
            userRequest.propertyName = string.Empty;
            userRequest.propertyValue = string.Empty;

            var typeList = newsService.GetTypeResponseListEx(userRequest);
            if (typeList == null)
            {
                return Json(new { success = false, message = "暂未获取到新闻类别!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, data = typeList }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult GetNewsTypeList()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var typeList = GetNewsType(rows, page, propertyName, propertyValue);

            int totalCount = 0;
            if (typeList != null && typeList.Count > 0)
            {
                totalCount = typeList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = typeList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }
        
        private List<NewsTypeResponse> GetNewsType(int rows,int page,string propertyName,string propertyValue)
        {
            var userRequest = new NewsTypeRequest();
            userRequest.pageCount = rows;
            userRequest.currentIndex = page;
            userRequest.orderBy = x => x.updatetime;
            userRequest.where = x => x.id != 0;
            userRequest.propertyName = propertyName;
            userRequest.propertyValue = propertyValue;

            var typeList = newsService.GetTypeResponseList(userRequest);

            return typeList;
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult CommitNews()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<edu_cms_news>();
            return operationbase.Commit(newsCRUDService, inserted, updated, deleted, (list) =>
            {
                foreach (var model in list)
                {
                    model.updatetime = DateTime.Now;
                    model.uid = CurrentLoginUserID;
                }

                if (newsCRUDService.AddInBatch(list))
                    return Json(new { success = true, message = "资讯信息新增成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "资讯信息新增失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                {
                    model.uid = CurrentLoginUserID;
                    model.updatetime = DateTime.Now;
                }

                if (newsCRUDService.UpdateInBatch(list))
                    return Json(new { success = true, message = "资讯信息更新成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "资讯信息更新失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                if (newsCRUDService.DeleteInBatch(list))
                    return Json(new { success = true, message = "资讯信息移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "资讯信息移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult ApproveNews()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<edu_cms_news>();
            return operationbase.Commit(newsCRUDService, inserted, updated, deleted, (list) =>
            {
                    return Json(new { success = false, message = "暂无操作！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                {
                    model.updatetime = DateTime.Now;
                }

                if (newsCRUDService.UpdateInBatch(list))
                    return Json(new { success = true, message = "当前信息审核成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "当前信息审核成功，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                return Json(new { success = false, message = "暂无操作！" }, JsonRequestBehavior.AllowGet);
            });
        }

        [HttpPost]
        public JsonResult CommitNewsType()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<edu_cms_news_type>();
            return operationbase.Commit(newsTypeCRUDService, inserted, updated, deleted, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (newsTypeCRUDService.AddInBatch(list))
                    return Json(new { success = true, message = "新闻类别新增成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "新闻类别新增失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (newsTypeCRUDService.UpdateInBatch(list))
                    return Json(new { success = true, message = "新闻类别更新成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "新闻类别更新失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                if (newsTypeCRUDService.DeleteInBatch(list))
                    return Json(new { success = true, message = "新闻类别移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "新闻类别移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }
    }
}