﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FuNong.DataContract;
using FuNong.Framework.PoolCenter;
using FuNong.Service;
using FuNong.Framework.Cookie;
using FuNong.Framework.Logger;

namespace FuNong.UserInterface.Controllers
{
    public class WapController : BaseController
    {
        public WapController(
               INewsService newsService
             , ICookie cookie
             , ILoggerService logger
             , IEventPool eventPool
            ):base(cookie,logger,eventPool,false)
        {
            this.newsService = newsService;

            this.cookie = cookie;
            this.logger = logger;
            this.eventPool = eventPool;
        }

        private readonly INewsService newsService;

        private readonly ICookie cookie;
        private readonly ILoggerService logger;
        private readonly IEventPool eventPool;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail()
        {
            return View();
        }

        //获取新闻列表信息
        [HttpGet]
        public JsonResult GetNewsList(int? page,int? typeid)
        {
            int currentPage = 0;
            int currentTypeid = 0;

            if (page != null)
            {
                currentPage = page.GetValueOrDefault() - 1;
            }

            if (typeid != null)
            {
                currentTypeid = typeid.GetValueOrDefault();
            }

            var request = new NewsRequest();
            request.pageCount = 10;
            request.currentIndex = currentPage;
            request.orderBy = x => x.updatetime;

            //if (currentTypeid != 0)
            //{
            //    request.where = x => x.id != 0 && x.typeid == currentTypeid;
            //}
            //else
            //{
            //    request.where = x => x.id != 0;
            //}

            var newsList = newsService.GetNewsList(request);

            return Json(newsList, JsonRequestBehavior.AllowGet);
        }

        //获取新闻类别
        [HttpGet]
        public JsonResult GetNewsType(int? page)
        {
            int currentPage = 0;

            if (page != null)
            {
                currentPage = page.GetValueOrDefault() - 1;
            }

            var request = new NewsTypeRequest();
            request.pageCount = 10;
            request.currentIndex = currentPage;
            request.where = x => x.id != 0 && x.typenamemeng.Equals("1");

            var typeList = newsService.GetTypeResponseList(request);

            return Json(typeList, JsonRequestBehavior.AllowGet);
        }

        //获取新闻详细信息
        [HttpGet]
        public JsonResult GetNewDetail(int? id)
        {
            int currentId = 0;
            if (id != null)
            {
                currentId = id.GetValueOrDefault();
            }

            var request = new NewsRequest();
            request.newsid = currentId;

            var newDetail = newsService.GetNewDetailById(request);

            return Json(newDetail,JsonRequestBehavior.AllowGet);

        }
    }
}
