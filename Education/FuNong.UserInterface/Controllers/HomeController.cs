﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Drawing.Imaging;
using DotNet.Utilities;
using FuNong.Service;
using FuNong.DataAccess;
using FuNong.DataContract;
using FuNong.UserInterface.Models;
using FuNong.Framework.Cookie;
using FuNong.Framework.Logger;
using System.Collections.Specialized;
using System.Text;
using System.Linq.Expressions;
using FuNong.Infrastructure.Predicate;
using FuNong.Framework.PoolCenter;
using System.Transactions;
using FuNong.Framework.Randoms;
using FuNong.Framework.MD5;

namespace FuNong.UserInterface.Controllers
{
   
    public class HomeController : BaseController
    {
        public HomeController(
               IBaseService<edu_department_detail> departmentCRUDService
             , IBaseService<edu_role_detail> roleCRUDService
             , IBaseService<edu_user_detail> userCRUDService
             , IBaseService<edu_model_detail> modelCRUDService
             , IBaseService<edu_role_model> roleModelCRUDService
             , IBaseService<funong_intention> intentionCRUDService
             , IBaseService<bma_users> bmaCRUDService
             , IDepartmentService departmentService
             , IRoleService roleService
             , IUserService userService
             , IModelService modelService
             , INewsService newsService
             , ISoilPublishService soilService
             , ISchoolService companyService
             , ICookie cookie
             , ILoggerService logger
             , IEventPool eventPool
            ): base(cookie, logger,eventPool,false)
        {
            this.departmentCRUDService = departmentCRUDService;
            this.roleCRUDService = roleCRUDService;
            this.userCRUDService = userCRUDService;
            this.modelCRUDService = modelCRUDService;
            this.roleModelCRUDService = roleModelCRUDService;
            this.intentionCRUDService = intentionCRUDService;
            this.bmaCRUDService = bmaCRUDService;

            this.departmentService = departmentService;
            this.roleService = roleService;
            this.userService = userService;
            this.modelService = modelService;

            this.newsService = newsService;
            this.soilService = soilService;
            this.companyService = companyService;

            this.cookie = cookie;
            this.logger = logger;
            this.eventPool = eventPool;
        }

        private readonly IBaseService<edu_department_detail> departmentCRUDService;
        private readonly IBaseService<edu_role_detail> roleCRUDService;
        private readonly IBaseService<edu_user_detail> userCRUDService;
        private readonly IBaseService<edu_model_detail> modelCRUDService;
        private readonly IBaseService<edu_role_model> roleModelCRUDService;
        private readonly IBaseService<funong_intention> intentionCRUDService;
        private readonly IBaseService<bma_users> bmaCRUDService;

        private readonly IDepartmentService departmentService;
        private readonly IRoleService roleService;
        private readonly IUserService userService;
        private readonly IModelService modelService;
        private readonly INewsService newsService;
        private readonly ISoilPublishService soilService;
        private readonly ISchoolService companyService;

        private readonly ICookie cookie;
        private readonly ILoggerService logger;
        private readonly IEventPool eventPool;

        //首页
        public ActionResult Index()
        {
            //遍历新闻获取其列表
            var newsTypeRequest = new NewsTypeRequest();
            newsTypeRequest.pageCount = Int32.MaxValue;
            newsTypeRequest.currentIndex = 0;
            newsTypeRequest.where = x => x.parentid != 0;
            newsTypeRequest.orderBy = x => x.updatetime;

            var dict = newsService.TriggerList(newsTypeRequest);


            var request = new SoilAttributeRequest();
            request.provinceid = 5;
            var response = soilService.GetCityByProvinceID(request);
            ViewBag.Citys = response;

            request.soiltypeid = -1;            
            var respSoiltype = soilService.GetSoilType(request).Where(x => x.parentid == 0).ToList();
            ViewBag.Soiltypes = respSoiltype;

            request.changetypeid = -1;
            var respChangetype = soilService.GetChangeType(request);
            ViewBag.Changetypes = respChangetype;

            //  五类土地类型对应的列表
            var triglelist = soilService.TriggerSoilIndex();

            var dictNew = new Dictionary<IndexModel, List<SoilPublishMainResponse>>();
           
            foreach (var item in triglelist)
            {
                var soilTypeResponse = item.Key;
                var soilMainList = item.Value;

                var soilrequest = new SoilAttributeRequest();
                soilrequest.soiltypeid = item.Key.id;
                var response2 = soilService.GetSoilType(soilrequest);

                IndexModel index = new IndexModel();
                index.SoilTypeParent = soilTypeResponse;
                index.SoilTypeChildren = response2;
                // 热点新闻一条 显示简介
                //遍历新闻获取其列表  3条 
                //投资列表 4条
                var newsrequest = new NewsRequest();
                newsrequest.soiltypeid = soilTypeResponse.id;
                newsrequest.currentIndex = 0;
                newsrequest.pageCount = 15;               
                index.NewsToplist = newsService.GetNewsBySoilTypeId(newsrequest);              

                //图片 2条

                dictNew.Add(index, soilMainList);
            }
            ViewBag.main = dictNew;


            //热门专题 2条带详情带图片 标题9个字+... 详情41个字  6条列表  列表24个字+...
            

            //土地工具 4条

            return View(dict);
        }

        //资讯中心
        public ActionResult NewsCenter()
        {
            //遍历新闻获取其列表
            var newsTypeRequest = new NewsTypeRequest();
            newsTypeRequest.pageCount = Int32.MaxValue;
            newsTypeRequest.currentIndex = 0;
            newsTypeRequest.where = x => x.parentid == 0;
            newsTypeRequest.orderBy = x => x.updatetime;

            var dict = newsService.TriggerList(newsTypeRequest);

            return View(dict);
        }

        public ActionResult NewsList()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetNewsList(string typeid)
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var request = new NewsRequest();
            request.pageCount = rows;
            request.currentIndex = page;
            request.orderBy = x => x.updatetime;
            request.propertyName = propertyName;
            request.propertyValue = propertyValue;

            if (string.IsNullOrEmpty(typeid) || typeid.Equals("null") || typeid.Equals("undefined"))
                request.where = x => x.id != 0;
            else
                request.where = x => x.typeid == typeid;

            var newsList = newsService.GetNewsList(request);

            int totalCount = 0;
            if (newsList != null && newsList.Count > 0)
            {
                totalCount = newsList[0].totalcount;
                newsList.ForEach(item =>
                {
                    if(string.IsNullOrEmpty(item.pageimg))
                    {
                        item.pageimg = "../Content/front/images/no.jpg";
                    }
                });
            }

            if (totalCount == 0)
            {
                newsList = new List<NewsResponse>();
                newsList.Add(new NewsResponse() { id = -1, newstitle = "<div style='width:100%;text-align:center;float:left;color:red;'>当前栏目暂无发布信息！</div>", schoolname = string.Empty, uname = string.Empty, updatetime = DateTime.Now });
            }

            var json = new
            {
                total = totalCount,
                rows = newsList
            };

            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        public ActionResult NewsDetail()
        {
            //遍历新闻获取其列表
            //var newsTypeRequest = new NewsTypeRequest();
            //newsTypeRequest.pageCount = Int32.MaxValue;
            //newsTypeRequest.currentIndex = 0;
            //newsTypeRequest.where = x => x.parentid != 0;
            //newsTypeRequest.orderBy = x => x.updatetime;

            //var dict = newsService.TriggerList(newsTypeRequest);

            //获取新闻信息列表
            var id = Request.QueryString["id"];
            if (!string.IsNullOrEmpty(id))
            {
                var newsRequest = new NewsRequest();
                newsRequest.newsid = Int32.Parse(id);
                var result = newsService.GetNewDetailById(newsRequest);
                ViewBag.message = result;
            }
            else
            {
                ViewBag.message = new NewsResponse() { newstitle = "当前访问页面暂无具体资讯信息！" };
            }

            return View();
        }

        //用户登陆
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Video()
        {
            return View();
        }

        //用户注册
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Checkuser(string inamex)
        {
            if (string.IsNullOrEmpty(inamex))
                return Json(new { success=false,message="用户名不能为空！"},JsonRequestBehavior.AllowGet);

            UserRequest request = new UserRequest();
            request.pageCount = 10;
            request.currentIndex = 0;
            request.where = x => x.username == inamex;
            request.orderBy = x => x.updatetime;

            var list = userService.TriggerList(request);

            if (list == null || list.Count == 0)
            {
                return Json(new { success = true, message = "此用户账号可用！" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, message = "此用户账号已被注册，请重新输入一个！" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult RegisterUser(string usernamex, string pwdx, string cpwdx, string phonex)
        {
            if (string.IsNullOrEmpty(usernamex))
                return Json(new { success = false, message = "用户名不能为空！" }, JsonRequestBehavior.AllowGet);
            if (string.IsNullOrEmpty(pwdx))
                return Json(new { success = false, message = "密码不能为空！" }, JsonRequestBehavior.AllowGet);
            if (string.IsNullOrEmpty(cpwdx))
                return Json(new { success = false, message = "确认密码不能为空！" }, JsonRequestBehavior.AllowGet);
            if (string.IsNullOrEmpty(phonex))
                return Json(new { success = false, message = "手机号码不能为空！" }, JsonRequestBehavior.AllowGet);
            if (pwdx != cpwdx)
                return Json(new { success = false, message = "两次输入密码不通过！" }, JsonRequestBehavior.AllowGet);

            var ramLib = new RandomWrapper();

            var bmaEntity = new bma_users();
            bmaEntity.username = usernamex;
            bmaEntity.mobile = phonex;
            bmaEntity.salt = ramLib.CreateRandomValue(6);
            bmaEntity.password = MD5Wrapper.Md5Wrapper.CreateUserPassword(pwdx, bmaEntity.salt);

            bmaEntity.email = "test@test.com";
            bmaEntity.userrid = 7;
            bmaEntity.storeid = 0;
            bmaEntity.mallagid = 1;
            bmaEntity.nickname = Guid.NewGuid().ToString().Substring(0, 19);
            bmaEntity.avatar = string.Empty;
            bmaEntity.paycredits = 0;
            bmaEntity.rankcredits = 0;
            bmaEntity.verifyemail = 0;
            bmaEntity.verifymobile = 0;
            bmaEntity.liftbantime = DateTime.Parse("1900-01-01 00:00:00.000");

            var bmaUserList = new List<bma_users>();
            bmaUserList.Add(bmaEntity);

            if (bmaCRUDService.AddInBatch(bmaUserList)) //添加用户完毕
            {
                var request = new UserRequest();
                request.username = usernamex;
                request.pageCount = 1;
                request.currentIndex = 0;

                var currentInsertUser = userService.TriggerMainUserList(request);

                if (currentInsertUser == null)
                {
                    return Json(new { success = false, message = "无法获取用户信息，新增用户失败！！" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var uid = currentInsertUser.uid;
                    var userEntity = new edu_user_detail();
                    userEntity.uid = uid;
                    userEntity.roleid = 5;  //普通会员
                    userEntity.ischeck = 0;
                    userEntity.recordorder = 1;
                    userEntity.departmentid = 3;  //客户
                    userEntity.updatetime = DateTime.Now;

                    var list = new List<edu_user_detail>();
                    list.Add(userEntity);
                    var flag = userCRUDService.AddInBatch(list);
                    if (flag)
                    {
                        return Json(new { success = true, message = "用户注册成功，请等待管理员审核！" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, message = "用户注册失败，请重试！" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, message = "用户注册失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetCityList(string provinceid)
        {
            int provinceIdInInt;
            if (string.IsNullOrEmpty(provinceid))
                return Json(new { success = false, message = "省份编号不能为空!" });
            if (!Int32.TryParse(provinceid, out provinceIdInInt))
                return Json(new { success = false, message = "身份编号需要为数字型！" });

            var request = new SoilAttributeRequest();
            request.provinceid = provinceIdInInt;

            var response = soilService.GetCityByProvinceID(request);

            var request1 = new SoilAttributeRequest();
            request1.soiltypeid = -1;
            var response1 = soilService.GetSoilType(request1).Where(x => x.id != 0).ToList();

            var request2 = new SoilAttributeRequest();
            request2.changetypeid = -1;
            var response2 = soilService.GetChangeType(request2);

            return Json(new { success = true, data = response, data1 = response1, data2 = response2 }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetDistrictListByCityID(string cityid)
        {
            int cityIdInInt;
            if (string.IsNullOrEmpty(cityid))
                return Json(new { success = false, message = "城市编号不能为空!" });
            if (!Int32.TryParse(cityid, out cityIdInInt))
                return Json(new { success = false, message = "城市编号需要为数字型！" });

            var request = new SoilAttributeRequest();
            request.cityid = cityIdInInt;

            var response = soilService.GetDistrictByCityID(request);

            return Json(new { success = true, data = response}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSoilMarketList()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            string districtlist = string.Empty;
            string soiltypelist = string.Empty;
            string changetypelist = string.Empty;

            if (!string.IsNullOrEmpty(Request.Form["districtlist"])) districtlist = Request.Form["districtlist"];
            if (!string.IsNullOrEmpty(Request.Form["soiltypelist"])) soiltypelist = Request.Form["soiltypelist"];
            if (!string.IsNullOrEmpty(Request.Form["changetypelist"])) changetypelist = Request.Form["changetypelist"];

            var listchangetype = ConvertStrToIntList(changetypelist);
            var listsoiltype = ConvertStrToIntList(soiltypelist);
            var listdistrict = ConvertStrToIntList(districtlist);

            var soilRequest = new SoilPublishMainRequest();
            soilRequest.pageCount = rows;
            soilRequest.currentIndex = page;
            soilRequest.orderBy = x => x.updatetime;
            soilRequest.propertyName = propertyName;
            soilRequest.propertyValue = propertyValue;

            var predicate = PredicateBuilder.True<funong_publish_main>();  

            if (listchangetype.Count != 0)
            {
                predicate = predicate.And<funong_publish_main>(x => listchangetype.Contains(x.changetypeid));
            }
            if(listsoiltype.Count!=0)
            {
                predicate = predicate.And<funong_publish_main>(x => listsoiltype.Contains(x.soiltypeid));
            }
            if (listdistrict.Count != 0)
            {
                predicate = predicate.And<funong_publish_main>(x => listdistrict.Contains(x.districtid));
            }

            soilRequest.where = predicate;

            var soilList = soilService.TriggerList(soilRequest);

            int totalCount = 0;
            if (soilList != null && soilList.Count > 0)
            {
                totalCount = soilList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = soilList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult GetTardeingMarketList()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string changetypelist = string.Empty;

            if (!string.IsNullOrEmpty(Request.Form["changetypelist"])) changetypelist = Request.Form["changetypelist"];

            var listchangetype = ConvertStrToIntList(changetypelist);

            var soilRequest             = new SoilPublishMainRequest();
            soilRequest.pageCount       = rows;
            soilRequest.currentIndex    = page;
            soilRequest.orderBy         = x => x.updatetime;
            soilRequest.propertyName    = string.Empty;
            soilRequest.propertyValue   = string.Empty;

            var predicate = PredicateBuilder.True<funong_publish_main>();

            if (listchangetype.Count != 0)
            {
                predicate = predicate.And<funong_publish_main>(x => listchangetype.Contains(x.changetypeid));
            }

            soilRequest.where = predicate;

            var soilList = soilService.TriggerList(soilRequest);

            int totalCount = 0;
            if (soilList != null && soilList.Count > 0)
            {
                totalCount = soilList[0].totalcount;
            }

            //datagrid easyui横排的核心代码
            var listT = new List<Models.TradingMarket>();

            for (var i = 0; i < soilList.Count / 4; i++)
            {
                var sr1 = soilList[i * 4];
                var sr2 = soilList[i * 4 + 1];
                var sr3 = soilList[i * 4 + 2];
                var sr4 = soilList[i * 4 + 3];

                var m = new TradingMarket();
                m.sr1 = sr1;
                m.sr2 = sr2;
                m.sr3 = sr3;
                m.sr4 = sr4;
                listT.Add(m);
            }

            var json = new
            {
                total = totalCount,
                rows = listT
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        private List<int> ConvertStrToIntList(string inputstring)
        {
            var list = new List<int>();

            if (string.IsNullOrEmpty(inputstring))
                return list;

            if (!inputstring.Contains(','))
            {
                int number;
                if (Int32.TryParse(inputstring, out number))
                {
                    list.Add(number);
                }
            }
            else
            {
                var arrayList = inputstring.Split(',').ToList();
                arrayList.ForEach(item =>
                {
                    int number;
                    if (Int32.TryParse(item, out number))
                    {
                        list.Add(number);
                    }
                });
            }
            return list;
        }

        private List<int> ConvertStrListToIntList(List<string> stringList)
        {
            var list = new List<int>();

            if (stringList == null)
                return list;

            if (stringList.Count == 0)
                return list;

            stringList.ForEach(item =>
            {
                int number;
                if (Int32.TryParse(item, out number))
                {
                    list.Add(number);
                }
            });

            return list;
        }

        [HttpGet]
        public JsonResult LoginValidation(string uname, string upass, string vcode)
        {
            if (string.IsNullOrEmpty(uname))
                return Json(new UserValidationModel() { success = false, message = "用户名不能为空，请重新输入！" }, JsonRequestBehavior.AllowGet);
            if (string.IsNullOrEmpty(upass))
                return Json(new UserValidationModel() { success = false, message = "密码不能为空，请重新输入！" }, JsonRequestBehavior.AllowGet);
            if (string.IsNullOrEmpty(vcode))
                return Json(new UserValidationModel() { success = false, message = "验证码不能为空，请重新输入！" }, JsonRequestBehavior.AllowGet);

            UserValidationRequest uValidRequest = new UserValidationRequest();
            uValidRequest.username = uname;
            uValidRequest.userpass = upass;
            var userEntity = userService.UserLogin(uValidRequest);

            //获取当前用户下面所管理的公司名称
            var companyRequest = new SchoolRequest();
            companyRequest.pageCount = Int32.MaxValue;
            companyRequest.currentIndex = 0;
            companyRequest.where = x => x.userid == userEntity.uid;
            companyRequest.orderBy = x => x.updatetime;
            var companyList = companyService.GetSchoolResponse(companyRequest);
          
            if (userEntity.Success)
            {
                //手机端无需验证码
                if (vcode.Equals("false"))
                {
                    logger.Info(new LogContent("用户[" + userEntity.username.Trim() + "：" + userEntity.rolename.Trim() + "]从手机端成功登陆本系统!", userEntity.uid.ToString(), "0"));
                }
                else
                {
                    var vcodeInCache = CacheHelper.GetCache("FuNong.ValidateCode");
                    if (vcodeInCache == null)
                        return Json(new UserValidationModel() { success = false, message = "当前验证码已经失效，请F5刷新本页面重试！" }, JsonRequestBehavior.AllowGet);

                    var vCodeInCacheString = vcodeInCache.ToString();
                    if (vCodeInCacheString.Equals(vcode))
                    {
                        logger.Info(new LogContent("用户[" + userEntity.username.Trim() + "：" + userEntity.rolename.Trim() + "]成功登陆本系统!", userEntity.uid.ToString(), "0"));
                    }
                    else
                    {
                        return Json(new UserValidationModel() { success = false, message = "验证码输入错误，请重试！" }, JsonRequestBehavior.AllowGet);
                    }
                }

                var nvCollection = new NameValueCollection();
                nvCollection.Add("FuNong.Login.UserID", userEntity.uid.ToString());
                nvCollection.Add("FuNong.Login.UserName", userEntity.username);
                nvCollection.Add("FuNong.Login.Salt", userEntity.salt);
                nvCollection.Add("FuNong.Login.RoleID", userEntity.roleid.ToString());
                nvCollection.Add("FuNong.Login.RoleName", HttpUtility.UrlEncode(userEntity.rolename));
                nvCollection.Add("FuNong.Login.NickName", HttpUtility.UrlEncode(userEntity.nickname));

                nvCollection.Add("FuNong.Login.CompanyList", string.Join(",", companyList.Select(x => x.id)));

                cookie.AddCookie("FuNong.UserInfo.Login", nvCollection);

                return Json(new UserValidationModel() { success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new UserValidationModel() { success = false, message = userEntity.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        //土地市场
        public ActionResult SoilMarket()
        {
            return View();
        }

        //土地详细信息
        public ActionResult SoilDetail()
        {
            //遍历新闻获取其列表
            var newsTypeRequest = new NewsTypeRequest();
            newsTypeRequest.pageCount = Int32.MaxValue;
            newsTypeRequest.currentIndex = 0;
            newsTypeRequest.where = x => x.parentid != 0;
            newsTypeRequest.orderBy = x => x.updatetime;

            var dict = newsService.TriggerList(newsTypeRequest);

            //获取详细土地信息
            var id = Request.QueryString["id"];
            var idInInt32 = 0;

            if (string.IsNullOrEmpty(id))
                ViewBag.message = new SoilPublishMainResponse() { publishtitle = "土地详细信息编号不能为空！" };

            if (!Int32.TryParse(id, out idInInt32))
                ViewBag.message = new SoilPublishMainResponse() { publishtitle = "土地详细信息编号需为整数型！" };

            var soilMainRequest = new SoilPublishMainRequest();
            soilMainRequest.currentIndex = 0;
            soilMainRequest.pageCount = 1;
            soilMainRequest.where = x => x.id == idInInt32;
            soilMainRequest.orderBy = x => x.updatetime;
            var result = soilService.TriggerList(soilMainRequest);
            ViewBag.message = result.FirstOrDefault();

            return View(dict);
        }

        [HttpPost]
        public JsonResult SubmitIntention(string inamex, string iphonex, string inotex, int imainid, int irequestid, int isapprovex)
        {
            if(isapprovex==0)
                return Json(new { success = false, message = "此发布信息暂未被审核，无法提交意向申请，请静待审核通过！" }, JsonRequestBehavior.AllowGet);
            if (string.IsNullOrEmpty(inamex))
                return Json(new { success = false, message = "请输入真实的用户名！" }, JsonRequestBehavior.AllowGet);
            if (string.IsNullOrEmpty(iphonex))
                return Json(new { success = false,message="请输入可用的电话号码！"},JsonRequestBehavior.AllowGet);

            //先检测此条信息是否已经被审核，只有被审核的信息 才能进行提交意向申请

            var fIntention = new funong_intention();
            fIntention.isarrange = 0;
            fIntention.issuccess = 0;
            fIntention.publishmainid = imainid;
            fIntention.requestid = irequestid;
            fIntention.requestname = inamex;
            fIntention.requestnote = inotex;
            fIntention.requestphone = iphonex;
            fIntention.recordorder = 1;
            fIntention.updatetime = DateTime.Now;

            var list = new List<funong_intention>();
            list.Add(fIntention);

            var result = intentionCRUDService.AddInBatch(list);
            if (result)
            {
                return Json(new { success=true,message="意向申请提交成功，请静待流转中心组织交易！"},JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, message = "意向申请提交失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }
        }

        //交易大厅
        public ActionResult TradingMarket()
        {
            return View();
        }

        //金融中心
        public ActionResult FinanceCenter()
        {
            return View();
        }

        //商务中心
        public ActionResult CommerceCenter()
        {
            return View();
        }

        //公司简介
        public ActionResult About()
        {
            return View();
        }

        //前台采集信息页面
        public ActionResult Collector()
        {
            return View();
        }

        //前台控制信息页面
        public ActionResult Controller()
        {
            return View();
        }

        //注销当前用户
        public ActionResult Logout()
        {
            var collection = cookie.GetCookieCollection("FuNong.UserInfo.Login");
            if (collection != null)
            {
                var schoolid = collection["FuNong.Login.SchoolID"];
                var userid = collection["FuNong.Login.UserID"];
                var username = collection["FuNong.Login.UserName"];
                var typeid = "0";
                logger.Info(new LogContent("用户" + username + "注销了本系统！", schoolid, typeid));
            }

            cookie.ClearCooke("FuNong.UserInfo.Login");
            
            var redirectResult = new RedirectResult("/");
            return redirectResult;
        }

        #region 验证码生成
        public ActionResult ValidateCode()
        {
            string code = CreateValidateCode(4);
            CacheHelper.SetCache("FuNong.ValidateCode", code);
            byte[] bytes = CreateValidateGraphic(code);
            return File(bytes, @"image/jpeg");
        }

        public string CreateValidateCode(int length)
        {
            int[] randMembers = new int[length];
            int[] validateNums = new int[length];
            string validateNumberStr = "";
            //生成起始序列值
            int seekSeek = unchecked((int)DateTime.Now.Ticks);
            Random seekRand = new Random(seekSeek);
            int beginSeek = (int)seekRand.Next(0, Int32.MaxValue - length * 10000);
            int[] seeks = new int[length];
            for (int i = 0; i < length; i++)
            {
                beginSeek += 10000;
                seeks[i] = beginSeek;
            }
            //生成随机数字
            for (int i = 0; i < length; i++)
            {
                Random rand = new Random(seeks[i]);
                int pownum = 1 * (int)Math.Pow(10, length);
                randMembers[i] = rand.Next(pownum, Int32.MaxValue);
            }
            //抽取随机数字
            for (int i = 0; i < length; i++)
            {
                string numStr = randMembers[i].ToString();
                int numLength = numStr.Length;
                Random rand = new Random();
                int numPosition = rand.Next(0, numLength - 1);
                validateNums[i] = Int32.Parse(numStr.Substring(numPosition, 1));
            }
            //生成验证码
            for (int i = 0; i < length; i++)
            {
                validateNumberStr += validateNums[i].ToString();
            }
            return validateNumberStr;
        }

        public byte[] CreateValidateGraphic(string validateCode)
        {
            Bitmap image = new Bitmap((int)Math.Ceiling(validateCode.Length * 12.0), 22);
            Graphics g = Graphics.FromImage(image);
            try
            {
                //生成随机生成器
                Random random = new Random();
                //清空图片背景色
                g.Clear(Color.White);
                //画图片的干扰线
                for (int i = 0; i < 25; i++)
                {
                    int x1 = random.Next(image.Width);
                    int x2 = random.Next(image.Width);
                    int y1 = random.Next(image.Height);
                    int y2 = random.Next(image.Height);
                    g.DrawLine(new Pen(Color.Silver), x1, y1, x2, y2);
                }
                Font font = new Font("Arial", 12, (FontStyle.Bold | FontStyle.Italic));
                LinearGradientBrush brush = new LinearGradientBrush(new Rectangle(0, 0, image.Width, image.Height),
                 Color.Blue, Color.DarkRed, 1.2f, true);
                g.DrawString(validateCode, font, brush, 3, 2);
                //画图片的前景干扰点
                for (int i = 0; i < 100; i++)
                {
                    int x = random.Next(image.Width);
                    int y = random.Next(image.Height);
                    image.SetPixel(x, y, Color.FromArgb(random.Next()));
                }
                //画图片的边框线
                g.DrawRectangle(new Pen(Color.Silver), 0, 0, image.Width - 1, image.Height - 1);
                //保存图片数据
                MemoryStream stream = new MemoryStream();
                image.Save(stream, ImageFormat.Jpeg);
                //输出图片流
                return stream.ToArray();
            }
            finally
            {
                g.Dispose();
                image.Dispose();
            }
        }
        #endregion
    }
}
