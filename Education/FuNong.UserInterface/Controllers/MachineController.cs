﻿using FuNong.DataAccess;
using FuNong.DataContract;
using FuNong.Framework.Cookie;
using FuNong.Framework.Logger;
using FuNong.Framework.PoolCenter;
using FuNong.Service;
using FuNong.UserInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FuNong.UserInterface.Controllers
{
    public class MachineController : BaseController
    {

        public MachineController(
               IBaseService<ds_machine_list> machineCRUDService
             , IBaseService<ds_machine_controller> controllerCRUDService
             , IBaseService<ds_machine_collector> collectorCRUDService
             , IMachineService machineService
             , ISchoolService schoolService
             , ICookie cookie
             , ILoggerService logger
             , IEventPool eventPool
             , IFuNongContext fuNongContext
            )
            : base(cookie, logger, eventPool, true)
        {
            this.machineCRUDService = machineCRUDService;
            this.controllerCRUDService = controllerCRUDService;
            this.collectorCRUDService = collectorCRUDService;

            this.machineService = machineService;
            this.schoolService = schoolService;

            this.cookie = cookie;
            this.logger = logger;
            this.eventPool = eventPool;

            this.fuNongContext = fuNongContext;
        }

        private readonly IBaseService<ds_machine_list> machineCRUDService;
        private readonly IBaseService<ds_machine_controller> controllerCRUDService;
        private readonly IBaseService<ds_machine_collector> collectorCRUDService;

        private readonly IMachineService machineService;
        private readonly ISchoolService schoolService;

        private readonly ICookie cookie;
        private readonly ILoggerService logger;
        private readonly IEventPool eventPool;

        private readonly IFuNongContext fuNongContext;

        #region 单页面

        public ActionResult MachineList()
        {
            return View();
        }
        
        public ActionResult ControllerConfig()
        {
            return View();
        }

        public ActionResult CollectorConfig()
        {
            return View();
        }

        public ActionResult IconSelection()
        {
            return View();
        }

        public ActionResult ParamSelection()
        {
            return View();
        }

       
        #endregion

        #region 设备列表维护
        [HttpPost]
        public JsonResult GetMachineListByPage()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var companyList = GetCompanyIDListByCurrentUser();
            
            var machineRequest = new MachineRequest();
            machineRequest.pageCount = rows;
            machineRequest.currentIndex = page;
            machineRequest.orderBy = x => x.updatetime;
            machineRequest.where = x => companyList.Contains(x.school_id);
            machineRequest.propertyName = propertyName;
            machineRequest.propertyValue = propertyValue;

            var roleList = machineService.TriggerList(machineRequest);

            int totalCount = 0;
            if (roleList != null && roleList.Count > 0)
            {
                totalCount = roleList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = roleList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult GetMachineBy(int? companyId, int? isController)
        {
            var collection = cookie.GetCookieCollection("FuNong.UserInfo.Login");
            if (collection == null)
                new RedirectResult("Home/Login");

            var uid = collection["FuNong.Login.UserID"];
            int uidCon = 0;
            if (!Int32.TryParse(uid, out uidCon))
            {
                return Json(new { success = false, message = "用户编号不是整数型，无法转换！" }, JsonRequestBehavior.AllowGet);
            }

            var request = new MachineRequest();
            request.pageCount = Int32.MaxValue;
            request.currentIndex = 0;
            request.orderBy = x => x.updatetime;

            if (companyId == null)
            {
                //if (isController == null)
                //    request.where = x => x.machine_id != string.Empty && (x.machine_type == 3 || x.machine_type == 4);
                //else
                //    request.where = x => x.machine_id != string.Empty && (x.machine_type == 1 || x.machine_type == 2);
                request.where = x => x.machine_id != string.Empty;
            }
            else
            {
                //if (isController == null)
                //    request.where = x => x.school_id == companyId && (x.machine_type == 3 || x.machine_type == 4);
                //else
                //    request.where = x => x.school_id == companyId && (x.machine_type == 1 || x.machine_type == 2);
                request.where = x => x.school_id == companyId;
            }

            var machineList = machineService.TriggerList(request);

            return Json(new { success = true, data = machineList }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetMachineDetailsByID(int companyId,string machineId)
        {
            var collection = cookie.GetCookieCollection("FuNong.UserInfo.Login");
            if (collection == null)
                new RedirectResult("Home/Login");

            var uid = collection["FuNong.Login.UserID"];
            int uidCon = 0;
            if (!Int32.TryParse(uid, out uidCon))
            {
                return Json(new { success = false, message = "用户编号不是整数型，无法转换！" }, JsonRequestBehavior.AllowGet);
            }

            var request = new MachineRequest();
            request.pageCount = Int32.MaxValue;
            request.currentIndex = 0;
            request.orderBy = x => x.updatetime;
            if (machineId == null)
                request.where = x => x.school_id == companyId;
            else
                request.where = x => x.school_id == companyId && x.machine_id == machineId;

            var controllerAll = new List<controller>();

            //获取设备列表
            var machineList = machineService.TriggerList(request);

            if (machineList != null)
            {
                //获取所有的设备ID
                var machineIDList = machineList.Select(x => x.machine_id);

                var cRequest = new ControllerRequest();
                cRequest.pageCount = Int32.MaxValue;
                cRequest.currentIndex = 0;
                cRequest.orderBy = x => x.updatetime;
                cRequest.where = x => machineIDList.Contains(x.machine_id);

                //获取到了所有的控制设备详细信息
                var controllerList = machineService.TriggerList(cRequest);

                if (controllerList != null)
                {
                    //遍历设备信息表
                    machineList.ForEach(parent =>
                    {
                        var controllerEntity = new controller();
                        controllerEntity.title = parent.machine_name;
                        controllerEntity.machineid = parent.machine_id;
                        controllerEntity.routers = new List<router>();
                        controllerEntity.leds = new List<led>();
                        //遍历控制设备详细信息表
                        controllerList.ForEach(child =>
                        {
                            //如果为同一设备
                            if (parent.machine_id == child.machine_id)
                            {
                                router router = null;
                                led led = null;

                                if (child.machine_type == 3)
                                {
                                    router = new router();
                                    //可控制路数
                                    router.routeid = child.route_id;
                                    router.title = child.route_name;
                                    router.functionsList = new List<functions>();
                                }
                                if (child.machine_type == 4)
                                {
                                    led = new led();
                                    //指示灯路数
                                    led.routerid = child.route_id;
                                    led.title = child.route_name;
                                    led.functionsList = new List<functions>();
                                }

                                #region 拆分每个router上面的功能
                                var rMul = child.route_multiple;
                                if (rMul.Contains('|'))
                                {
                                    var rMulList = child.route_multiple.Split('|').ToList();
                                    rMulList.ForEach(item =>
                                    {
                                        var functions = new functions();
                                        if (item.Contains(","))
                                        {
                                            var currentFuncList = item.Split(',').ToList();
                                            functions.functionflag = currentFuncList[0];
                                            functions.functionname = currentFuncList[1];
                                            functions.functionicon = currentFuncList[2];

                                            functions.functionorder = "1";
                                            functions.state = false;

                                            if (child.machine_type == 3) router.functionsList.Add(functions);

                                            if (child.machine_type == 4) led.functionsList.Add(functions);
                                        }
                                        else
                                        {
                                            //配置有问题 
                                            //TODO
                                        }
                                    });

                                    if (child.machine_type == 3) controllerEntity.routers.Add(router);

                                    if (child.machine_type == 4) controllerEntity.leds.Add(led);
                                }
                                else
                                {
                                    //配置有问题
                                    //TODO
                                }
                                #endregion
                            }
                        });

                        controllerAll.Add(controllerEntity);
                    });
                }
            }

            return Json(new { success = true, data = controllerAll }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetRealTimeCollectorDataByMachine(string machineId)
        {
            string sql = @" select filtered.*,c.route_name,d.machine_name,setting.settingname,setting.settingvalue4 from (
                            select a.machine_id
                                  ,a.route_id
                                  ,a.param_id
                                  ,a.data_value
                                  ,a.data_time 
                            from ds_collector_data a
                            join (
	                            select machine_id
	                                  ,route_id
		                              ,param_id
		                              ,MAX(data_time) as data_time
	                              from ds_collector_data
	                                where machine_id='"+machineId+@"'
	                                   group by route_id,param_id,machine_id) b
	                              on a.route_id = b.route_id and a.param_id = b.param_id and a.data_time = b.data_time and a.machine_id = b.machine_id
	                              ) filtered
                               left join ds_machine_collector c on filtered.machine_id = c.machine_id and filtered.route_id = c.route_id and filtered.param_id = c.param_id
                               left join ds_machine_list d on filtered.machine_id = d.machine_id 
                               left join edu_base_setting setting on setting.settingvalue1 = filtered.param_id
                               where setting.settinggroup='param'
                              ";

            var resultList = fuNongContext.SqlQuery<CollectorModel>(sql).Cast<CollectorModel>().ToList();

            return Json(new { success = true, data = resultList }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetHistoryCollectorDataByMachine(string machineId,string paramId,string starttime,string endtime)
        {
            DateTime start,end;
            
            if(DateTime.TryParse(starttime,out start))
                starttime= start.ToString("yyyy-MM-dd")+" 00:00:00";

            if (DateTime.TryParse(endtime, out end))
                endtime = end.ToString("yyyy-MM-dd") + " 23:59:59";

            //首先获取采集设备的参数列表
            var whereSQL = " where data.machine_id='" + machineId + "' ";
            if (!string.IsNullOrEmpty(paramId))
                whereSQL += "  and param_id = '" + paramId.ToString() + "' ";

            var groupSQL = " group by data.route_id,data.param_id,data.machine_id,setting.settingname,setting.settingvalue4 ";

            var mainSQL = @"select machine_id
                               ,route_id
                               ,param_id
                               ,setting.settingname
                               ,setting.settingvalue4
                          from ds_collector_data data
                          join edu_base_setting setting 
                          on data.param_id = setting.settingvalue1 ";

            var allSQL = mainSQL + whereSQL + groupSQL;

            var groupResultList = fuNongContext.SqlQuery<CollectorGroup>(allSQL).Cast<CollectorGroup>().ToList();

            var chartDataList = new List<CollectorChartFmt>();
            var plainDataList = new List<CollectorChartFmt>();

            //遍历参数列表取数据
            if (groupResultList != null)
            {
                //遍历采集器
                groupResultList.ForEach(item =>
                {
                    var historySelectSQL = @" select  data.data_value,data.data_time from ds_collector_data data ";
                    var historyWheresSQL = @" where data.machine_id='" + item.machine_id + "' and data.route_id=" + item.route_id + " and data.param_id='" + item.param_id + "' ";
                    var historyOrdersSQL = @" order by data.data_time desc ";

                    var historyFilterSQL = string.Empty;

                    if(!string.IsNullOrEmpty(starttime) && !string.IsNullOrEmpty(endtime))
                    {
                        historyFilterSQL = @" and (data.data_time >= '" + starttime + "' and data.data_time <='" + endtime + "') ";
                    }
                    if(string.IsNullOrEmpty(starttime) && string.IsNullOrEmpty(endtime))
                    {
                        historySelectSQL = @" select top 100 data.data_value,data.data_time from ds_collector_data data ";
                    }

                    var historyMainSQL = historySelectSQL + historyWheresSQL + historyFilterSQL + historyOrdersSQL;

                    var historyDataList = fuNongContext.SqlQuery<CollectorChart>(historyMainSQL).Cast<CollectorChart>().ToList();
                    if (historyDataList == null)
                    {
                        chartDataList.Add(new CollectorChartFmt() { Param_name = "无", Param_unit = "无", Param_data = "[]"});
                    }
                    else
                    {
                        var container = new List<string>();
                        historyDataList.ForEach(x =>
                        {
                            var time = "Date.UTC(" + x.data_time.ToString("yyyy,MM,dd,HH,mm,ss") + ")";
                            var data = x.data_value;
                            var val = "[" + time + "," + float.Parse(data) + "]";
                            container.Add(val);
                            plainDataList.Add(new CollectorChartFmt() { Param_name = item.settingname, Param_unit = item.settingvalue4, Param_data = data, Param_time = x.data_time.ToString("yyyy-MM-dd HH:mm:ss") });
                        });
                        chartDataList.Add(new CollectorChartFmt() { Param_name = item.settingname, Param_unit = item.settingvalue4, Param_data = string.Join(",", container) });
                    }

                });
            }

            return Json(new { success = true, data = chartDataList, list = plainDataList }, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public JsonResult GetVideoListBy(int companyId, int? monitorId)
        {
            var collection = cookie.GetCookieCollection("FuNong.UserInfo.Login");
            if (collection == null)
                new RedirectResult("Home/Login");

            var uid = collection["FuNong.Login.UserID"];
            int uidCon = 0;
            if (!Int32.TryParse(uid, out uidCon))
            {
                return Json(new { success = false, message = "用户编号不是整数型，无法转换！" }, JsonRequestBehavior.AllowGet);
            }

            var request = new VideoRequest();
            request.pageCount = Int32.MaxValue;
            request.currentIndex = 0;
            request.orderBy = x => x.updatetime;

            if (monitorId == null)
            {
                request.where = x => x.schoolid == companyId;
            }
            else
            {
                request.where = x => x.schoolid == companyId && x.id == monitorId;
            }

            var videoList = schoolService.GetVideoResponse(request);

            return Json(new { success = true, data = videoList }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult CommitMachineDetails()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<ds_machine_list>();
            return operationbase.Commit(machineCRUDService, inserted, updated, deleted, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (machineCRUDService.AddInBatch(list))
                    return Json(new { success = "设备信息新增成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "设备信息新增失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (machineCRUDService.UpdateInBatch(list))
                    return Json(new { success = "设备信息更新成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "设备信息更新失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                if (machineCRUDService.DeleteInBatch(list))
                    return Json(new { success = "设备信息移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "设备信息移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }
        #endregion

        #region 控制设备配置列表维护
        [HttpPost]
        public JsonResult GetControllerListByPage()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];


            var machineIDList = GetMachineIDListByCurrentUser();

            var controllerRequest = new ControllerRequest();
            controllerRequest.pageCount = rows;
            controllerRequest.currentIndex = page;
            controllerRequest.orderBy = x => x.updatetime;
            controllerRequest.where = x => machineIDList.Contains(x.machine_id);
            controllerRequest.propertyName = propertyName;
            controllerRequest.propertyValue = propertyValue;

            var controllerList = machineService.TriggerList(controllerRequest);

            int totalCount = 0;
            if (controllerList != null && controllerList.Count > 0)
            {
                totalCount = controllerList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = controllerList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult GetControllerById(int id)
        {
            var controllerRequest = new ControllerRequest();
            controllerRequest.pageCount = Int32.MaxValue;
            controllerRequest.currentIndex = 0;
            controllerRequest.orderBy = x => x.updatetime;
            controllerRequest.where = x => x.id == id;

            var machine = machineService.TriggerList(controllerRequest);

            JsonResult result = Json(machine, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult CommitControllerConfigDetails()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<ds_machine_controller>();
            return operationbase.Commit(controllerCRUDService, inserted, updated, deleted, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (controllerCRUDService.AddInBatch(list))
                    return Json(new { success = "控制设备配置成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "控制设备配置失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (controllerCRUDService.UpdateInBatch(list))
                    return Json(new { success = "控制设备配置信息更新成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "控制设备配置信息更新失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                if (controllerCRUDService.DeleteInBatch(list))
                    return Json(new { success = "控制设备配置信息移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "控制设备配置信息移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }
        #endregion

        #region 采集设备配置列表维护
        [HttpPost]
        public JsonResult GetCollectorListByPage()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var machineIDList = GetMachineIDListByCurrentUser();

            var collectorRequest = new CollectorRequest();
            collectorRequest.pageCount = rows;
            collectorRequest.currentIndex = page;
            collectorRequest.orderBy = x => x.updatetime;
            collectorRequest.where = x => machineIDList.Contains(x.machine_id);
            collectorRequest.propertyName = propertyName;
            collectorRequest.propertyValue = propertyValue;

            var roleList = machineService.TriggerList(collectorRequest);

            int totalCount = 0;
            if (roleList != null && roleList.Count > 0)
            {
                totalCount = roleList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = roleList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult CommitCollectorConfigDetails()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<ds_machine_collector>();
            return operationbase.Commit(collectorCRUDService, inserted, updated, deleted, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (collectorCRUDService.AddInBatch(list))
                    return Json(new { success = "采集设备配置成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "采集设备配置失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (collectorCRUDService.UpdateInBatch(list))
                    return Json(new { success = "采集设备配置信息更新成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "采集设备配置信息更新失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                if (collectorCRUDService.DeleteInBatch(list))
                    return Json(new { success = "采集设备配置信息移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "采集设备配置信息移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }
        #endregion

        #region 获取当前用户下的公司列表,设备列表
        private List<int> GetCompanyIDListByCurrentUser()
        {
            var collection = cookie.GetCookieCollection("FuNong.UserInfo.Login");
            var companyList = new List<int>();
            if (collection != null)
            {
                var companyC = collection["FuNong.Login.CompanyList"];
                if (companyC.Contains(','))
                {
                    var companyArray = collection["FuNong.Login.CompanyList"].Split(',').ToList();
                    companyArray.ForEach(x => companyList.Add(Int32.Parse(x)));
                }
                else
                {
                    companyList.Add(Int32.Parse(companyC));
                }
            }
            return companyList;
        }

        private List<string> GetMachineIDListByCurrentUser()
        {
            var companyIDList = GetCompanyIDListByCurrentUser();
            var request = new MachineRequest();
            request.pageCount = Int32.MaxValue;
            request.currentIndex = 0;
            request.orderBy = x => x.updatetime;
            request.where = x => companyIDList.Contains(x.school_id);

            var machineIDListEntity = machineService.TriggerList(request);

            var machineIDListString = new List<string>();
            if (machineIDListEntity != null)
            {
                machineIDListEntity.ForEach(x => machineIDListString.Add(x.machine_id));
            }

            return machineIDListString;
        }
        #endregion
    }
}
