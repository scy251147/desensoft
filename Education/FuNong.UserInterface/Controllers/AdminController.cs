﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FuNong.DataContract;
using FuNong.Service;
using FuNong.Infrastructure.Json;
using FuNong.DataAccess;
using FuNong.Framework.Cookie;
using FuNong.Framework.Logger;
using FuNong.UserInterface.Models;
using Newtonsoft.Json;
using FuNong.Framework.PoolCenter;
using FuNong.Framework.Randoms;
using FuNong.Framework.MD5;
using System.Transactions;

namespace FuNong.UserInterface.Controllers
{
    public class AdminController : BaseController
    {
        public AdminController(
               IBaseService<edu_department_detail> departmentCRUDService
             , IBaseService<edu_role_detail> roleCRUDService
             , IBaseService<bma_users> bmaUsersCRUDService
             , IBaseService<edu_user_detail> userCRUDService
             , IBaseService<edu_model_detail> modelCRUDService
             , IBaseService<edu_role_model> roleModelCRUDService
             , IBaseService<edu_field_control> fControlCRUDService
             , IRepository<bma_users> mainUserRepo
             , IRepository<edu_user_detail> detaUserRepo
             , IRepository<edu_model_detail> modelRepo
             , IDepartmentService departmentService
             , IBaseDataService baseDataService
             , IRoleService roleService
             , IUserService userService
             , IModelService modelService
             , INewsService newsService
             , ICookie cookie
             , ILoggerService logger
             , IEventPool eventPool
            ):base(cookie,logger,eventPool,true)
        {
            this.departmentCRUDService = departmentCRUDService;
            this.roleCRUDService = roleCRUDService;
            this.bmaUsersCRUDService = bmaUsersCRUDService;
            this.userCRUDService = userCRUDService;
            this.modelCRUDService = modelCRUDService;
            this.roleModelCRUDService = roleModelCRUDService;
            this.fControlCRUDService = fControlCRUDService;

            this.mainUserRepo = mainUserRepo;
            this.detaUserRepo = detaUserRepo;
            this.modelRepo = modelRepo;

            this.departmentService = departmentService;
            this.baseDataService = baseDataService;
            this.roleService = roleService;
            this.userService = userService;
            this.modelService = modelService;

            this.newsService = newsService;

            this.cookie = cookie;
            this.logger = logger;
            this.eventPool = eventPool;
        }

        private readonly IBaseService<edu_department_detail> departmentCRUDService;
        private readonly IBaseService<edu_role_detail> roleCRUDService;
        private readonly IBaseService<edu_model_detail> modelCRUDService;
        private readonly IBaseService<edu_role_model> roleModelCRUDService;
        private readonly IBaseService<edu_field_control> fControlCRUDService;

        private readonly IBaseService<bma_users> bmaUsersCRUDService;
        private readonly IBaseService<edu_user_detail> userCRUDService;

        private readonly IDepartmentService departmentService;
        private readonly IRoleService roleService;
        private readonly IUserService userService;
        private readonly IModelService modelService;
        private readonly INewsService newsService;
        private readonly IBaseDataService baseDataService;

        private readonly ICookie cookie;
        private readonly ILoggerService logger;
        private readonly IEventPool eventPool;

        private readonly IRepository<bma_users> mainUserRepo;
        private readonly IRepository<edu_user_detail> detaUserRepo;
        private readonly IRepository<edu_model_detail> modelRepo;

        #region 后台主体框架
        [ValidateInput(false)]
        public ActionResult A()
        {
            var mrRequest = new ModelRoleRequest();

            //从cookie中获取已登录用户的roleid信息
            var cookie = new CookieWrapper();
            var nv = cookie.GetCookieCollection("FuNong.UserInfo.Login");
            var roleid = string.Empty;
            if (nv != null)
                roleid = nv["FuNong.Login.RoleID"];

            int roleidInInt32;

            if (string.IsNullOrEmpty(roleid))
                throw new Exception("角色编号为空，无法创建权限树！");


            if (!Int32.TryParse(roleid, out roleidInInt32))
                throw new Exception("角色编号不为整型，无法创建权限树！");

            mrRequest.roleid = roleidInInt32;
            var modelList = modelService.GetModelApply(mrRequest);

            if (modelList == null || modelList.Count == 0)
                throw new Exception("当前角色未分配任何权限，无法创建权限树！");



            var menuParent = modelList.Where(x => x.parentid == 0).ToList();
            var menuParentList = new List<MenuParent>();
            menuParent.ForEach(item =>
            {
                var itemex = new MenuParent();
                itemex.icon = item.modelicon ?? "icon-flag_blue";
                itemex.menuid = item.id;
                itemex.menuname = item.modelname;
                itemex.menus = new List<Menu>();
                itemex.recordorder = item.recordorder;
                menuParentList.Add(itemex);
            });

            var menuChildrenList = new List<Menu>();
            var menuChildren = modelList.Where(x => x.parentid != 0 && x.parentid != null).ToList();
            if (menuChildren != null && menuChildren.Count > 0)
            {
                menuChildren.ForEach(item =>
                {
                    menuParentList.ForEach(parent =>
                    {
                        if (parent.menuid == item.parentid)
                        {
                            Menu child = new Menu();
                            child.menuid = item.id;
                            child.menuname = item.modelname;
                            child.url = item.modelpath;
                            child.icon = item.modelicon ?? "icon-flag_blue";
                            child.recordorder = item.recordorder;
                            parent.menus.Add(child);
                        }
                    });

                });
            }

            menuParentList.ForEach(parent =>
            {
                parent.menus = parent.menus.OrderBy(x => x.recordorder).ToList();
            });
            menuParentList = menuParentList.OrderBy(x => x.recordorder).ToList();

            MenusRoot root = new MenusRoot();
            root.menus = menuParentList;

            var jsonString = JsonConvert.SerializeObject(root);

            ViewBag.menujson = jsonString;

            return View();
        }

        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region 单页面
        public ActionResult UserDetail(string rolename)
        {
            ViewBag.name = rolename;
            return View();
        }

        public ActionResult DepartmentDetail()
        {
            return View();
        }

        public ActionResult RoleDetail()
        {
            return View();
        }

        public ActionResult ModelDetail()
        {
            return View();
        }

        public ActionResult PermissionControl()
        {
            return View();
        }

        public ActionResult ViewUserInfo()
        {
            var result = GetLoginUserInfo();
            return View(result);
        }

        public ActionResult ModifyPwd(string oldpwd, string newpwd, string confirmpwd)
        {
            var resultUser = GetLoginUserInfo();

            if (string.IsNullOrEmpty(oldpwd))
                return View(new UserResponse() { Success = false, Message = "旧登录密码不能为空！",username=resultUser.username });
            if (string.IsNullOrEmpty(newpwd))
                return View(new UserResponse() { Success = false, Message = "新登录密码不能为空！", username = resultUser.username });
            if (string.IsNullOrEmpty(confirmpwd))
                return View(new UserResponse() { Success = false, Message = "确认密码不能为空！", username = resultUser.username });
            if (newpwd != confirmpwd)
                return View(new UserResponse() { Success = false, Message = "两次输入的密码不一致！", username = resultUser.username });

            if (resultUser.Success)
            {
                var oldPwdCalculate = FuNong.Framework.MD5.MD5Wrapper.Md5Wrapper.CreateUserPassword(oldpwd, resultUser.salt);
                if (oldPwdCalculate.Equals(resultUser.userpass))
                {
                    var list = new List<bma_users>();
                    var uEntity = mainUserRepo.Get(x => x.uid == resultUser.uid);
                    if(uEntity!=null)
                    {
                        uEntity.password = FuNong.Framework.MD5.MD5Wrapper.Md5Wrapper.CreateUserPassword(newpwd, uEntity.salt);
                    }

                    list.Add(uEntity);
                    var flag = bmaUsersCRUDService.UpdateInBatch(list);
                    if (flag)
                    {
                        return View(new UserResponse() { Success = true, Message = "登录密码修改成功！", username = resultUser.username });
                    }
                    else
                    {
                        return View(new UserResponse() { Success = false, Message = "登录密码修改失败，请重试！", username = resultUser.username });
                    }
                }
                else
                {
                    return View(new UserResponse() { Success = false, Message = "旧登录密码输入错误！", username = resultUser.username });
                }
            }
            else
            {
                return View(resultUser);
            }
        }

        public ActionResult CompleteUserInfo(UserResponse userResponse)
        {
            var userinfo = GetLoginUserInfo();

            //用户刚打开该页面
            if (string.IsNullOrEmpty(userResponse.idcard) || string.IsNullOrEmpty(userResponse.nickname))
            {
                return View(userinfo);
            }
            else  //用户提交修改操作
            {
                var cookie = new FuNong.Framework.Cookie.CookieWrapper();
                var nv = cookie.GetCookieCollection("FuNong.UserInfo.Login");
              
                if (nv == null)
                {
                    return View(new UserResponse() { Success = false, Message = "当前登录用户已过期，请重新登录！" });
                }
                
                var userid =  Int32.Parse(nv["FuNong.Login.UserID"]);

                var mainUser = mainUserRepo.Get(x => x.uid == userid);
                var detaUser = detaUserRepo.Get(x=>x.uid == userid);

                detaUser.idcard = userResponse.idcard;
                mainUser.nickname = userResponse.nickname;
                mainUser.email = userResponse.usermail;
                mainUser.mobile = userResponse.userphone;
                detaUser.useraddress = userResponse.useraddress;
                detaUser.usernote = userResponse.usernote;

                var mainList = new List<bma_users>();
                var detaList = new List<edu_user_detail>();
                mainList.Add(mainUser);
                detaList.Add(detaUser);

                using (var scope = new TransactionScope())
                {
                    var mainFlag = bmaUsersCRUDService.UpdateInBatch(mainList);
                    var detaFlag = userCRUDService.UpdateInBatch(detaList);

                    if (mainFlag && detaFlag)
                    {
                        scope.Complete();
                        return View(new UserResponse() { Success = true, Message = "用户基本信息修改成功！", username = userinfo.username });
                    }
                    else
                    {
                        return View(new UserResponse() { Success = false, Message = "用户基本信息修改失败，请重试！", username = userinfo.username });
                    }
                }
            }
        }

        private UserResponse GetLoginUserInfo()
        {
            var cookie = new FuNong.Framework.Cookie.CookieWrapper();
            var nv = cookie.GetCookieCollection("FuNong.UserInfo.Login");
            string userid = string.Empty;
            if (nv != null)
            {
                userid = nv["FuNong.Login.UserID"];
                if (!string.IsNullOrEmpty(userid))
                {
                    var useridInt = Int32.Parse(userid);
                    var user = new UserRequest();
                    user.currentIndex = 0;
                    user.pageCount = 1;
                    user.where = x => x.uid == useridInt;
                    user.orderBy = x => x.updatetime;

                    var response = userService.TriggerList(user);

                    if (response == null)
                    {
                        return new UserResponse() { Success = false, Message = "无法获取用户信息，请确认系统中有此用户存在！" };
                    }
                    else if (response.Count == 0)
                    {
                        return new UserResponse() { Success = false, Message = "无法获取用户信息，请确认系统中有此用户存在！" };
                    }
                    else
                    {
                        var res = response[0];
                        res.Success = true;
                        return res;
                    }
                }
                else
                {
                    return new UserResponse() { Success = false, Message = "当前登录用户已过期，请重新登录！" };
                }
            }
            else
            {
                return new UserResponse() { Success = false, Message = "当前登录用户已过期，请重新登录！" };
            }
        }

        #endregion

        #region 用户信息维护
        [HttpPost]
        public JsonResult GetUserDetailByPage()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            string roleId = Request.Params["roleid"];
            int roleIdInt = -1;  //默认为一个不存在的角色
            Int32.TryParse(roleId, out roleIdInt);

            var userRequest = new UserRequest();
            userRequest.pageCount = rows;
            userRequest.currentIndex = page;
            userRequest.orderBy = x => x.updatetime;

            //if (IsMultiSchool.Equals("1"))  //多校
                userRequest.where = x => x.uid != 0 && x.roleid == roleIdInt;
            //else
            //    userRequest.where = x => x.uid != 0 && x.roleid == roleIdInt && x.schoolid == CurrentLoginSchoolID;

            userRequest.propertyName = propertyName;
            userRequest.propertyValue = propertyValue;

            var userlist = userService.TriggerList(userRequest);

            int totalCount = 0;
            if (userlist != null && userlist.Count > 0)
            {
                totalCount = userlist[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = userlist
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult CommitUserDetail()
        {
            var uid = Request.Form["uid"];
            var uidInt = 0;
            if (Int32.TryParse(uid, out uidInt))
            {
                var mainUserEntity = mainUserRepo.Get(x => x.uid == uidInt);
                var detaUserEntity = detaUserRepo.Get(x => x.uid == uidInt);

                var mainList = new List<bma_users>();
                var detaList = new List<edu_user_detail>();
                mainList.Add(mainUserEntity);
                detaList.Add(detaUserEntity);

                if (userCRUDService.DeleteInBatch(detaList) && bmaUsersCRUDService.DeleteInBatch(mainList))
                {
                    return Json(new UserResponse() { Success = true, Message = "删除当前用户成功！" });
                }
                else
                {
                    return Json(new UserResponse() { Success = false, Message = "删除当前用户失败，请重试！" });
                }
            }
            else
            {
                return Json(new UserResponse() { Success = false, Message = "用户标志编码不正确，删除失败！" });
            }
        }

        [HttpGet]
        public JsonResult CheckUserExist(string fieldValue)
        {
            var flag = UserExist(fieldValue);

            if (flag)
                return Json(new object[] { "username", false }, JsonRequestBehavior.AllowGet);
            else
                return Json(new object[] { "username", true }, JsonRequestBehavior.AllowGet);
        }
        private bool UserExist(string fieldValue)
        {
            var request = new UserRequest();
            request.currentIndex = 0;
            request.pageCount = Int32.MaxValue;
            request.username = fieldValue;

            var result = userService.TriggerMainUserList(request);

            if (result.uid <= 0)
                return false;
            else
                return true;
        }

        #endregion

        #region 部门信息维护
        [HttpPost]
        public JsonResult GetDepartmentDetailByPage()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            int totalCount = 0;


            var departmentRequest = new DepartmentRequest();
            departmentRequest.pageCount = rows;
            departmentRequest.currentIndex = page;
            departmentRequest.orderBy = x => x.updatetime;
            departmentRequest.where = x => x.id != 0;
            departmentRequest.propertyName = propertyName;
            departmentRequest.propertyValue = propertyValue;

            var userlist = departmentService.TriggerList(departmentRequest);

            var items = from p in userlist select p;

            var json = new
            {
                total = totalCount,
                rows = items
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult CommitDepartmentDetails()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<edu_department_detail>();
            return operationbase.Commit(departmentCRUDService, inserted, updated, deleted, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (departmentCRUDService.AddInBatch(list))
                    return Json(new { success = "部门详细信息新增成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "部门详细信息新增失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (departmentCRUDService.UpdateInBatch(list))
                    return Json(new { success = "部门详细信息更新成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "部门详细信息更新失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                if (departmentCRUDService.DeleteInBatch(list))
                    return Json(new { success = "部门详细信息移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "部门详细信息移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }
        #endregion

        #region 角色信息维护
        [HttpPost]
        public JsonResult GetRoleDetailByPage()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];


            var roleRequest = new RoleRequest();
            roleRequest.pageCount = rows;
            roleRequest.currentIndex = page;
            roleRequest.orderBy = x => x.updatetime;
            roleRequest.where = x => x.id != 0;
            roleRequest.propertyName = propertyName;
            roleRequest.propertyValue = propertyValue;

            var roleList = roleService.TriggerList(roleRequest);

            int totalCount = 0;
            if (roleList != null && roleList.Count > 0)
            {
                totalCount = roleList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = roleList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult CommitRoleDetails()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<edu_role_detail>();
            return operationbase.Commit(roleCRUDService, inserted, updated, deleted, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (roleCRUDService.AddInBatch(list))
                    return Json(new { success = "角色详细信息新增成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "角色详细信息新增失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (roleCRUDService.UpdateInBatch(list))
                    return Json(new { success = "角色详细信息更新成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "角色详细信息更新失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                if (roleCRUDService.DeleteInBatch(list))
                    return Json(new { success = "角色详细信息移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "角色详细信息移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }
        #endregion

        #region 模块信息维护
        [HttpPost]
        public JsonResult GetModelDetailByPage()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var modelRequest = new ModelRequest();
            modelRequest.pageCount = rows;
            modelRequest.currentIndex = page;
            modelRequest.orderBy = x => x.updatetime;
            modelRequest.where = x => x.id != 0;
            modelRequest.propertyName = propertyName;
            modelRequest.propertyValue = propertyValue;

            var roleList = modelService.TriggerList(modelRequest);

            int totalCount = 0;
            if (roleList != null && roleList.Count >0)
            {
                totalCount = roleList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = roleList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult SetIcon(int modelid,string icon)
        {
            var entity = modelRepo.Get(x => x.id == modelid);
            if (entity == null)
            {
                return Json(new { success = false, message = "无法获取选中模块的信息，请重试！" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                entity.modelicon = icon;
                entity.updatetime = DateTime.Now;
                var list = new List<edu_model_detail>();
                list.Add(entity);
                if (modelCRUDService.UpdateInBatch(list))
                {
                    return Json(new { success = true, message = "为选中模块设置图标成功！" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = "为选中模块设置图标失败，请重试！" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public JsonResult GetIconList()
        {
            var request = new IconRequest();
            request.currentIndex = 0;
            request.pageCount = Int32.MaxValue;
            request.where = x => x.id != 0;
            request.orderBy = x => x.updatetime;

            var result = baseDataService.GetIconList(request);

            if (result == null)
            {
                return Json(new { success = false, message = "按钮图标列表为空，请重试！" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, data = result }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CommitModelDetails()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<edu_model_detail>();
            return operationbase.Commit(modelCRUDService, inserted, updated, deleted, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (modelCRUDService.AddInBatch(list))
                    return Json(new { success = "模块详细信息新增成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "模块详细信息新增失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (modelCRUDService.UpdateInBatch(list))
                    return Json(new { success = "模块详细信息更新成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "模块详细信息更新失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                if (modelCRUDService.DeleteInBatch(list))
                    return Json(new { success = "模块详细信息移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "模块详细信息移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }

        //根据传入条件，获取已分配模块列表和未分配模块列表
        [HttpGet]
        public JsonResult GetModelTree(string roleid, bool apply)
        {
            if (string.IsNullOrEmpty(roleid))
                return Json(new { success = false, message = "角色编号不能为空!", data = "[{\"id\": -1, \"text\":\"角色编号不能为空！\"}]" }, JsonRequestBehavior.AllowGet);

            var modelRoleRequest = new ModelRoleRequest();
            modelRoleRequest.roleid = Int32.Parse(roleid);

            List<ModelResponse> modelList;
            if (apply)
                modelList = modelService.GetModelApply(modelRoleRequest);
            else
                modelList = modelService.GetModelUnApply(modelRoleRequest);

            var modelTree = modelService.ContructModelTree(modelList, "0");

            if (string.IsNullOrEmpty(modelTree))
                return Json(new { success = true, message = "", data ="[{\"id\": -1, \"text\":\"暂未分配任何模块！\"}]" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = true, message = "", data = modelTree }, JsonRequestBehavior.AllowGet);
        }

        //为角色新增相关访问模块:attach = true
        //为角色移除相关访问模块:attach = false
        [HttpGet]
        public JsonResult AttachModelToRole(string roleid,string modelidlist,bool attach)
        {
            int roleidInInt ;

            if (string.IsNullOrEmpty(roleid))
                return Json(new { success = false, message = "角色编号不能为空!", data = string.Empty }, JsonRequestBehavior.AllowGet);

            if (!Int32.TryParse(roleid, out roleidInInt))
                return Json(new { success = false, message = "角色编号类型错误!", data = string.Empty }, JsonRequestBehavior.AllowGet);

            if(string.IsNullOrEmpty(modelidlist))
                return Json(new { success = false, message = "请选择待分配模块!", data = string.Empty }, JsonRequestBehavior.AllowGet);

            var roleModelList = new List<edu_role_model>();
            modelidlist.Split(',').ToList().ForEach(item =>
            {
                int modelid;
                if (!string.IsNullOrEmpty(item) && Int32.TryParse(item, out modelid))
                {
                    var roleModel = new edu_role_model();
                    roleModel.roleid = roleidInInt;
                    roleModel.modelid = modelid;
                    roleModel.recordorder = 1;
                    roleModel.updatetime = DateTime.Now;
                    roleModelList.Add(roleModel);
                }
            });

            if (attach)  //新增相关访问模块
            {
                if (roleModelCRUDService.AddInBatch(roleModelList))
                    return Json(new { success = true, message = "为当前角色分配权限成功!", data = string.Empty }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = " 为当前角色分配权限，请重试！", data = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            else  //移除相关访问模块
            {

                ModelRoleRequest request = new ModelRoleRequest();
                request.roleid = roleidInInt;
                request.modelidlist = modelidlist;
                var responseList = modelService.GetRoleModelByIdList(request);

                if (responseList != null)
                {
                    roleModelList.ForEach(item =>
                    {
                        var findEntity = (from p in responseList where p.modelid == item.modelid && p.roleid == item.roleid select p).FirstOrDefault();
                        if (findEntity != null)
                        {
                            item.id = findEntity.id;
                        }
                    });
                }

                if (roleModelCRUDService.DeleteInBatch(roleModelList))
                    return Json(new { success = true, message = "当前角色移除权限成功!", data = string.Empty }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = " 当前角色移除权限失败，请重试！", data = string.Empty }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region 获取相关详细信息
        [HttpPost]
        public JsonResult GetAllDepartments()
        {
            var departmentRequest = new DepartmentRequest();
            departmentRequest.departmentid = 0;
            departmentRequest.pageCount = 1000;
            departmentRequest.currentIndex = 0;
            departmentRequest.where = x => x.id != 0;
            departmentRequest.orderBy = x => x.updatetime;
            var departmentList = departmentService.TriggerList(departmentRequest);

            return Json(departmentList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetAllRoles()
        {
            var roleList = GetAllRolesList();
            return Json(roleList, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetRolesBy(int id)
        {
            var roleList = GetAllRolesList().Where(x => x.id == id);
            return Json(roleList, JsonRequestBehavior.AllowGet);
        }


        private List<RoleResponse> GetAllRolesList()
        {
            var roleRequest = new RoleRequest();
            roleRequest.roleid = 0;
            roleRequest.pageCount = 1000;
            roleRequest.currentIndex = 0;
            roleRequest.where = x => x.id != 0;
            roleRequest.orderBy = x => x.updatetime;

            var roleList = roleService.TriggerList(roleRequest);

            return roleList;
        }

        [HttpPost]
        public JsonResult GetParentModels()
        {
            var modelRequest = new ModelRequest();
            modelRequest.modelid = 0;
            modelRequest.pageCount = 1000;
            modelRequest.currentIndex = 0;
            modelRequest.orderBy = x => x.updatetime;
            modelRequest.where = x => x.id != 0;
            modelRequest.propertyName = string.Empty;
            modelRequest.propertyValue = string.Empty;

            var modelList = modelService.TriggerList(modelRequest);
            modelList.Add(new ModelResponse() { id = 0, modelname = "父菜单", parentid = 0, recordorder = 0 });
            modelList = modelList.Where(x => x.parentid == 0 || x.parentid == null).OrderBy(x => x.recordorder).ToList();
           
            return Json(modelList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetUserDetailsBy(int uid)
        {
            var request = new UserRequest();
            request.pageCount = Int32.MaxValue;
            request.currentIndex = 0;
            request.where = x => x.uid == uid;
            request.orderBy = x => x.updatetime;

            var result = userService.TriggerList(request).FirstOrDefault();
            if (result == null)
                result = new UserResponse() { Success = false, Message = "数据库中无此用户信息！" };
            else
                result.Success = true;
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 登出
        [HttpGet]
        public JsonResult Logout()
        {
            var currentLoginUserDetails = cookie.GetCookieCollection("FuNong.UserInfo.Login");
            if (currentLoginUserDetails == null)  //当前用户已经被注销
            {
                return Json(new UserValidationModel() { success = false, message = "当前用户已经不存在！" },JsonRequestBehavior.AllowGet);
            }
            else
            {
                var nv = cookie.GetCookieCollection("FuNong.UserInfo.Login");
                string userid = string.Empty;
                string username = string.Empty;
                string roleid = string.Empty;
                string userrole = string.Empty;
                LogContent logContent = null;
                if (nv != null)
                {
                    userid = nv["FuNong.Login.UserID"];
                    username = nv["FuNong.Login.UserName"];
                    roleid = nv["FuNong.Login.RoleID"];
                    userrole = HttpUtility.UrlDecode(nv["FuNong.Login.RoleName"]);
                    log4net.LogicalThreadContext.Properties["u_id"] = userid;
                    logContent = new LogContent("用户[" + username + "：" + userrole + "]成功注销本系统!", userid, "0");
                }
                
                logger.Info(logContent);

                cookie.ClearCooke("FuNong.UserInfo.Login");

                return Json(new UserValidationModel() { success = true},JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region 站内信

        public ActionResult InnerMailBox()
        {
            //站内消息只发送给具有审核权限的人群 Admin
            //权属审核提醒
            eventPool.Subscribe("FN.ApproveProcess3", (sender, e) =>
            {
                ViewBag.temp = "有新的权属审核信息需要确认！";
            });
            eventPool.FireEvent("FN.ApproveProcess3");

            //土地信息发布提醒
            eventPool.Subscribe("FN.ApproveProcess4", (sender, e) =>
            {
                ViewBag.temp = "有新的土地发布信息需要确认！";
            });
            eventPool.FireEvent("FN.ApproveProcess4");

            //缴费信息发布提醒
            eventPool.Subscribe("FN.ApproveProcess8", (sender, e) =>
            {
                ViewBag.temp = "有新的缴费信息需要确认！";
            });
            eventPool.FireEvent("FN.ApproveProcess8");

            //资讯信息发布提醒
            eventPool.Subscribe("FN.ApproveNews", (sender, e) =>
            {
                ViewBag.temp = "有新的资讯信息需要确认！";
            });
            eventPool.FireEvent("FN.ApproveNews");
            
            //新用户注册提醒
            eventPool.Subscribe("FN.UserRegister", (sender, e) =>
            {
                ViewBag.temp = "有新的注册用户需要确认！";
            });
            eventPool.FireEvent("FN.UserRegister");

            return View();
        }

        #endregion 

        #region 用户新增页面
        public ActionResult UserMaintain(UserResponse response)
        {
            if (response.method ==null)  //表示要新增,直接返回新建页面
            {
                return View();
            }
            if(response.method.Equals("add"))
            {
                if(string.IsNullOrEmpty(response.username)) return View(new UserResponse() { Success = false, Message = "必须填写登陆名称！" });
                if (UserExist(response.username)) return View(new UserResponse() { Success = false, Message = "当前登录名称已存在，请重新选择一个！" });
                if (string.IsNullOrEmpty(response.userpass)) return View(new UserResponse() { Success = false, Message = "必须填写登陆密码！" });
                if (string.IsNullOrEmpty(response.userphone)) return View(new UserResponse() { Success = false, Message = "必须填写联系电话！" });
                if (response.genderid < 0) return View(new UserResponse() { Success = false, Message = "必须选择性别！" });
                if (response.roleid <= 0) return View(new UserResponse() { Success = false, Message = "必须选择用户角色！" });
                if (string.IsNullOrEmpty(response.nickname)) return View(new UserResponse() { Success = false, Message = "必须填写真实姓名！" });
                if (string.IsNullOrEmpty(response.usermail)) return View(new UserResponse() { Success = false, Message = "必须填写电子邮箱！" });

                var userEntity = new bma_users();
                var userDetail = new edu_user_detail();
                var ramLib = new RandomWrapper();

                //---bma_users用户添加
                userEntity.salt = ramLib.CreateRandomValue(6);
                userEntity.password = MD5Wrapper.Md5Wrapper.CreateUserPassword(response.userpass, userEntity.salt);
                userEntity.username = response.username;
                userEntity.email = response.usermail;
                userEntity.mobile = response.userphone;
                
                userEntity.userrid = 7;
                userEntity.storeid = 0;
                userEntity.mallagid = 1;
                userEntity.nickname = response.nickname;
                userEntity.avatar = string.Empty;
                userEntity.paycredits = 0;
                userEntity.rankcredits = 0;
                userEntity.verifyemail = 0;
                userEntity.verifymobile = 0;
                userEntity.liftbantime = DateTime.Parse("1900-01-01 00:00:00.000");
                
                var bmaUserList = new List<bma_users>();
                bmaUserList.Add(userEntity);

                if (bmaUsersCRUDService.AddInBatch(bmaUserList)) //添加用户完毕
                {
                    var request = new UserRequest();
                    request.username = response.username;
                    request.pageCount = 1;
                    request.currentIndex = 0;

                    var currentInsertUser = userService.TriggerMainUserList(request);

                    if (currentInsertUser == null)
                    {
                        return View(new UserResponse() { Success = false, Message = "无法获取用户信息，新增用户失败！" });
                    }
                    else
                    {
                        var uid = currentInsertUser.uid;

                        userDetail.uid = uid;

                        userDetail.roleid       = response.roleid;
                        userDetail.departmentid = response.departmentid;
                        userDetail.smsnumber    = response.smsnumber;
                        userDetail.personcode   = response.personcode;
                        userDetail.idcard       = response.idcard;
                        userDetail.checkincard  = response.checkincard;
                        userDetail.genderid     = response.genderid;
                        userDetail.personstateid = 0;//初始置为正式状态
                        userDetail.ischeck      = 1;  //初始置为已审核状态
                        userDetail.qq           = response.qq;
                        userDetail.host         = response.host;
                        userDetail.useraddress  = response.useraddress;
                        userDetail.usernote     = response.usernote;
                        userDetail.createtime   = DateTime.Now;
                        userDetail.recordorder  = 1;
                        userDetail.updatetime   = DateTime.Now;

                        var userList = new List<edu_user_detail>();
                        userList.Add(userDetail);

                        if (userCRUDService.AddInBatch(userList))
                        {
                            var collection = cookie.GetCookieCollection("FuNong.UserInfo.Login");
                            if (collection != null)
                            {
                                var userid = collection["FuNong.Login.UserID"];
                                var typeid = "1";
                                logger.Info(new LogContent("添加了用户名为:" + response.username + "的用户!", userid, typeid));
                            }
                            
                            return View(new UserResponse() { Success = true, Message = "新增当前用户成功，请等待审核通过！" });
                        }
                        else
                        {
                            return View(new UserResponse() { Success = false, Message = "新增当前用户失败，请重试！" });
                        }
                    }
                }
                else
                {
                    return View(new UserResponse() { Success = false, Message = "新增当前用户失败，请重试！" });
                }
            }
            if (response.method.Equals("update")) //表示要修改，需要先load之前的数据
            {
                var request = new UserRequest();
                request.pageCount = Int32.MaxValue;
                request.currentIndex = 0;
                request.where = x => x.uid == response.uid;
                request.orderBy = x => x.updatetime;

                var currentUpdateUser = userService.TriggerList(request).FirstOrDefault();

                if (currentUpdateUser == null)
                {
                    return View(new UserResponse() { Success = false, Message = "无法获取用户信息，更新用户信息失败！" });
                }
                else
                {
                    var userEntity = new bma_users();
                    var userDetail = new edu_user_detail();

                    userEntity.uid = currentUpdateUser.uid;
                    userEntity.username = currentUpdateUser.username;
                    userEntity.email = response.usermail;
                    userEntity.mobile = response.userphone;
                    userEntity.password = currentUpdateUser.userpass;
                    userEntity.salt = currentUpdateUser.salt;
                    userEntity.userrid = 7;
                    userEntity.storeid = 0;
                    userEntity.mallagid = 1;
                    userEntity.nickname = response.nickname;
                    userEntity.avatar = string.Empty;
                    userEntity.paycredits = 0;
                    userEntity.rankcredits = 0;
                    userEntity.verifyemail = 0;
                    userEntity.verifymobile = 0;
                    userEntity.liftbantime = DateTime.Parse("1900-01-01 00:00:00.000");

                    var bmaUserList = new List<bma_users>();
                    bmaUserList.Add(userEntity);

                    userDetail.uid = currentUpdateUser.uid;
                    //userDetail.schoolid = currentUpdateUser.schoolid;
                    userDetail.roleid = currentUpdateUser.roleid;
                    userDetail.departmentid = response.departmentid;
                    //userDetail.classid = response.classid;
                    //userDetail.refuid = response.refuid;
                    userDetail.smsnumber = response.smsnumber;
                    userDetail.personcode = response.personcode;
                    userDetail.idcard = response.idcard;
                    userDetail.checkincard = response.checkincard;
                    userDetail.genderid = currentUpdateUser.genderid;
                    userDetail.personstateid = currentUpdateUser.personstateid;
                    userDetail.ischeck = 1;
                    userDetail.qq = response.qq;
                    userDetail.host = response.host;
                    userDetail.useraddress = response.useraddress;
                    userDetail.usernote = response.usernote;
                    userDetail.createtime = currentUpdateUser.createtime;
                    userDetail.recordorder = currentUpdateUser.recordorder;
                    userDetail.updatetime = DateTime.Now;

                    var userDetailList = new List<edu_user_detail>();
                    userDetailList.Add(userDetail);

                    if (bmaUsersCRUDService.UpdateInBatch(bmaUserList) && userCRUDService.UpdateInBatch(userDetailList))
                    {
                        return View(new UserResponse() { Success = true, Message = "更新当前用户成功，请刷新页面查看！" });
                    }
                    else
                    {
                        return View(new UserResponse() { Success = false, Message = "更新当前用户失败，请重试！" });
                    }
                }
            }
            return View();
        }

        public ActionResult UserLayout()
        {
            var roleList = GetAllRolesList();
            return View(roleList);
        }
        #endregion

        #region 数据库字段权限控制

        public ActionResult FieldControl()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetFileControl()
        {
            var request = new FieldControlRequest();
            request.pageCount = Int32.MaxValue;
            request.currentIndex = 0;
            request.orderBy = x => x.updatetime;
            request.where = x => x.id != 0;

            var modelList = baseDataService.GetFieldControlResponse(request);

            int totalCount = 0;
            if (modelList != null && modelList.Count > 0)
            {
                totalCount = modelList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = modelList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult CommitFieldControl()
        {
            var editaction = Request.Form["editaction"];

            var roleid = Request.Form["roleid"];
            var databasename = Request.Form["databasename"];
            var columnname = Request.Form["columnname"];
            var isshow = Request.Form["isshow"];
            var fielddesc = Request.Form["fielddesc"];
            var parentid = Request.Form["parentid"];
            var recordorder = 1;
            var updatetime = DateTime.Now;

            int roleidInt;
            int parentIdInt = 0;
            int isShowInt = 1;

            if(!Int32.TryParse(roleid,out roleidInt))
            {
                return Json(new FieldControlResponse(){Success=false,Message="角色编号必须为数字类型，请重试！"},JsonRequestBehavior.AllowGet);
            }

            Int32.TryParse(parentid, out parentIdInt);
            Int32.TryParse(isshow, out isShowInt);

            #region 新增
            if (editaction.Equals("add"))
            {

                var entity = new edu_field_control();
                entity.columnname = columnname.Trim();
                entity.databasename = databasename.Trim();
                entity.fielddesc = fielddesc.Trim();
                entity.isshow = isShowInt;
                entity.parentid = parentIdInt;
                entity.recordorder = recordorder;
                entity.roleid = roleidInt;
                entity.updatetime = updatetime;
                var list = new List<edu_field_control>();
                list.Add(entity);

                if (fControlCRUDService.AddInBatch(list))
                {
                    return Json(new FieldControlResponse() { Success = true, Message = "添加节点成功！" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new FieldControlResponse() { Success = false, Message = "添加节点失败，请重试！" }, JsonRequestBehavior.AllowGet);
                }
            }
            #endregion
            #region 更新
            if (editaction.Equals("update"))
            {
                int idInt;
                var id = Request.Form["id"];
                if (!Int32.TryParse(id, out idInt))
                {
                    return Json(new FieldControlResponse() { Success = false, Message = "主键编号不存在，请重试！" }, JsonRequestBehavior.AllowGet);
                }

                var request = new FieldControlRequest();
                request.currentIndex = 0;
                request.pageCount = Int32.MaxValue;
                request.where = x => x.id == idInt;
                request.orderBy = x => x.updatetime;

                var result = baseDataService.GetFieldControlResponse(request);
                if (result == null)
                {
                    return Json(new FieldControlResponse() { Success = false, Message = "待更新节点不存在，请重试！" }, JsonRequestBehavior.AllowGet);
                }
                if (result.Count == 0)
                {
                    return Json(new FieldControlResponse() { Success = false, Message = "待更新节点不存在，请重试！" }, JsonRequestBehavior.AllowGet);
                }

                var entity = result.FirstOrDefault();
                var entityOriginal = new edu_field_control();
                entityOriginal.id = entity.id;
                entityOriginal.columnname = columnname.Trim();
                entityOriginal.databasename = databasename.Trim();
                entityOriginal.fielddesc = fielddesc.Trim();
                entityOriginal.isshow = isShowInt;
                entityOriginal.parentid = parentIdInt;
                entityOriginal.recordorder = recordorder;
                entityOriginal.roleid = roleidInt;
                entityOriginal.updatetime = updatetime;
                 var list = new List<edu_field_control>();
                 list.Add(entityOriginal);

                 if (fControlCRUDService.UpdateInBatch(list))
                 {
                     return Json(new FieldControlResponse() { Success = true, Message = "更新节点成功！" }, JsonRequestBehavior.AllowGet);
                 }
                 else
                 {
                     return Json(new FieldControlResponse() { Success = false, Message = "更新节点失败，请重试！" }, JsonRequestBehavior.AllowGet);
                 }
            }
            #endregion
            #region 删除
            if (editaction.Equals("remove"))
            {
                int idInt;
                var id = Request.Form["id"];
                if (!Int32.TryParse(id, out idInt))
                {
                    return Json(new FieldControlResponse() { Success = false, Message = "主键编号不存在，请重试！" }, JsonRequestBehavior.AllowGet);
                }

                var request = new FieldControlRequest();
                request.currentIndex = 0;
                request.pageCount = Int32.MaxValue;
                request.where = x => x.id == idInt;
                request.orderBy = x => x.updatetime;

                var result = baseDataService.GetFieldControlResponse(request);
                if (result == null)
                {
                    return Json(new FieldControlResponse() { Success = false, Message = "待更新节点不存在，请重试！" }, JsonRequestBehavior.AllowGet);
                }
                if (result.Count == 0)
                {
                    return Json(new FieldControlResponse() { Success = false, Message = "待更新节点不存在，请重试！" }, JsonRequestBehavior.AllowGet);
                }

                var entity = result.FirstOrDefault();
                var entityOriginal = new edu_field_control();
                entityOriginal.id = entity.id;
                entityOriginal.columnname = entity.columnname;
                entityOriginal.databasename = entity.databasename;
                entityOriginal.fielddesc = entity.fielddesc.Trim();
                entityOriginal.isshow = entity.isshow;
                entityOriginal.parentid = entity.parentid;
                entityOriginal.recordorder = entity.recordorder;
                entityOriginal.roleid = entity.roleid;
                entityOriginal.updatetime = entity.updatetime;
                var list = new List<edu_field_control>();
                list.Add(entityOriginal);

                if (fControlCRUDService.DeleteInBatch(list))
                {
                    return Json(new FieldControlResponse() { Success = true, Message = "删除节点成功！" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new FieldControlResponse() { Success = false, Message = "删除节点失败，请重试！" }, JsonRequestBehavior.AllowGet);
                }
            }
            #endregion
            return null;
        }

        [HttpPost]
        public JsonResult GetTopField()
        {
            var request = new FieldControlRequest();
            request.pageCount = Int32.MaxValue;
            request.currentIndex = 0;
            request.orderBy = x => x.updatetime;
            request.where = x => x.id != 0 && (x.parentid == 0 || x.parentid == null);

            var modelList = baseDataService.GetFieldControlResponse(request);

            modelList.Insert(0, new FieldControlResponse() { parentid = 0, databasename = "顶级节点", id = 0 });

            return Json(modelList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetFieldPermissionBy(int roleid, string databasename)
        {
            var request = new FieldControlRequest();
            request.pageCount = Int32.MaxValue;
            request.currentIndex = 0;
            request.orderBy = x => x.updatetime;
            request.where = x => x.id != 0 && x.parentid != 0 && x.parentid != null && x.roleid == roleid && x.databasename == databasename;

            var pList = baseDataService.GetFieldControlResponse(request);

            if (pList != null)
            {
                return Json(new { success = true, data = pList },JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, message = "获取相关数据库字段权限失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}
