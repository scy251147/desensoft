﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FuNong.Service;
using FuNong.Framework.Cookie;
using FuNong.Framework.Logger;
using FuNong.Framework.PoolCenter;
using FuNong.DataAccess;
using FuNong.DataContract;

namespace FuNong.UserInterface.Controllers
{
    public class BaseDataController : BaseController
    {
        public BaseDataController(
               IBaseService<edu_area_village>   villageCRUDService
             , IBaseService<edu_base_setting>   baseCRUDService
             , IBaseService<edu_school>         schoolCRUDService
             , IBaseService<edu_school_grade>   gradeCRUDService
             , IBaseService<edu_school_class>   classCRUDService
             , IBaseService<edu_student_class_apply> applyCRUDService
             , IBaseService<edu_user_detail> userCRUDService
             , IBaseService<edu_card_change> cardChangeCRUDService
             , IBaseService<edu_video_all> videoCRUDService
             , IRepository<edu_user_detail> userRepo
             , IBaseDataService                 baseDataService
             , ISchoolService                   schoolService
             , ISoilPublishService              soilService
             , ICookie                          cookie
             , ILoggerService                   logger
             , IEventPool                       eventPool
            )
            : base(cookie, logger, eventPool, true)
        {

            this.villageCRUDService = villageCRUDService;
            this.baseCRUDService = baseCRUDService;
            this.schoolCRUDService = schoolCRUDService;
            this.gradeCRUDService = gradeCRUDService;
            this.classCRUDService = classCRUDService;
            this.applyCRUDService = applyCRUDService;
            this.cardChangeCRUDService = cardChangeCRUDService;
            this.userCRUDService = userCRUDService;
            this.videoCRUDService = videoCRUDService;

            this.userRepo = userRepo;

            this.baseDataService = baseDataService;
            this.schoolService = schoolService;
            this.soilService = soilService;

            this.cookie = cookie;
            this.logger = logger;
            this.eventPool = eventPool;
        }

        private readonly IBaseService<edu_area_village> villageCRUDService;
        private readonly IBaseService<edu_base_setting> baseCRUDService;
        private readonly IBaseService<edu_school> schoolCRUDService;
        private readonly IBaseService<edu_school_grade> gradeCRUDService;
        private readonly IBaseService<edu_school_class> classCRUDService;
        private readonly IBaseService<edu_student_class_apply> applyCRUDService;
        private readonly IBaseService<edu_card_change> cardChangeCRUDService;
        private readonly IBaseService<edu_user_detail> userCRUDService;
        private readonly IBaseService<edu_video_all> videoCRUDService;

        private readonly IRepository<edu_user_detail> userRepo;
             
        private readonly IBaseDataService    baseDataService;
        private readonly ISchoolService      schoolService;
        private readonly ISoilPublishService soilService;

        private readonly ICookie        cookie;
        private readonly ILoggerService logger;
        private readonly IEventPool     eventPool;

        #region 基础数据
        public ActionResult SysSetting()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetSysSettingByPage()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var request = new BaseSettingRequest();
            request.pageCount = rows;
            request.currentIndex = page;
            request.orderBy = x => x.updatetime;
            request.where = x => x.id != 0;
            request.propertyName = propertyName;
            request.propertyValue = propertyValue;

            var roleList = baseDataService.GetSettingList(request);

            int totalCount = 0;
            if (roleList != null && roleList.Count > 0)
            {
                totalCount = roleList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = roleList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult CommitSysSettings()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<edu_base_setting>();
            return operationbase.Commit(baseCRUDService, inserted, updated, deleted, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (baseCRUDService.AddInBatch(list))
                    return Json(new { success = "基建信息新增成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "基建信息新增失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (baseCRUDService.UpdateInBatch(list))
                    return Json(new { success = "基建信息更新成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "基建信息更新失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                if (baseCRUDService.DeleteInBatch(list))
                    return Json(new { success = "基建信息移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "基建信息移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }

        #endregion

        #region 日志操作
        public ActionResult SystemLog()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetSysLogByPage()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            string typeid = Request.Params["typeid"];

             var collection = cookie.GetCookieCollection("FuNong.UserInfo.Login");
             var uid = collection["FuNong.Login.UserID"];

            var request = new LogRequest();
            request.pageCount = rows;
            request.currentIndex = page;
            request.orderBy = x => x.Date;
            request.where = x => x.id != 0 && x.UserID == uid && x.SysTypeID == typeid;
            request.propertyName = propertyName;
            request.propertyValue = propertyValue;

            var roleList = baseDataService.GetLogList(request);

            int totalCount = 0;
            if (roleList != null && roleList.Count > 0)
            {
                totalCount = roleList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = roleList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }
        #endregion

        #region 乡镇管理
        public ActionResult VillageSetting()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetVillageByPage()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var request = new VillageRequest();
            request.pageCount = rows;
            request.currentIndex = page;
            request.orderBy = x => x.updatetime;
            request.where = x => x.id != 0;
            request.propertyName = propertyName;
            request.propertyValue = propertyValue;

            var roleList = baseDataService.GetVillageList(request);

            int totalCount = 0;
            if (roleList != null && roleList.Count > 0)
            {
                totalCount = roleList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = roleList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult CommitVillage()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<edu_area_village>();
            return operationbase.Commit(villageCRUDService, inserted, updated, deleted, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (villageCRUDService.AddInBatch(list))
                    return Json(new { success = "乡镇信息新增成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "乡镇信息新增失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (villageCRUDService.UpdateInBatch(list))
                    return Json(new { success = "乡镇信息更新成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "乡镇信息更新失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                if (villageCRUDService.DeleteInBatch(list))
                    return Json(new { success = "乡镇信息移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "乡镇信息移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }

        #endregion

        #region 获取基建数据信息
        
        [HttpPost]
        public JsonResult GetBaseSettingBy(string groupname)
        {
            var result = GetBaseSettingByGroupName(groupname);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private List<BaseSettingResponse> GetBaseSettingByGroupName(string groupname)
        {
            var request = new BaseSettingRequest();
            request.pageCount = Int32.MaxValue;
            request.currentIndex = 0;
            request.orderBy = x => x.updatetime;
            request.where = x => x.settinggroup == groupname;

            var result = baseDataService.GetSettingList(request);

            return result;
        }

        [HttpPost]
        public JsonResult GetSchoolBy()
        {

            var collection = cookie.GetCookieCollection("FuNong.UserInfo.Login");
            if (collection == null)
                new RedirectResult("Home/Login");

            var uid = collection["FuNong.Login.UserID"];
            int uidCon = 0;
            if (!Int32.TryParse(uid, out uidCon))
            {
                return Json("用户编号不是整数型，无法转换！", JsonRequestBehavior.AllowGet);
            }

            var request = new SchoolRequest();
            request.pageCount = Int32.MaxValue;
            request.currentIndex = 0;
            request.orderBy = x => x.updatetime;
            request.where = x => x.id != 0 && x.userid == uidCon;

            var schoolList = schoolService.GetSchoolResponse(request);

            return Json(schoolList,JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSchoolByFlag()
        {

            var collection = cookie.GetCookieCollection("FuNong.UserInfo.Login");
            if (collection == null)
                new RedirectResult("Home/Login");

            var uid = collection["FuNong.Login.UserID"];
            int uidCon = 0;
            if (!Int32.TryParse(uid, out uidCon))
            {
                return Json(new { success = false, message = "用户编号不是整数型，无法转换！" }, JsonRequestBehavior.AllowGet);
            }

            var request = new SchoolRequest();
            request.pageCount = Int32.MaxValue;
            request.currentIndex = 0;
            request.orderBy = x => x.updatetime;
            request.where = x => x.id != 0 && x.userid == uidCon;

            var schoolList = schoolService.GetSchoolResponse(request);

            return Json(new { success = true, data = schoolList }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSchoolByDistrictId(int? districtId)
        {
            var collection = cookie.GetCookieCollection("FuNong.UserInfo.Login");
            if (collection == null)
                new RedirectResult("Home/Login");

            var uid = collection["FuNong.Login.UserID"];
            int uidCon = 0;
            if (!Int32.TryParse(uid, out uidCon))
            {
                return Json(new { success = false, message = "用户编号不是整数型，无法转换！" }, JsonRequestBehavior.AllowGet);
            }

            var request = new SchoolRequest();
            request.pageCount = Int32.MaxValue;
            request.currentIndex = 0;
            request.orderBy = x => x.updatetime;
            if (districtId != null)
                request.where = x => x.districtid == districtId && x.userid == uidCon;
            else
                request.where = x => x.id != 0 && x.userid == uidCon;

            var schoolList = schoolService.GetSchoolResponse(request);

            return Json(new { success = true, data = schoolList }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region 学校管理

        public ActionResult School()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetSchoolByPage()
        {
            
            var collection = cookie.GetCookieCollection("FuNong.UserInfo.Login");
            if(collection==null)
                new RedirectResult("Home/Login");

            var uid = collection["FuNong.Login.UserID"];
            int uidCon = 0;
            if(!Int32.TryParse(uid, out uidCon))
            {
                throw new Exception("用户编号不是整数型，无法转换！");
            }

            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var request = new SchoolRequest();
            request.pageCount = rows;
            request.currentIndex = page;
            request.orderBy = x => x.updatetime;
            request.where = x => x.id != 0 && x.userid == uidCon;
            request.propertyName = propertyName;
            request.propertyValue = propertyValue;

            var roleList = schoolService.GetSchoolResponse(request);

            int totalCount = 0;
            if (roleList != null && roleList.Count > 0)
            {
                totalCount = roleList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = roleList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult GetSchoolByDistrict(int districtid)
        {
            var request = new SchoolRequest();
            request.pageCount = Int32.MaxValue;
            request.currentIndex = 0;
            request.orderBy = x => x.updatetime;
            request.where = x => x.districtid == districtid;

            var schoolList = schoolService.GetSchoolResponse(request);

            return Json(new { success = true, data = schoolList }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CommitSchool()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var collection = cookie.GetCookieCollection("FuNong.UserInfo.Login");

            var operationbase = new OperationBase<edu_school>();
            return operationbase.Commit(schoolCRUDService, inserted, updated, deleted, (list) =>
            {
                foreach (var model in list)
                {
                    if (collection != null)
                    {
                        var uid = collection["FuNong.Login.UserID"];
                        model.userid = Int32.Parse(uid);
                        model.updatetime = DateTime.Now;
                    }
                    else
                    {
                        new RedirectResult("Home/Login");
                    }
                }

                if (schoolCRUDService.AddInBatch(list))
                    return Json(new { success = "恭喜！您的公司设立成功！" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "抱歉！您的公司设立失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                {
                    if (collection != null)
                    {
                        var uid = collection["FuNong.Login.UserID"];

                        model.userid = Int32.Parse(uid);
                        model.updatetime = DateTime.Now;
                    }
                    else
                    {
                        new RedirectResult("Home/Login");
                    }
                }

                if (schoolCRUDService.UpdateInBatch(list))
                    return Json(new { success = "您的公司信息更新成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "您的公司信息更新失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                if (schoolCRUDService.DeleteInBatch(list))
                    return Json(new { success = "您的公司信息移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "您的公司信息移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }


        #endregion

        #region 年级管理
        public ActionResult Grade()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetGradeByPage()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var request = new GradeRequest();
            request.pageCount = rows;
            request.currentIndex = page;
            request.orderBy = x => x.updatetime;
            request.where = x => x.id != 0;
            request.propertyName = propertyName;
            request.propertyValue = propertyValue;

            var roleList = schoolService.GetGradeResponse(request);

            int totalCount = 0;
            if (roleList != null && roleList.Count > 0)
            {
                totalCount = roleList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = roleList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult CommitGrade()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            //获取当前登录用户的学校信息
            var schoolid = CurrentLoginSchoolID;
            if (schoolid == -1)
            {
                return Json(new { error = "抱歉！由于用户信息过期，您的班级开设失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }

            var operationbase = new OperationBase<edu_school_grade>();
            return operationbase.Commit(gradeCRUDService, inserted, updated, deleted, (list) =>
            {
                foreach (var model in list)
                {
                    model.schoolid = schoolid;
                    model.updatetime = DateTime.Now;
                }
                if (gradeCRUDService.AddInBatch(list))
                    return Json(new { success = "恭喜！您的年级开设成功！" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "抱歉！您的年级开设失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (gradeCRUDService.UpdateInBatch(list))
                    return Json(new { success = "年级信息更新成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "年级信息更新失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                if (gradeCRUDService.DeleteInBatch(list))
                    return Json(new { success = "年级信息移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "年级信息移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }
     
        #endregion

        #region 班级管理

        public ActionResult Class()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetClassByPage()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var request = new ClassRequest();
            request.pageCount = rows;
            request.currentIndex = page;
            request.orderBy = x => x.updatetime;
            request.where = x => x.id != 0;
            request.propertyName = propertyName;
            request.propertyValue = propertyValue;

            var roleList = schoolService.GetClassResponse(request);

            int totalCount = 0;
            if (roleList != null && roleList.Count > 0)
            {
                totalCount = roleList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = roleList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult CommitClass()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<edu_school_class>();
            return operationbase.Commit(classCRUDService, inserted, updated, deleted, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (classCRUDService.AddInBatch(list))
                    return Json(new { success = "恭喜！您的班级开设成功！" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "抱歉！您的班级开设失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (classCRUDService.UpdateInBatch(list))
                    return Json(new { success = "班级信息更新成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "班级信息更新失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                if (classCRUDService.DeleteInBatch(list))
                    return Json(new { success = "班级信息移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "班级信息移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }

        [HttpPost]
        public JsonResult GetAllGradesBy(int schoolid)
        {
            var result = GetGrades(schoolid);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetWrapGradesBy(int schoolid)
        {
            var result = GetGrades(schoolid);
            return Json(new { success = true, data = result }, JsonRequestBehavior.AllowGet);
        }

        private List<GradeResponse> GetGrades(int schoolid)
        {
            var request = new GradeRequest();
            request.currentIndex = 0;
            request.pageCount = Int32.MaxValue;
            request.where = x => x.schoolid == schoolid;
            request.orderBy = x => x.updatetime;

            var result = schoolService.GetGradeResponse(request);
            return result;
        }

        [HttpPost]
        public JsonResult GetClassBy(int gradeid)
        {
            var request = new ClassRequest();
            request.currentIndex = 0;
            request.pageCount = Int32.MaxValue;
            request.where = x => x.gradeid == gradeid;
            request.orderBy = x => x.updatetime;

            var result = schoolService.GetClassResponse(request);

            return Json(new { success = true, data = result }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region 课程表管理
        public ActionResult CourseTable()
        {
            var result = GetCourseTableFillData();
            return View(result);
        }

        public ActionResult UserSelectionDialog()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetCourseTableAjax()
        {
            var result = GetCourseTableFillData();

            if (result == null)
                return Json(new { success = false, message = "无法获取数据内容！" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = true, gradedata = result.gradelist, classdata = result.classlist, weekdata = result.weeklist, userdata = result.userlist, perioddata = result.periodlist, coursedata = result.courselist }, JsonRequestBehavior.AllowGet);
        }

        //设置课程表
        [HttpPost]
        public JsonResult CommitCourseTable()
        {
            return null;
        }

        private CourseTableResponse GetCourseTableFillData()
        {
            var request = new CourseTableRequest();
            request.schoolid = CurrentLoginSchoolID;
            request.roleid = 2;  //只获取教师
            request.pageCount = 1;
            request.currentIndex = 0;
            request.where = x => x.id != 0;
            request.orderBy = x => x.updatetime;

            var result = schoolService.GetCourseTableFillData(request);

            return result;
        }

        //根据班级id获取班主任信息
        [HttpPost]
        public JsonResult GetMasterBy(int classid)
        {
            var request = new CourseTableRequest();
            request.schoolid = CurrentLoginSchoolID;
            request.classid = classid;
            request.pageCount = 1;
            request.currentIndex = 0;
            request.where = x => x.id != 0 ;
            request.orderBy = x => x.updatetime;

            var result = schoolService.GetMasterByClassid(request);

            if (result == null)
            {
                return Json(new { success = false, message = "暂未设置班主任" }, JsonRequestBehavior.AllowGet);
            }
            else if (result.masterid <= 0)
            {
                return Json(new { success = false, message = "暂未设置班主任" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, data = result }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region 学生调班管理
        
        public ActionResult StudentClassApply()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetStudentClassApplyByPage()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var request = new StudentClassApplyRequest();
            request.pageCount = rows;
            request.currentIndex = page;
            request.orderBy = x => x.updatetime;
            request.where = x => x.id != 0;
            request.schoolid = CurrentLoginSchoolID;
            request.propertyName = propertyName;
            request.propertyValue = propertyValue;

            var roleList = schoolService.GetStudentClassApplyResponse(request);

            int totalCount = 0;
            if (roleList != null && roleList.Count > 0)
            {
                totalCount = roleList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = roleList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult CommitStudentClassApply()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<edu_student_class_apply>();
            return operationbase.Commit(applyCRUDService, inserted, updated, deleted, (list) =>
            {
                var userUpdateList = new List<edu_user_detail>();
                foreach (var model in list)
                {
                    model.updatetime = DateTime.Now;

                    //更新数据库edu_user_detail表中的classid字段
                    var userentity = userRepo.Get(x => x.uid == model.uid);
                    if (userentity != null && userentity.uid > 0)
                    {
                        //userentity.classid = model.classid;
                        userentity.updatetime = DateTime.Now;
                        userUpdateList.Add(userentity);
                    }
                }

                if (applyCRUDService.AddInBatch(list) && userCRUDService.UpdateInBatch(userUpdateList))
                {
                    return Json(new { success = "当前学生调班成功！" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { error = "当前学生调班失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                var userUpdateList = new List<edu_user_detail>();
                foreach (var model in list)
                {
                    model.updatetime = DateTime.Now;

                    //更新数据库edu_user_detail表中的classid字段
                    var userentity = userRepo.Get(x => x.uid == model.uid);
                    if (userentity != null && userentity.uid > 0)
                    {
                        //userentity.classid = model.classid;
                        userentity.updatetime = DateTime.Now;
                        userUpdateList.Add(userentity);
                    }
                }
                if (applyCRUDService.UpdateInBatch(list) && userCRUDService.UpdateInBatch(userUpdateList))
                {
                    return Json(new { success = "更新当前学生调班信息成功!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { error = "更新当前学生调班信息失败，请重试！" }, JsonRequestBehavior.AllowGet);
                }
            }, (list) =>
            {
                if (applyCRUDService.DeleteInBatch(list))
                    return Json(new { success = "学生调班信息移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "学生调班信息移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }

        #endregion

        #region 卡变更管理

        public ActionResult CardChange()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetCardChangeByPage()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var request = new CardChangeRequest();
            request.schoolid = CurrentLoginSchoolID;
            request.pageCount = rows;
            request.currentIndex = page;
            request.orderBy = x => x.updatetime;
            request.where = x => x.id != 0;
            request.propertyName = propertyName;
            request.propertyValue = propertyValue;

            var roleList = schoolService.GetCardChangeResponse(request);

            int totalCount = 0;
            if (roleList != null && roleList.Count > 0)
            {
                totalCount = roleList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = roleList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult CommitCardChange()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<edu_card_change>();
            return operationbase.Commit(cardChangeCRUDService, inserted, updated, deleted, (list) =>
            {
                var entity = list.FirstOrDefault();
                entity.updatetime = DateTime.Now;

                var userentity = userRepo.Get(x => x.checkincard == entity.oldcardnum);
                if (userentity == null)
                {
                    return Json(new { success = "无法找到旧卡卡号,变更失败！" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    userentity.checkincard = entity.newcardnum;
                    userentity.updatetime = DateTime.Now;
                }

                var userlist = new List<edu_user_detail>();
                userlist.Add(userentity);

                if (cardChangeCRUDService.AddInBatch(list) && userCRUDService.UpdateInBatch(userlist))
                {
                    return Json(new { success = "恭喜！卡记录变更成功！" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { error = "抱歉！卡记录变更失败，请重试！" }, JsonRequestBehavior.AllowGet);
                }
            }, (list) =>
            {
                var entity = list.FirstOrDefault();
                entity.updatetime = DateTime.Now;

                var userentity = userRepo.Get(x => x.checkincard == entity.oldcardnum);
                if (userentity == null)
                {
                    return Json(new { success = "无法找到旧卡卡号,变更记录更新失败！" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    userentity.checkincard = entity.newcardnum;
                    userentity.updatetime = DateTime.Now;
                }

                var userlist = new List<edu_user_detail>();
                userlist.Add(userentity);

                if (cardChangeCRUDService.UpdateInBatch(list) && userCRUDService.UpdateInBatch(userlist))
                {
                    return Json(new { success = "恭喜！卡变更记录更新成功！!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { error = "卡变更记录更新失败，请重试！" }, JsonRequestBehavior.AllowGet);
                }
            }, (list) =>
            {
                if (cardChangeCRUDService.DeleteInBatch(list))
                    return Json(new { success = "卡变更记录删除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "卡变更记录删除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }

        #endregion

        #region 视频监控配置管理

        public ActionResult VideoSetting()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetVideoSetting()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var collection = cookie.GetCookieCollection("FuNong.UserInfo.Login");
            var companyList = new List<int>();
            if(collection!=null)
            {
                var companyC = collection["FuNong.Login.CompanyList"];
                if (companyC.Contains(','))
                {
                    var companyArray = collection["FuNong.Login.CompanyList"].Split(',').ToList();
                    companyArray.ForEach(x => companyList.Add(Int32.Parse(x)));
                }
                else
                {
                    companyList.Add(Int32.Parse(companyC));
                }
            }
            

            var request = new VideoRequest();
            request.pageCount = rows;
            request.currentIndex = page;
            request.orderBy = x => x.updatetime;
            request.where = x => companyList.Contains(x.schoolid);
            request.propertyName = propertyName;
            request.propertyValue = propertyValue;

            var roleList = schoolService.GetVideoResponse(request);

            int totalCount = 0;
            if (roleList != null && roleList.Count > 0)
            {
                totalCount = roleList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = roleList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        public JsonResult CommitVideo()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<edu_video_all>();
            return operationbase.Commit(videoCRUDService, inserted, updated, deleted, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (videoCRUDService.AddInBatch(list))
                    return Json(new { success = "恭喜！视频监控配置信息成功！" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "抱歉！视频监控配置信息失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (videoCRUDService.UpdateInBatch(list))
                    return Json(new { success = "视频监控配置信息更新成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "视频监控配置信息更新失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                if (videoCRUDService.DeleteInBatch(list))
                    return Json(new { success = "视频监控配置信息移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "视频监控配置信息移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }

        #endregion
    }
}
