﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FuNong.DataContract;

namespace FuNong.UserInterface.Models
{
    public class TradingMarket
    {
        public SoilPublishMainResponse sr1 { get; set; }
        public SoilPublishMainResponse sr2 { get; set; }
        public SoilPublishMainResponse sr3 { get; set; }
        public SoilPublishMainResponse sr4 { get; set; }
    }
}