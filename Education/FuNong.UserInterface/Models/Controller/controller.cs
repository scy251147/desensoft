﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuNong.UserInterface.Models
{
    public class controller
    {
        //设备编号
        public string machineid { get; set; }
        //设备名称
        public string title { get; set; }
        //设备可控部分
        public IList<router> routers { get; set; }
        //设备指示灯部分
        public IList<led> leds { get; set; }
    }
}