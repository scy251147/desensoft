﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Web;

namespace FuNong.UserInterface.Models
{
    public class ClientConnection
    {
        public  TcpClient ClientInstance { get; set; }

        public  string ClientSession { get; set; }

        public  string MachineID { get; set; }
    }
}