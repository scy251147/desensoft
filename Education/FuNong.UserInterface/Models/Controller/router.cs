﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuNong.UserInterface.Models
{
    public class router
    {
        public int routeid { get; set; }
        public string title { get; set; }
        public IList<functions> functionsList { get; set; }
    }
}