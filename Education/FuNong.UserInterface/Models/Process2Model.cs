﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuNong.UserInterface.Models
{
    public class Process2Model
    {
        //业务流程流水号
        public int requestid { get; set; }

        //身份证正反面复印件
        public string indeximg { get; set; }

        //土地权证复印件
        public string indeximg1 { get; set; }

        public bool success { get; set; }
        public string message { get; set; }
    }
}