﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuNong.UserInterface.Models
{
    public class Process8DemoEntity
    {
        public int id { get; set; }
        public string requestname { get; set; }
        public string requestphone { get; set; }
        public string clientname { get; set; }
        public string clientphone { get; set; }

        public string infomain { get; set; }
        public string changetype { get; set; }
        public DateTime? signtime { get; set; }
        public string chargefee { get; set; }
        public int isapprove { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }
    }
}