﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuNong.UserInterface.Models
{
    public class Process4Model
    {
        //publish_main
        public int id { get; set; }
        public string publishtitle { get; set; }
        public int soiltypeid { get; set; }
        public int changetypeid { get; set; }
        public int districtid { get; set; }
        public string locationdetail { get; set; }
        public int sparechangeyear { get; set; }
        public int canchangeyear { get; set; }
        public float soilarea { get; set; }
        public string description { get; set; }
        public string publisher { get; set; }
        public string publisherphone { get; set; }
        
        public string indeximg { get; set; }

        public string chargefee { get; set; }

        public int requestid { get; set; }

        //publish_detail
        public string soilground { get; set; }
        public string earthtype { get; set; }
        public string water { get; set; }
        public string around { get; set; }

        public string publisherqq { get; set; }
        public string publishermail { get; set; }

        public bool success { get; set; }
        public string message { get; set; }
    }
}