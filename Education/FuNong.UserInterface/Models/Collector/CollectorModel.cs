﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuNong.UserInterface.Models
{
    public class CollectorModel
    {
        public string machine_id { get; set; }
        public int route_id { get; set; }
        public string param_id { get; set; }
        public string data_value { get; set; }
        public DateTime data_time { get; set; }
        public string route_name { get; set; }
        public string machine_name { get; set; }
        public string settingname { get; set; }
        public string settingvalue4 { get; set; }
    }
}