﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuNong.UserInterface.Models
{
    public class CollectorChartFmt
    {
        public string Param_name { get; set; }
        public string Param_unit { get; set; }
        public string Param_data { get; set; }
        public string Param_time { get; set; }
    }
}