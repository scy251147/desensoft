﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuNong.UserInterface.Models
{
    public class CollectorGroup
    {
        public string machine_id { get; set; }

        public int route_id { get; set; }

        public string param_id { get; set; }

        public string settingname { get; set; }

        public string settingvalue4 { get; set; }
    }
}