﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FuNong.DataContract;

namespace FuNong.UserInterface.Models
{
    public class IndexModel
    {
        //父类型
        public SoilTypeResponse SoilTypeParent { get; set; }
        //子类型
        public List<SoilTypeResponse> SoilTypeChildren { get; set; }
      
        //热点新闻下面的新闻列表8条
        public List<NewsResponse> NewsToplist { get; set; }
   
        // 下面图片两张

        //广告投放
    }
}