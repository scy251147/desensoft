﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuNong.UserInterface.Models
{
    public class MenuParent
    {
        public int menuid { get; set; }
        public string icon { get; set; }
        public string menuname { get; set; }
        public List<Menu> menus { get; set; }
        public int? recordorder { get; set; }
    }
}