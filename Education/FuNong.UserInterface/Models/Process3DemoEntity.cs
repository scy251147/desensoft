﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuNong.UserInterface.Models
{
    public class Process3DemoEntity
    {
        public int id { get; set; }
        public string clientidcard { get; set; }
        public string clientname { get; set; }
        public string soilcertificateimages { get; set; }
        public string serveridcard { get; set; }
        public string servername { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }
        public int isapprove { get; set; }
    }
}