﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;


namespace FuNong.TestModel
{
    class Program
    {
        static void Main(string[] args)
        {
            IFuNongContext context = new FuNongContext();
            IUnitOfWork uow = new UnitOfWork(context);

           // var service = new SoilPublishService(uow,new MemoryCacheManager(),new LoggerService());

            Console.ReadKey();
        }
    }
}
