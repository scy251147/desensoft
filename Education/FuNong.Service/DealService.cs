﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using FuNong.Framework.Caching;
using FuNong.Framework.Logger;
using FuNong.DataContract;
using AutoMapper;

namespace FuNong.Service
{
    public class DealService : IDealService
    {
        public DealService(
              IUnitOfWork unitOfWork
            , ICacheManager cacheManager
            , ILoggerService logger)
        {
            this.unitOfWork = unitOfWork;
            this.cacheManager = cacheManager;
            this.logger = logger;

            this.soilMainRepo = unitOfWork.Repository<funong_publish_main>();
            this.soilDetailRepo = unitOfWork.Repository<funong_publish_detail>();
            this.provinceRepo = unitOfWork.Repository<edu_area_province>();
            this.cityRepo = unitOfWork.Repository<edu_area_city>();
            this.districtRepo = unitOfWork.Repository<edu_area_district>();
            this.soilTypeRepo = unitOfWork.Repository<funong_soil_type>();
            this.changeTypeRepo = unitOfWork.Repository<funong_change_type>();
            this.soilAroundRepo = unitOfWork.Repository<funong_soil_around>();
            this.intentionRepo = unitOfWork.Repository<funong_intention>();
            this.dealRepo = unitOfWork.Repository<funong_deal>();

            dealDomain = new Model.Deal(  soilMainRepo
                                        , soilDetailRepo
                                        , soilAroundRepo
                                        , provinceRepo
                                        , cityRepo
                                        , districtRepo
                                        , soilTypeRepo
                                        , changeTypeRepo
                                        , intentionRepo
                                        , dealRepo);
        }

        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheManager cacheManager;
        private readonly ILoggerService logger;

        private readonly IRepository<funong_publish_main> soilMainRepo;
        private readonly IRepository<funong_publish_detail> soilDetailRepo;
        private readonly IRepository<funong_soil_around> soilAroundRepo;
        private readonly IRepository<edu_area_province> provinceRepo;
        private readonly IRepository<edu_area_city> cityRepo;
        private readonly IRepository<edu_area_district> districtRepo;
        private readonly IRepository<funong_soil_type> soilTypeRepo;
        private readonly IRepository<funong_change_type> changeTypeRepo;
        private readonly IRepository<funong_intention> intentionRepo;
        private readonly IRepository<funong_deal> dealRepo;

        private readonly FuNong.Model.Deal dealDomain;

        public List<DealResponse> TriggerList(DealRequest reqeuest)
        {
            int totalcount;

            var queryableResults = dealDomain.GetDealList(reqeuest.pageCount, reqeuest.currentIndex, out totalcount, reqeuest.where, reqeuest.orderBy, reqeuest.propertyName, reqeuest.propertyValue);

            var resultList = queryableResults.ToList();

            var returnList = new List<DealResponse>();
            resultList.ForEach(item =>
            {
                var main = soilMainRepo.Get(x=>x.id==item.publishmainid);
                if(main==null) main = new funong_publish_main();

                var convertedItem = Mapper.DynamicMap<DealResponse>(item);
                convertedItem.totalcount = totalcount;
                convertedItem.publishtitle = main.publishtitle;

                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.recordorder).ToList();
            return returnList;
        }
    }
}
