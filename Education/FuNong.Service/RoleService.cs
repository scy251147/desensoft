﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using FuNong.DataContract;
using FuNong.Model;
using AutoMapper;
using FuNong.Framework.Caching;
using FuNong.Framework.Logger;
using System.Data.Entity.Validation;

namespace FuNong.Service
{
    public class RoleService:IRoleService
    {
        public RoleService(
              IUnitOfWork unitOfWork
            , ICacheManager cacheManager
            , ILoggerService logger)
        {
            this.unitOfWork = unitOfWork;
            this.cacheManager = cacheManager;
            this.logger = logger;
        }

        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheManager cacheManager;
        private readonly ILoggerService logger;

        public List<RoleResponse> TriggerList(RoleRequest roleRequest)
        {
            var role = new Role(unitOfWork.Repository<edu_role_detail>());
            int totalcount;

            var queryableResults = role.GetRoleDetails(roleRequest.pageCount, roleRequest.currentIndex, out totalcount, roleRequest.where, roleRequest.orderBy, roleRequest.propertyName, roleRequest.propertyValue);

            var resultList = queryableResults.ToList();

            var returnList = new List<RoleResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<RoleResponse>(item);
                convertedItem.totalcount = totalcount;
                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.recordorder).ToList();
            return returnList;
        }
    }
}
