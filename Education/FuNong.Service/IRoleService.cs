﻿using System;
using System.Collections.Generic;
using FuNong.DataAccess;
using FuNong.DataContract;
namespace FuNong.Service
{
    public interface IRoleService
    {
        List<RoleResponse> TriggerList(RoleRequest roleRequest);
    }
}
