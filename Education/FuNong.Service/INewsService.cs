﻿using System;
using FuNong.DataContract;
using System.Collections.Generic;
namespace FuNong.Service
{
    public interface INewsService
    {
        Dictionary<NewsTypeResponse, List<NewsResponse>> TriggerList(NewsTypeRequest newsTypeRequest);
        List<NewsTypeResponse> GetTypeResponseList(NewsTypeRequest newsTypeRequest);
        NewsResponse GetNewDetailById(NewsRequest request);
        List<NewsResponse> GetNewsBySoilTypeId(NewsRequest request);
        List<NewsResponse> GetNewsList(NewsRequest request);
        List<NewsSoilMapResponse> GetNewsSoilMap(NewsSoilMapRequest request);

        List<BaseSettingResponse> GetTypeResponseListEx(BaseSettingRequest bRequest);
    }
}
