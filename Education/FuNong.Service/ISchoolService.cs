﻿using FuNong.DataContract;
using System.Collections.Generic;
namespace FuNong.Service
{
    public interface ISchoolService
    {
        //学校信息列表
        List<SchoolResponse> GetSchoolResponse(SchoolRequest request);

        //年级信息列表
        List<GradeResponse> GetGradeResponse(GradeRequest request);

        //班级信息列表
        List<ClassResponse> GetClassResponse(ClassRequest request);

        //课程表信息
        CourseTableResponse GetCourseTableFillData(CourseTableRequest request);

        //学生调班信息列表
        List<StudentClassApplyResponse> GetStudentClassApplyResponse(StudentClassApplyRequest request);
        
        //卡变更记录信息列表
        List<CardChangeResponse> GetCardChangeResponse(CardChangeRequest request);

        //视频监控配置信息列表
        List<VideoResponse> GetVideoResponse(VideoRequest request);

        //通过班级ID获取班主任信息
        CourseTableResponse GetMasterByClassid(CourseTableRequest request);
    }
}
