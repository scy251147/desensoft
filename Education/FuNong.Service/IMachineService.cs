﻿using FuNong.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.Service
{
    public interface IMachineService
    {
        //获取设备列表
        List<MachineResponse> TriggerList(MachineRequest machineRequest);

        //获取控制设备配置列表
        List<ControllerResponse> TriggerList(ControllerRequest controllerRequest);

        //获取采集设备配置列表
        List<CollectorResponse> TriggerList(CollectorRequest collectorRequest);
    }
}
