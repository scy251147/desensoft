﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataContract;

namespace FuNong.Service
{
    public interface IModelService
    {
        List<ModelResponse> TriggerList(ModelRequest modelRequest);
        string ContructModelTree(List<ModelResponse> modelList, string modelParent);

        List<ModelResponse> GetModelUnApply(ModelRoleRequest modelrolerequest);
        List<ModelResponse> GetModelApply(ModelRoleRequest modelrolerequest);
        List<ModelRoleResponse> GetRoleModelByIdList(ModelRoleRequest modelrolerequest);
    }
}
