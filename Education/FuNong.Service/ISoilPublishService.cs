﻿using System;
using System.Collections.Generic;
using FuNong.DataContract;
namespace FuNong.Service
{
    public interface ISoilPublishService
    {
        List<SoilTypeResponse> GetSoilType(SoilAttributeRequest request);
        List<ChangeTypeResponse> GetChangeType(SoilAttributeRequest request);

        List<SoilAroundResponse> GetSoilAroundList(SoilAttributeRequest request);

        SoilPublishMainResponse GetPublishMainBy(SoilPublishMainRequest request);

        List<ProvinceResponse> GetProvince();
        List<CityResponse> GetCityByProvinceID(SoilAttributeRequest request);
        List<DistrictResponse> GetDistrictByCityID(SoilAttributeRequest request);
        
        List<SoilPublishMainResponse> TriggerList(SoilPublishMainRequest soilPMRequest);
        Dictionary<SoilTypeResponse, List<SoilPublishMainResponse>> TriggerSoilIndex();

        List<IntentionResponse> GetIntentionByMainID(IntentionRequest request);
    }
}
