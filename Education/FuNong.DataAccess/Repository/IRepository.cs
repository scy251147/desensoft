﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace FuNong.DataAccess
{
    public interface IRepository<T>
    {
        void Add(T entity);
        void Remove(T entity);
        void Remove(Expression<Func<T, bool>> where);
        void Update(T entity);

        T GetByKey(object id);
        T Get(Expression<Func<T, bool>> where);

        IQueryable<T> GetPagger<TKey>(int pageCount, int currentIndex, out int totalCount, Expression<Func<T, bool>> where, Expression<Func<T, TKey>> orderby, bool up);
        IQueryable<T> GetMany(Expression<Func<T, bool>> where);
        IQueryable SqlQuery(string sql, params object[] parameters);
    }
}
