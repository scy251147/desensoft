﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data;

namespace FuNong.DataAccess
{
    public class Repository<T>:IRepository<T> where T:class
    {
        public Repository(IFuNongContext context)
        {
            this._context = context;
        }

        private readonly IFuNongContext _context;

        public void Add(T entity)
        {
            try
            {
                if (entity == null) 
                    throw new ArgumentException("实体类为空");

                _context.DbSet<T>().Add(entity);
            }
            catch (DbEntityValidationException dbex)
            {
                var msg = string.Empty;
                foreach (var validationErrors in dbex.EntityValidationErrors)
                    foreach (var validateionError in validationErrors.ValidationErrors)
                        msg += string.Format("属性:{0} 错误:{1}", validateionError.PropertyName, validateionError.ErrorMessage);
                var fail = new Exception(msg, dbex);
                throw fail;
            }
        }

        public void Remove(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("实体类为空");

                _context.DbSet<T>().Attach(entity);
                _context.DbSet<T>().Remove(entity);
            }
            catch (DbEntityValidationException dbex)
            {
                var msg = string.Empty;
                foreach (var validationErrors in dbex.EntityValidationErrors)
                    foreach (var validateionError in validationErrors.ValidationErrors)
                        msg += string.Format("属性:{0} 错误:{1}", validateionError.PropertyName, validateionError.ErrorMessage);
                var fail = new Exception(msg, dbex);
                throw fail;
            }
        }

        public virtual void Remove(Expression<Func<T, bool>> where)
        {
            try
            {
                var entities = _context.DbSet<T>().Where(where);
                foreach (var entity in entities.ToList())
                {
                    _context.DbSet<T>().Attach(entity);
                    _context.DbSet<T>().Remove(entity);
                }
            }
            catch (DbEntityValidationException dbex)
            {
                var msg = string.Empty;
                foreach (var validationErrors in dbex.EntityValidationErrors)
                    foreach (var validateionError in validationErrors.ValidationErrors)
                        msg += string.Format("属性:{0} 错误:{1}", validateionError.PropertyName, validateionError.ErrorMessage);

                var fail = new Exception(msg, dbex);
                throw fail;
            }
        }

        public void Update(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("实体类为空");

                _context.Entry(entity).State = EntityState.Modified;
            }
            catch (DbEntityValidationException dbex)
            {
                var msg = string.Empty;
                foreach (var validationErrors in dbex.EntityValidationErrors)
                    foreach (var validateionError in validationErrors.ValidationErrors)
                        msg += string.Format("属性:{0} 错误:{1}", validateionError.PropertyName, validateionError.ErrorMessage);
                var fail = new Exception(msg, dbex);
                throw fail;
            }
        }

        public T Get(Expression<Func<T, bool>> where)
        {
            return GetMany(where).FirstOrDefault();
        }

        public IQueryable<T> GetMany(Expression<Func<T, bool>> where)
        {
            try
            {
                var dbEntity = _context.DbSet<T>();
                var returnResult = dbEntity.AsNoTracking().Where(where);
                return returnResult;
            }
            catch (DbEntityValidationException dbex)
            {
                var msg = string.Empty;
                foreach (var validationErrors in dbex.EntityValidationErrors)
                    foreach (var validateionError in validationErrors.ValidationErrors)
                        msg += string.Format("属性:{0} 错误:{1}", validateionError.PropertyName, validateionError.ErrorMessage);
                var fail = new Exception(msg, dbex);
                throw fail;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        //up：升序true；降序false
        public IQueryable<T> GetPagger<TKey>(int pageCount, int currentIndex, out int totalCount, Expression<Func<T, bool>> where, Expression<Func<T, TKey>> orderby, bool up)
        {
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            var resultQuery = GetMany(where);

            totalCount = resultQuery.Count();

            if (up)
                return resultQuery.Where(where)
                                        .OrderByDescending(orderby).Skip(skipRows)
                                        .Take(pageCount).AsQueryable();
            else
                return resultQuery.Where(where)
                                  .OrderBy(orderby).Skip(skipRows)
                                  .Take(pageCount).AsQueryable();
        }

        public T GetByKey(object id)
        {
            return _context.DbSet<T>().Find(id);
        }

        public virtual IQueryable SqlQuery(string sql, params object[] parameters)
        {
            return _context.SqlQuery<T>(sql, parameters).AsQueryable();
        }
    }
}
