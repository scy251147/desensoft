﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChatRoom.Models;
using System.Net.Sockets;

namespace ChatRoom.Hubs
{
    public static class ChatUserCache
    {
        public static IList<UserChat> userList = new List<UserChat>();

        public static IDictionary<string, TcpClient> clientList = new Dictionary<string, TcpClient>();
    }
}