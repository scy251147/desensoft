﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Web;

namespace FuNong.Framework.Cookie
{
    public class CookieWrapper:ICookie
    {
        public void AddCookie(string key, string value)
        {
            AddCookie(key, value, DateTime.Now.AddDays(7));
        }

        public void AddCookie(string key, string value, DateTime expireTime)
        {
            if (string.IsNullOrEmpty(key)) return;
            HttpCookie cookie = new HttpCookie(key);
            cookie.Value = value;
            cookie.Expires = expireTime;
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public void AddCookie(string key, NameValueCollection nvCollection)
        {
            AddCookie(key, nvCollection, DateTime.Now.AddDays(7));
        }

        public void AddCookie(string key, NameValueCollection nvCollection, DateTime expireTime)
        {
            if (string.IsNullOrEmpty(key)) return;
            if (nvCollection == null) return;
            HttpCookie cookie = new HttpCookie(key);
            cookie.Values.Add(nvCollection);
            cookie.Expires = DateTime.Now.AddDays(7);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public string GetCookieString(string key)
        {
            if (!string.IsNullOrEmpty(key))
                return HttpContext.Current.Request.Cookies[key].Value;
            return string.Empty;
        }

        public NameValueCollection GetCookieCollection(string key)
        {
            if (!string.IsNullOrEmpty(key))
            {
                var result = HttpContext.Current.Request.Cookies[key];
                if(result==null) return null;
                else return result.Values;
            }
            return null;
        }

        public void ClearCooke(string key)
        {
            if (string.IsNullOrEmpty(key)) return;
            HttpContext.Current.Response.Cookies[key].Expires = DateTime.Now.AddDays(-1);
        }
    }
}
