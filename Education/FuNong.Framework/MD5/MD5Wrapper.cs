﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace FuNong.Framework.MD5
{
    public class MD5Wrapper
    {
        public class Md5Wrapper
        {
            public static string GetMD5Result(string value)
            {
                if (string.IsNullOrEmpty(value))
                    return string.Empty;


                var md5 = new MD5CryptoServiceProvider();
                byte[] hashByte = md5.ComputeHash(Encoding.UTF8.GetBytes(value));
                StringBuilder sb = new StringBuilder();
                foreach (byte item in hashByte)
                    sb.Append(item.ToString("x").PadLeft(2, '0'));
                return sb.ToString();
            }

            public static string CreateUserPassword(string password, string salt)
            {
                return GetMD5Result(password + salt);
            }
        }
    }
}
