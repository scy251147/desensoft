﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.Framework.Logger
{
    public interface ILoggerService
    {
        void Debug(object message);
        void Error(object message);
        void Fatal(object message);
        void Info(object message);
        void Warn(object message);
    }
}
