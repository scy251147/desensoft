﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net.Layout;

namespace FuNong.Framework.Logger
{
    public class CustomLayout:PatternLayout
    {
        public CustomLayout()
        {
            this.AddConverter("property", typeof(XPatternConverter));
        }
    }
}
