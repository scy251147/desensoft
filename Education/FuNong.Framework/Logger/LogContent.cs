﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.Framework.Logger
{
    public class LogContent
    {
        public LogContent(string content, string u_id, string t_id)
        {
            this.content = content;
            this.u_id = u_id;
            this.t_id = t_id;
        }

        public string content { get; set; }
        public string u_id { get; set; }
        public string t_id { get; set; }
    }
}
