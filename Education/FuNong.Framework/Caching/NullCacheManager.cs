﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.Framework.Caching
{
    //NullCache的介入是为了防止发生Exception而导致项目崩溃的情况
    public class NullCacheManager:ICacheManager
    {
        public T Get<T>(string key)
        {
            return default(T);
        }

        public void Set(string key, object data, int cacheTime)
        {
            
        }

        public bool IsSet(string key)
        {
            return false;
        }

        public void Remove(string key)
        {
            
        }

        public void RemoveByPattern(string pattern)
        {
            
        }

        public void Clear()
        {
            
        }
    }
}
