﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace FuNong.Framework.PoolCenter
{
    /// <summary>
    /// A defensive event firing mechanism.
    /// Juval Lowy's and Eric Gunnerson's C# Best Practices
    /// download from: http://www.only4gurus.com/v2/preview.asp?ID=3183
    /// </summary>
    public class EventsHelper
    {
        /// <summary>
        /// Invoke all handlers attached to the multicast delegate, ensuring
        /// that the delegate exists, and iterating through the delegate collection
        /// so that event delegates with unassigned delegate handlers don't cause
        /// exceptions.
        /// </summary>
        /// <param name="del"></param>
        /// <param name="args"></param>
        public static void Fire(Delegate del, params object[] args)
        {
            if (del != null)
            {
                Delegate[] delegates = del.GetInvocationList();
                foreach (Delegate sink in delegates)
                {
                    try
                    {
                        sink.DynamicInvoke(args);
                    }
                    catch (Exception e)
                    {
                        Trace.WriteLine(e.Message);
                        throw (e);
                    }
                }
            }
        }
    } 
}
