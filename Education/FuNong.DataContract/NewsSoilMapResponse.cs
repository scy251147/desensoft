﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class NewsSoilMapResponse:Response
    {
        public int id { get; set; }

        public int soiltypeid { get; set; }
        public int newstypeid { get; set; }

        public string soiltypename { get; set; }
        public string newstypename { get; set; }

        public int? recordorder { get; set; }

        public DateTime? updatetime { get; set; }

        public int totalcount { get; set; }
    }
}
