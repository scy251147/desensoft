﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class FieldControlResponse : Response
    {
        public int id { get; set; }
        public int roleid { get; set; }
        public string databasename { get; set; }
        public string columnname { get; set; }
        public int? isshow { get; set; }
        public string fielddesc { get; set; }
        public int? parentid { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }

        public string rolename { get; set; }

        public int totalcount { get; set; }

        public int _parentId { get; set; }
    }
}
