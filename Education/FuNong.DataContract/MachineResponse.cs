﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class MachineResponse
    {
        public string machine_id { get; set; }

        public string machine_name { get; set; }

        public int school_id { get; set; }
        //公司名称
        public string company_name { get; set; }

        public string machine_note { get; set; }

        public Nullable<int> recordorder { get; set; }

        public Nullable<System.DateTime> updatetime { get; set; }

        public int totalcount { get; set; }
    }
}
