﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;

namespace FuNong.DataContract
{
    public class IntentionRequest:Request<funong_intention>
    {
        public int mainid { get; set; }
    }
}
