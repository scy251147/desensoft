﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class SoilTypeResponse
    {

        public int id { get; set; }
        public string typename { get; set; }
        public string typenote { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }
        public int parentid { get; set; }
    }
    public class ChangeTypeResponse
    {
        public int id { get; set; }
        public string changetitle { get; set; }
        public string changenote { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }
    }

    public class SoilAroundResponse
    {
        public int id { get; set; }
        public string aroundgroup { get; set; }
        public string aroundattribute { get; set; }
        public string aroundnote1 { get; set; }
        public string aroundnote2 { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }
    }

    public class CityResponse
    {
        public int id { get; set; }
        public int provinceid { get; set; }
        public string name { get; set; }
        public string areacode { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }
    }
    public class DistrictResponse
    {
        public int id { get; set; }
        public int cityid { get; set; }
        public string name { get; set; }
        public string postcode { get; set; }
        public string jin { get; set; }
        public string wei { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }
    }
    public class ProvinceResponse
    {
        public int id { get; set; }
        public string name { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }
    }
}