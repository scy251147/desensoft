﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class CollectorResponse
    {
        public int id { get; set; }

        public string machine_id { get; set; }

        public int machine_type { get; set; }
        //设备类别名称
        public string type_name { get; set; }

        public string machine_name { get; set; }

        public int company_id { get; set; }

        public string company_name { get; set; }

        public int route_id { get; set; }

        public string route_name { get; set; }

        public string param_id { get; set; }

        public string param_name { get; set; }

        public string param_min { get; set; }

        public string param_max { get; set; }

        public string collector_note { get; set; }

        public Nullable<int> recordorder { get; set; }

        public Nullable<System.DateTime> updatetime { get; set; }

        public int totalcount { get; set; }
    }
}
