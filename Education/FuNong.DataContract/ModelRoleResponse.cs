﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class ModelRoleResponse:Response
    {
        public int id { get; set; }
        public int roleid { get; set; }
        public int modelid { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }
    }
}
