﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class VillageResponse : Response
    {
        public int id { get; set; }      //乡镇
        public string name { get; set; }
        public string jin { get; set; }
        public string wei { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }
        public int totalcount { get; set; }

        public int districtid { get; set; }  //区县
        public int cityid { get; set; }      //市
        public int provinceid { get; set; }  //省

        public string districtname { get; set; }  
        public string cityname { get; set; }
        public string provincename { get; set; }

    }
}
