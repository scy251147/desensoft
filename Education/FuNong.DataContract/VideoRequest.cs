﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;

namespace FuNong.DataContract
{
    public class VideoRequest:Request<edu_video_all>
    {
        public int schoolid { get; set; }
    }
}
