﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;

namespace FuNong.DataContract
{
    public class SoilPublishMainRequest:Request<funong_publish_main>
    {
        public string soiltitle { get; set; }
        public string publisher { get; set; }
        public string publisherphone { get; set; }
    }
}
