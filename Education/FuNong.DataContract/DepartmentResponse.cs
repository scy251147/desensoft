﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class DepartmentResponse : Response
    {
        public int id { get; set; }
        public string departmentname { get; set; }
        public string departmentnote { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }
        public int totalcount { get;set;}
    }
}
