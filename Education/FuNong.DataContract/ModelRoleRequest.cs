﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;

namespace FuNong.DataContract
{
    public class ModelRoleRequest : Request<edu_role_model>
    {
        public int roleid { get; set; }
        public string modelidlist { get; set; }
    }
}
