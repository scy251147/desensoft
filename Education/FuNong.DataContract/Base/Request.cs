﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace FuNong.DataContract
{
    public class Request<T> where T : class
    {
      
        public int pageCount { get; set; }
        public int currentIndex { get; set; }
        public Expression<Func<T, bool>> where { get; set; }
        public Expression<Func<T, DateTime?>> orderBy { get; set; }
        public string propertyName { get; set; }
        public string propertyValue { get; set; }
    }
}
