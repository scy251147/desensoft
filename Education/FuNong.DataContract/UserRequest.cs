﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using FuNong.DataAccess;

namespace FuNong.DataContract
{
    public class UserRequest : Request<edu_users_view>
    {
        public int userid { get; set; }

        public string username { get; set; }
    }
}
