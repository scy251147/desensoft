﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class BaseSettingResponse : Response
    {
        public int id { get; set; }

        public string settinggroup { get; set; }

        public string settingname { get; set; }

        public string settingvalue1 { get; set; }

        public string settingvalue2 { get; set; }

        public string settingvalue3 { get; set; }

        public string settingvalue4 { get; set; }

        public string settingvalue5 { get; set; }

        public int? recordorder { get; set; }

        public DateTime? updatetime { get; set; }

        public int totalcount { get; set; }
    }
}
