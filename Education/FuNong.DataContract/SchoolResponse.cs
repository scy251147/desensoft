﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class SchoolResponse : Response
    {
        public int id { get; set; }        //主键
        public string name { get; set; }   //学校名称
        public string code { get; set; }   //学校编码
        public string note { get; set; }   //备注 
        public string createtime { get; set; } //本条记录创建时间
        public string jin { get; set; }  //经度
        public string wei { get; set; }  //纬度
        public int? recordorder { get; set; }   //排序
        public DateTime? updatetime { get; set; }   //更新时间

        public int districtid { get; set; }      //区县id 
        public string districtname { get; set; }   //区县名称
          
        public int cityid { get; set; }      //城市id
        public string cityname { get; set; }   //城市名称
         
        public int provinceid { get; set; }      //省id
        public string provincename { get; set; }   //省名称
         
        public int typeid { get; set; }     //学校类型id  schooltype
        public string typename { get; set; }   //类型字段

        public int stateid { get; set; }    //学校状态id  state
        public string statename { get; set; }  //状态字段

        public int userid { get; set; }

        public int totalcount { get; set; }
    }
}
