﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;

namespace FuNong.DataContract
{
    public class ModelRequest : Request<edu_model_detail>
    {
        public int modelid { get; set; }
    }
}
