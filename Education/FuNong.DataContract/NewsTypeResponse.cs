﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class NewsTypeResponse : Response
    {
        public int id { get; set; }
        public string typename { get; set; }
        public string typenote { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }
        public int? parentid { get; set; }

        public string typenamemeng { get; set; }
        public string parentname { get; set; }

        public int totalcount { get; set; }
    }
}
