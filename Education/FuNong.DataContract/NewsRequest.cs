﻿using FuNong.DataAccess;

namespace FuNong.DataContract
{
    public class NewsRequest : Request<edu_cms_news>
    {
        public int newsid { get; set; }

        //新闻类别id
        public int newstypeid { get; set; }

        //1.表明获取今日热点
        //2.表明获取今日推荐
        public int markflag { get; set; }

        //获取相关类别
        public int typeid { get; set; }

        //所属土地类型id
        public int soiltypeid { get; set; }
    }
}
