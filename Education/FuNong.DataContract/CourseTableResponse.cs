﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;

namespace FuNong.DataContract
{
    public class CourseTableResponse : Response
    {
        public int id { get; set; }              //主键
        public int classid { get; set; }         //班级列表
        public int weekid { get; set; }          //星期
        public int periodid { get; set; }        //上中下午
        public string periodtime { get; set; }   //小节时间段
        public int courseid { get; set; }        //课程id

        public int masterid { get; set; }        //班主任id
        public string mastername { get; set; }   //班主任名称
        public string mastercode { get; set; }   //班主任工号
        
        public int teacherid { get; set; }       //任课教师id

        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }

        public List<edu_school_grade> gradelist { get; set; }   //

        public List<edu_school_class> classlist { get; set; }

        public List<edu_users_view> userlist { get; set; }

        public List<edu_base_setting> courselist { get; set; }

        public List<edu_base_setting> weeklist { get; set; }

        public List<edu_base_setting> periodlist { get; set; }

    }
}
