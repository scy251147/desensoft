﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;

namespace FuNong.DataContract
{
    public class CardChangeRequest:Request<edu_card_change>
    {
        public int schoolid { get; set; }
    }
}
