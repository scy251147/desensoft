﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;

namespace FuNong.DataContract
{
    public class ProcessRequest:Request<funong_service_request>
    {
        public int userid { get; set; }

        public int requestid { get; set; }
    }
}
