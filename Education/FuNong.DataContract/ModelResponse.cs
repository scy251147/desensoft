﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class ModelResponse : Response
    {
        public ModelResponse()
        {
            parentid = 0;
        }

        public int id { get; set; }
        public string modelicon { get; set; }
        public string modelname { get; set; }
        public string modelpath { get; set; }
        public string modelnote { get; set; }
        public string modelaction { get; set; }
        public int? parentid { get; set; }
        public string parentname { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }

        public int totalcount { get; set; }

        public int _parentId { get; set; }
    }
}
