﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class RoleResponse : Response
    {
        public int id { get; set; }
        public string rolename { get; set; }
        
        public int? ismultischool { get; set; }  //是否允许多校
        public string refroleid { get; set; } //关联角色，比如当前是教师，假如关联角色为教师，学生的话，那么在人员选择界面上，就只出现所有教师和学生信息，其他角色人员一概不显示，存储方式为：1,2
        public int? refowner { get; set; } //是否管理自己，如果refroleid为教师和学生的话，那么人员选择界面将会出现所有的教师和学生信息，但是如果refowner为1的话，那么将会只显示教师和学生中，与自己相关的人员信息，比如 学生用户如果关联教师和学生的话，那么refowner为1的时候，在人员选择列表中，这些学生只能看到自己的班主任，任课教师，同班同学信息，其他的都看不到。

        public string rolenote { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }
        public int totalcount { get; set; }
    }
}
