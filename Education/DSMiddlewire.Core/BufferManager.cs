﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace DSMiddlewire.Core
{
    public class BufferManager
    {

        public BufferManager(int totalBytes,int bufferSize)
        {
            this.totalBytes = totalBytes;
            this.bufferSize = bufferSize;

            this.currentIndex = 0;
            bufferPool = new Stack<int>();
        }

        private int totalBytes;
        private int currentIndex;
        private int bufferSize;
        private byte[] buffer;
        private Stack<int> bufferPool;

        //Create a large buffer size with total bytes allocated.
        public void InitBuffer()
        {
            buffer = new byte[totalBytes];
        }

        public bool SetBuffer(SocketAsyncEventArgs args)
        {
            if (bufferPool.Count > 0)
            {
                args.SetBuffer(buffer, bufferPool.Pop(), bufferSize);
            }
            else
            {
                if ((totalBytes - bufferSize) < currentIndex)
                {
                    return false;
                }
                args.SetBuffer(buffer, currentIndex, bufferSize);
                currentIndex += bufferSize;
            }
            return true;
        }

        public void FreeBuffer(SocketAsyncEventArgs args)
        {
            bufferPool.Push(args.Offset);
            args.SetBuffer(null, 0, 0);
        }
    }
}
