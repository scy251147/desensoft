﻿using System; 
using System.Collections.Generic;
using System.Linq; 
using System.Text;
using System.Net; 
using System.Net.Sockets;

namespace DSMiddlewire.Core
{
    public class SocketClient
    {
        public SocketClient(IPAddress ipAddress, int port, int buffersize)
        {
            endPoint = new IPEndPoint(ipAddress, port);
            this.bufferSize = buffersize;

            this.acceptArgs = new SocketAsyncEventArgs();
            this.acceptArgs.Completed += acceptArgs_Completed;
        }

        private int bufferSize;
        private EndPoint endPoint;
        private Socket client;
        private SocketAsyncEventArgs acceptArgs;
        private SocketAsyncEventArgs receiveArgs;
        public Action<SocketAsyncEventArgs> OnReceive;

        public void StartListen()
        {
            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            client.Bind(endPoint);
            client.Listen(1);  //listen from server

            StartAccept(null);
        }

        private void StartAccept(SocketAsyncEventArgs e)
        {
            if (this.acceptArgs.AcceptSocket != null)
            {
                this.acceptArgs.AcceptSocket = null;
            }
            else
            {
                this.acceptArgs = new SocketAsyncEventArgs();

                acceptArgs.Completed += acceptArgs_Completed;
            }

            //No Matther clients connected the server in Async Mode or Sync mode,        
            //the "ProcessAccept" method will alway be invoked.  
            if (!client.AcceptAsync(acceptArgs))
            {
                ProcessAccept(acceptArgs);
            }
        }

        private void acceptArgs_Completed(object sender, SocketAsyncEventArgs e)
        {

            ProcessAccept(e);
        }

        private void ProcessAccept(SocketAsyncEventArgs e)
        {
            receiveArgs = new SocketAsyncEventArgs();
            receiveArgs.SetBuffer(new byte[bufferSize], 0, bufferSize);
            receiveArgs.AcceptSocket = e.AcceptSocket;
            receiveArgs.Completed += receiveArgs_Completed;

            if (!e.AcceptSocket.ReceiveAsync(receiveArgs))
            {
                ProcessReceive(receiveArgs);
            }
        }

        private void receiveArgs_Completed(object sender, SocketAsyncEventArgs e)
        {
            if (e.BytesTransferred > 0 && e.SocketError == SocketError.Success)
            {
                switch (e.LastOperation)
                {
                    case SocketAsyncOperation.Receive:
                        ProcessReceive(e); break;
                    default: break;
                }
            }
        }

        private void ProcessReceive(SocketAsyncEventArgs e)
        {
            if (OnReceive != null)
                OnReceive(e);

            if (!e.AcceptSocket.ReceiveAsync(e))
            {
                ProcessReceive(e);
            }
        }
    }
}

