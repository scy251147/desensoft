﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSMiddlewire.Core
{
    public enum Permission
    {
        Approve = 1,
        Deny = 0
    }
}
