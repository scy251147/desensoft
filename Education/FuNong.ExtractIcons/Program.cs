﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace FuNong.ExtractIcons
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var reader = new StreamReader("icon.css"))
            {
                string text = reader.ReadToEnd();

                text = text.Replace("\r\n", "~");

                var regex = new Regex(@"(?i).icon-.*?~");

                var matches = regex.Matches(text);

                using (var sw = new StreamWriter("c:\\icon.txt"))
                {
                    for (var i = 0; i < matches.Count; i++)
                    {
                        var match = matches[i];

                        if (match.Success)
                        {
                            var value = match.Value;
                            value = value.Replace("~", "");
                            value = value.Replace("{", "");
                            value = value.Replace(".", "");

                            var model = "insert into edu_icon(iconname,iconcontent) values('" + value + "','<span class=''" + value + "'' onclick=''clickToSetIcon(\""+value+"\")'' style=''height:16px;width:16px;float:left;margin:1px;cursor:pointer;''></span>')";

                            Console.Write(model);


                            sw.WriteLine(model);

                        }
                    }
                    sw.Flush();
                }
            }

            Console.ReadKey();
        }
    }
}
